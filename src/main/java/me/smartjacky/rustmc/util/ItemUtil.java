package me.smartjacky.rustmc.util;

import me.smartjacky.rustmc.ammo.AmmoHandler;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.armor.ArmorHandler;
import me.smartjacky.rustmc.consumable.ConsumableHandler;
import me.smartjacky.rustmc.drop.MobDropHandler;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.misc.MiscItemHandler;
import me.smartjacky.rustmc.projectile.ProjectileHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.tool.ToolHandler;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtil {
    public static String getItemDisplayName(Material material, Short metadata) {
        if (ConsumableHandler.isConsumable(material)) {
            return Registry.getConsumableRegistry().get(material)
                    .getDisplayName();
        } else if (GunHandler.isGun(material, metadata)) {
            return Registry.getGunRegistry()
                    .get(ListUtil.convertToKey(material, metadata))
                    .getDisplayName();
        } else if (AmmoHandler.isAmmo(material)) {
            return Registry.getAmmoRegistry().get(material).getDisplayName();
        } else if (ToolHandler.isTool(material)) {
            return Registry.getToolRegistry().get(material).getDisplayName();
        } else if (StructureHandler.isStucture(material, metadata)) {
            return Registry.getStructureRegistry()
                    .get(ListUtil.convertToKey(material, metadata))
                    .getDisplayName();
        } else if (MobDropHandler.isMobDrop(material)) {
            return Registry.getMobDropRegistry().get(material).getDisplayName();
        } else if (MiscItemHandler.isMiscItem(material, metadata)) {
            return Registry.getMiscItemRegistry().get(
                    ListUtil.convertToKey(material, metadata));
        } else if (ArmorHandler.isArmor(material)) {
            return Registry.getArmorRegistry().get(material).getDisplayName();
        } else if (ProjectileHandler.isProjectile(material, metadata)) {
            return Registry.getProjectileRegistry()
                    .get(ListUtil.convertToKey(material, metadata))
                    .getDisplayName();
        } else {
            return null;
        }
    }

    public static ItemStack getItem(String string) {
        if (string.endsWith("blueprint")
                || ConsumableHandler.isConsumable(string)) {
            if (string.endsWith("blueprint")) {
                String blueprintType = string.replaceAll("blueprint", "");
                if (ItemUtil.getItem(blueprintType) != null) {
                    String displayName = ItemUtil.getItem(blueprintType)
                            .getItemMeta().getDisplayName();
                    return ItemUtil.setItemDisplayName(ConsumableHandler
                            .getConsumale(string.substring(string.length() - 9,
                                    string.length())), displayName + " "
                            + "Blue Print");
                } else {
                    return null;
                }
            } else {
                return ConsumableHandler.getConsumale(string);
            }
        } else if (GunHandler.isGun(string)) {
            return GunHandler.getGun(string);
        } else if (AmmoHandler.isAmmo(string)) {
            return AmmoHandler.getAmmo(string);
        } else if (ToolHandler.isTool(string)) {
            return ToolHandler.getTool(string);
        } else if (StructureHandler.isStructure(string)) {
            return StructureHandler.getStructure(string);
        } else if (MobDropHandler.isMobDrop(string)) {
            return MobDropHandler.getMobDrop(string);
        } else if (MiscItemHandler.isMiscItem(string)) {
            return MiscItemHandler.getMiscItem(string);
        } else if (ArmorHandler.isArmor(string)) {
            return ArmorHandler.getArmor(string);
        } else if (ProjectileHandler.isProjectile(string)) {
            return ProjectileHandler.getProjectile(string);
        } else {
            return null;
        }
    }

    public static ItemStack setItemDisplayName(ItemStack itemStack,
            String string) {
        ItemMeta im = itemStack.getItemMeta();
        im.setDisplayName(ChatUtil.format(string));
        itemStack.setItemMeta(im);
        return itemStack;
    }

    public static ItemStack setItemDisplayName(ItemStack itemStack) {
        ItemMeta im = itemStack.getItemMeta();
        im.setDisplayName(ChatUtil.format("&e"
                + ItemUtil.getItemDisplayName(itemStack.getType(),
                        itemStack.getDurability())));
        itemStack.setItemMeta(im);
        return itemStack;
    }
}
