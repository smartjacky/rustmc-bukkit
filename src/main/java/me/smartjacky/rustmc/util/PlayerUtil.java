package me.smartjacky.rustmc.util;

import me.smartjacky.rustmc.RustMC;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class PlayerUtil {
    public static Object getMetadata(Player player, String string) {
        if (player.hasMetadata(string)) {
            Object val = null;
            for (MetadataValue value : player.getMetadata(string)) {
                val = value.value();
            }
            return val;
        }
        return null;
    }

    public static void setMetadata(Player player, String string, Object object) {
        player.setMetadata(string, new FixedMetadataValue(RustMC.instance(),
                object));
    }
}
