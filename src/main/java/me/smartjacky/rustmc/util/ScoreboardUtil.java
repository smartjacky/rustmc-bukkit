package me.smartjacky.rustmc.util;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class ScoreboardUtil {
    public static String parseItem(String value, Player player) {
        if (value.equalsIgnoreCase("%health%")) {
            return Integer.toString((int) (player.getHealth()));
        }
        if (value.equalsIgnoreCase("%radiation%")) {
            int radiation = 0;
            if (player.hasMetadata("RADIATION")) {
                List<MetadataValue> radiationList = player
                        .getMetadata("RADIATION");
                for (MetadataValue val : radiationList) {
                    radiation = (Integer) val.value();
                }
                return Integer.toString(radiation);
            } else {
                return "0";
            }
        }
        if (value.equalsIgnoreCase("%hunger%")) {
            return Integer.toString(player.getFoodLevel());
        } else {
            return null;
        }
    }
}
