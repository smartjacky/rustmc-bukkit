package me.smartjacky.rustmc.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public class ListUtil {
    public static List<Object> convertToKey(Material material, short metadata) {
        List<Object> materialData = new ArrayList<Object>();
        materialData.add(material);
        materialData.add(metadata);
        return materialData;
    }

    public static List<Integer> convertToKey(int x, int z) {
        List<Integer> blockLocation = new ArrayList<Integer>();
        blockLocation.add(x);
        blockLocation.add(z);
        return blockLocation;
    }
}
