package me.smartjacky.rustmc.util;

import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtil {
    public static String format(String message) {
        return ChatColor.translateAlternateColorCodes('&', message.replaceAll(
                "%rustmc-prefix%", ConfigurationHandler.chatPrefix));
    }

    public static String format(String message, Player player) {
        return ChatColor.translateAlternateColorCodes(
                '&',
                message.replaceAll("%rustmc-prefix%",
                        ConfigurationHandler.chatPrefix)
                        .replaceAll("%username%", player.getName())
                        .replaceAll("%nickname%", player.getDisplayName()));
    }

    public static String titleFormat(String message) {
        return ChatColor.translateAlternateColorCodes('&',
                ConfigurationHandler.chatPrefix + "&r" + message);
    }

    public static IChatBaseComponent toIChatBaseComponet(String string) {
        return ChatSerializer.a(string);
    }

    public static String jsonConverter(String text) {
        return "{\"text\": \"" + ChatUtil.format(text) + "\"}";
    }
}
