package me.smartjacky.rustmc.util;

import java.io.File;
import java.io.IOException;

import me.smartjacky.rustmc.RustMC;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileUtil {
    private static File userDir = new File(RustMC.instance().getDataFolder()
            + File.separator + "userdata");

    public static File getUserDir() {
        return userDir;
    }

    public static void createUserData(String uuid) {
        File userData = new File(userDir + File.separator + uuid + ".yml");
        if (!userData.exists()) {
            try {
                userData.createNewFile();
                FileUtil.setPlayerData(uuid, "radiation", 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean hasData(String uuid) {
        File userData = new File(userDir + File.separator + uuid + ".yml");
        if (userData.exists()) {
            return true;
        }
        return false;
    }

    public static Object getPlayerData(String uuid, String path) {
        File userData = new File(userDir + File.separator + uuid + ".yml");
        FileConfiguration playerData = YamlConfiguration
                .loadConfiguration(userData);
        try {
            playerData.save(userData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return playerData.get(path);
    }

    public static void setPlayerData(String uuid, String path, Object value) {
        File userData = new File(userDir + File.separator + uuid + ".yml");
        FileConfiguration playerData = YamlConfiguration
                .loadConfiguration(userData);
        playerData.set(path, value);
        try {
            playerData.save(userData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
