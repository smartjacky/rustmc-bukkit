package me.smartjacky.rustmc.gun;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.util.Vector;

public class GunHandler {
    public static void init() {
        Registry.registerGun(Material.INK_SACK, (short) 0, new Gun(0, 1, 7, 2,
                24, 4, Material.SEEDS, "M4"));
        Registry.registerGun(Material.INK_SACK, (short) 1, new Gun(0, 1, 4, 2,
                12, 3, Material.PUMPKIN_SEEDS, "9MM"));
        Registry.registerGun(Material.INK_SACK, (short) 2, new Gun(5, 5, 10, 4,
                8, 2, Material.MELON, "Shotgun"));
        Registry.registerGun(Material.INK_SACK, (short) 3, new Gun(0, 1, 6, 2,
                8, 4, Material.PUMPKIN_SEEDS, "P250"));
        Registry.registerGun(Material.INK_SACK, (short) 4, new Gun(0, 1, 5, 1,
                30, 3, Material.PUMPKIN_SEEDS, "MP5A4"));
        Registry.registerGun(Material.INK_SACK, (short) 5, new Gun(0, 1, 10, 3,
                3, 4, Material.SEEDS, "Bolt Action Rifle"));
        Registry.registerGun(Material.INK_SACK, (short) 6, new Gun(1, 1, 10, 0,
                1, 1, Material.NETHER_STALK, "Hand Cannon"));
        Registry.registerGun(Material.INK_SACK, (short) 7, new Gun(5, 5, 9, 4,
                1, 3, Material.NETHER_STALK, "Pipe Shotgun"));
        Registry.registerGun(Material.INK_SACK, (short) 8, new Gun(0, 1, 6, 2,
                8, 3, Material.PUMPKIN_SEEDS, "Revolver"));
        Registry.registerGun(Material.INK_SACK, (short) 9, new Gun(0, 1, 7, 2,
                24, 4, Material.SEEDS, "Golden M4"));
        Registry.registerGun(Material.INK_SACK, (short) 10, new Gun(0, 1, 4, 2,
                12, 3, Material.PUMPKIN_SEEDS, "Golden 9mm"));
        Registry.registerGun(Material.INK_SACK, (short) 11, new Gun(0, 420, 6969,
        		0, Integer.MAX_VALUE, Integer.MAX_VALUE, Material.SEEDS,
        		"1st Year Anniversary Gun"));
    }

    public static void fire(Player player, Action action) {
        String ammo = player.getItemInHand().getItemMeta().getDisplayName();
        ItemStack gun = player.getItemInHand();
        Material gunType = gun.getType();
        short gunData = gun.getDurability();
        if (ammo == null
                || !ammo.matches("([" + (char) 167
                        + "][0-9a-z][a-zA-z0-9 a-zA-z0-9]+ - [0-9]+)")) {
            setGun(player.getItemInHand());
        } else {
            int getDelay = (Integer) GunHandler.getPlayerMetadata(player,
                    "DELAY_KEY");
            if (getDelay == 0 && !gun.getItemMeta().hasEnchants()) {
                if (getAmmoInGun(gun) > 0) {
                    ItemMeta im = player.getItemInHand().getItemMeta();
                    for (int i = 0; i < Registry.getGunRegistry()
                            .get(ListUtil.convertToKey(gunType, gunData))
                            .getBullet(); i++) {
                        Location loc = player.getEyeLocation();
                        Snowball snowball = player.getWorld().spawn(loc,
                                Snowball.class);
                        snowball.setShooter(player);
                        snowball.setVelocity(getRandomAccuracy(
                                player,
                                Registry.getGunRegistry()
                                        .get(ListUtil.convertToKey(gunType,
                                                gunData)).getAccuracy())
                                .multiply(
                                        Registry.getGunRegistry()
                                                .get(ListUtil.convertToKey(
                                                        gunType, gunData))
                                                .getRange()));
                        snowball.setMetadata(
                                "DAMAGE_KEY",
                                new FixedMetadataValue(RustMC.instance(),
                                        Registry.getGunRegistry()
                                                .get(ListUtil.convertToKey(
                                                        gunType, gunData))
                                                .getDamage()));
                    }
                    if ((Double) getPlayerMetadata(player, "RECOIL") >= 0.1) {

                    } else {
                        player.setMetadata(
                                "RECOIL",
                                new FixedMetadataValue(RustMC.instance(),
                                        (Double) getPlayerMetadata(player,
                                                "RECOIL") + 0.05));

                    }
                    player.playSound(player.getLocation(), Sound.NOTE_PLING, 1,
                            1);
                    List<Entity> nearbyEntities = player.getNearbyEntities(50,
                            50, 50);
                    for (Entity entity : nearbyEntities) {
                        if (entity instanceof Player) {
                            Player nearbyPlayer = (Player) entity;
                            nearbyPlayer.playSound(player.getLocation(),
                                    Sound.NOTE_PLING, 1, 1);
                        }
                    }
                    int useAmmo = getAmmoInGun(gun) - 1;
                    im.setDisplayName(ChatColor.YELLOW
                            + Registry
                                    .getGunRegistry()
                                    .get(ListUtil
                                            .convertToKey(gunType, gunData))
                                    .getDisplayName() + " - " + useAmmo);
                    player.getItemInHand().setItemMeta(im);
                    player.setMetadata(
                            "DELAY_KEY",
                            new FixedMetadataValue(RustMC.instance(), Registry
                                    .getGunRegistry()
                                    .get(ListUtil
                                            .convertToKey(gunType, gunData))
                                    .getDelay()));
                } else if (getAmmoInGun(gun) == 0) {
                    reloadGun(player, gun);
                }
            }
        }
    }

    public static void setGun(ItemStack item) {
        Material gunType = item.getType();
        short gunData = item.getDurability();
        Object[] x = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
                "x", "y", "z", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        Random random = new Random();
        String randomString = ChatColor.DARK_GRAY + "ID: ";
        for (int i = 0; i < 10; i++) {
            randomString += x[random.nextInt(35)];
        }
        ArrayList<String> loreList = new ArrayList<String>();
        loreList.add(randomString);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(ChatColor.YELLOW
                + Registry.getGunRegistry()
                        .get(ListUtil.convertToKey(gunType, gunData))
                        .getDisplayName() + " - 0");
        im.setLore(loreList);
        item.setItemMeta(im);
    }

    public static int getAmmoInGun(ItemStack gun) {
        int ammoLeft = 0;
        if (gun.getItemMeta().getDisplayName() != null) {
            String ammo = gun.getItemMeta().getDisplayName();
            String getInt = ammo.replaceAll("([" + (char) 167
                    + "][0-9a-z][a-zA-z0-9 a-zA-z0-9]+ - )", "");
            ammoLeft = Integer.parseInt(getInt);
        }
        return ammoLeft;
    }

    public static void reloadGun(final Player player, final ItemStack gun) {
        final Material gunType = gun.getType();
        final byte gunData = (byte) gun.getDurability();
        final int ammoNeeded = Registry.getGunRegistry()
                .get(ListUtil.convertToKey(gunType, gunData)).getMaxAmmo()
                - getAmmoInGun(gun);
        final int ammoInInventory = getAmmoInInventory(player, Registry
                .getGunRegistry().get(ListUtil.convertToKey(gunType, gunData))
                .getBulletType());
        final ItemMeta im = gun.getItemMeta();
        String displayName = Registry.getGunRegistry()
                .get(ListUtil.convertToKey(gunType, gunData)).getDisplayName();
        final int maxAmmo = Registry.getGunRegistry()
                .get(ListUtil.convertToKey(gunType, gunData)).getMaxAmmo();
        if (player.getGameMode() == GameMode.SURVIVAL) {
            if (!im.hasEnchants()) {
                if (ammoNeeded != 0) {
                    if (ammoInInventory != 0) {
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&eReloading!"));
                        if (ammoInInventory >= ammoNeeded) {
                            im.setDisplayName(ChatColor.YELLOW + displayName
                                    + " - " + maxAmmo);
                        } else if (ammoInInventory < ammoNeeded) {
                            im.setDisplayName(ChatColor.YELLOW + displayName
                                    + " - " + ammoInInventory);
                        }
                        im.addEnchant(Enchantment.DURABILITY, 1, false);
                        gun.setItemMeta(im);
                        Bukkit.getServer()
                                .getScheduler()
                                .scheduleSyncDelayedTask(RustMC.instance(),
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                player.getInventory()
                                                        .removeItem(
                                                                new ItemStack(
                                                                        Registry.getGunRegistry()
                                                                                .get(ListUtil
                                                                                        .convertToKey(
                                                                                                gunType,
                                                                                                gunData))
                                                                                .getBulletType(),
                                                                        getUsedAmmo(
                                                                                ammoInInventory,
                                                                                ammoNeeded)));
                                            }
                                        }, 1L);
                        Bukkit.getServer()
                                .getScheduler()
                                .scheduleSyncDelayedTask(RustMC.instance(),
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                ItemStack[] inventory = player
                                                        .getInventory()
                                                        .getContents();
                                                ItemMeta itemIM = gun
                                                        .getItemMeta();
                                                for (ItemStack i : inventory) {
                                                    if (i != null
                                                            && gun.getType() == i
                                                                    .getType()) {
                                                        ItemMeta iIM = i
                                                                .getItemMeta();
                                                        if (iIM.hasLore()) {
                                                            String itemLore = null, iLore = null;
                                                            List<String> itemLoreList = itemIM
                                                                    .getLore();
                                                            List<String> iLoreList = iIM
                                                                    .getLore();
                                                            for (String str : itemLoreList) {
                                                                itemLore = str;
                                                            }
                                                            for (String str : iLoreList) {
                                                                iLore = str;
                                                            }
                                                            if (itemLore == iLore) {
                                                                iIM.removeEnchant(Enchantment.DURABILITY);
                                                                i.setItemMeta(iIM);
                                                                player.sendMessage(ChatUtil
                                                                        .format("%rustmc-prefix%&aReload Compelete"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }, 40L);
                    } else {
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&cOut of ammo"));
                    }
                } else {
                    player.sendMessage(ChatUtil
                            .format("%rustmc-prefix%Full ammo"));
                }
            }
        } else {
            player.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&cYou can only reload your gun in survival mode"));
        }
    }

    public static int getUsedAmmo(int a1, int a2) {
        int usedAmmo = 0;
        if (a1 >= a2) {
            usedAmmo = a2;
        } else if (a1 < a2) {
            usedAmmo = a1;
        }
        return usedAmmo;

    }

    public static int getAmmoInInventory(Player player, Material bulletType) {
        ItemStack[] playerInventory = player.getInventory().getContents();
        int ammoInInventory = 0;
        for (ItemStack item : playerInventory) {
            if (item != null && item.getType() == bulletType
                    && item.getAmount() > 0) {
                ammoInInventory += item.getAmount();
            }
        }
        return ammoInInventory;
    }

    public static Vector getRandomAccuracy(Player player, int acc) {
        Random random = new Random();
        double pitch = 0, yaw = 0, recoil = 0;
        if (acc > 0) {
            pitch = ((player.getLocation().getPitch() + 90 + random.nextInt(acc
                    * 2 - acc)) * Math.PI) / 180;
            yaw = ((player.getLocation().getYaw() + 90 + random.nextInt(acc * 2
                    - acc)) * Math.PI) / 180;
        } else {
            pitch = ((player.getLocation().getPitch() + 90) * Math.PI) / 180;
            yaw = ((player.getLocation().getYaw() + 90) * Math.PI) / 180;
        }
        for (MetadataValue val : player.getMetadata("RECOIL")) {
            recoil = (Double) val.value();
        }
        double x = Math.sin(pitch) * Math.cos(yaw);
        double y = Math.sin(pitch) * Math.sin(yaw);
        double z = 0;
        if (recoil <= 0) {
            z = Math.cos(pitch);
        } else {
            z = Math.cos(pitch) + recoil;
        }
        Vector vector = new Vector(x, z, y);
        return vector;
    }

    public static Object getPlayerMetadata(Player player, String key) {
        List<MetadataValue> damage = player.getMetadata(key);
        for (MetadataValue val : damage) {
            Object value = val.value();
            return value;
        }
        return null;
    }

    public static boolean isGun(Material key, short key2) {
        if (Registry.getGunRegistry().containsKey(
                ListUtil.convertToKey(key, key2))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGun(String string) {
        for (Gun gun : Registry.getGunRegistry().values()) {
            if (gun.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getGun(String string) {
        for (Gun gun : Registry.getGunRegistry().values()) {
            if (gun.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<List<Object>, Gun> entry : Registry.getGunRegistry()
                        .entrySet()) {
                    if (entry.getValue() == gun) {
                        ItemStack item = new ItemStack((Material) entry
                                .getKey().get(0), 1);
                        item.setDurability((Short) entry.getKey().get(1));
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
