package me.smartjacky.rustmc.gun;

import org.bukkit.Material;

public class Gun {
    private int accuracy;
    private int bullet;
    private int damage;
    private int delay;
    private int maxAmmo;
    private int range;
    private Material bulletType;
    private String displayName;

    public Gun(int accuracy, int bullet, int damage, int delay, int maxAmmo,
            int range, Material type, String displayName) {
        this.accuracy = accuracy;
        this.bullet = bullet;
        this.damage = damage;
        this.delay = delay;
        this.maxAmmo = maxAmmo;
        this.range = range;
        this.bulletType = type;
        this.displayName = displayName;
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public int getBullet() {
        return this.bullet;
    }

    public int getDamage() {
        return this.damage;
    }

    public int getDelay() {
        return this.delay;
    }

    public int getMaxAmmo() {
        return this.maxAmmo;
    }

    public int getRange() {
        return this.range;
    }

    public Material getBulletType() {
        return this.bulletType;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public void setBullet(int bullet) {
        this.bullet = bullet;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void setMaxAmmo(int maxAmmo) {
        this.maxAmmo = maxAmmo;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public void setBulletType(Material bulletType) {
        this.bulletType = bulletType;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
