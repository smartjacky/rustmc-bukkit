package me.smartjacky.rustmc.api;

public interface ITool {
    public int getDamage();

    public int getDelay();

    public String getDisplayName();

    public int getDurability();

    public int getTreeObtainAmount();

    public int getWoodPileObtainAmount();

    public int getMetalObtainAmount();

    public boolean isBreakable();
}
