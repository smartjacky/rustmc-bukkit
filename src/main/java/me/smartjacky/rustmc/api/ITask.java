package me.smartjacky.rustmc.api;

public interface ITask {
    public Runnable getRunnable();

    public long getPeriod();

    public boolean isEnabled();
}
