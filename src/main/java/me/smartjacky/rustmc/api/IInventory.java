package me.smartjacky.rustmc.api;

import me.smartjacky.rustmc.inventory.InventoryItemStack;

import org.bukkit.Material;

public interface IInventory {
    public String[] getAction(Material material, short metadata);

    public String getInventoryName();

    public InventoryItemStack[] getItems();
}
