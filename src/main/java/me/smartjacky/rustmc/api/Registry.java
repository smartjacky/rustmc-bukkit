package me.smartjacky.rustmc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.smartjacky.rustmc.drop.MobDrop;
import me.smartjacky.rustmc.gun.Gun;

import org.bukkit.Material;

public class Registry {
    private static HashMap<Material, IAmmo> ammoRegistry = null;
    private static HashMap<Material, IConsumable> consumableRegistry = null;
    private static HashMap<List<Object>, Gun> gunRegistry = null;
    private static HashMap<List<Object>, IStructure> structureRegistry = null;
    private static HashMap<Material, ITool> toolRegistry = null;
    private static HashMap<String, IInventory> inventoryRegistry = null;
    private static HashMap<String, ITask> taskRegistry = null;
    private static HashMap<Material, MobDrop> mobDropRegistry = null;
    private static HashMap<List<Object>, IRecipe> recipeRegistry = null;
    private static HashMap<List<Object>, String> miscItemRegistry = null;
    private static HashMap<Material, Armor> armorRegistry = null;
    private static HashMap<List<Object>, IProjectile> projectileRegistry = null;

    public Registry() {
        Registry.ammoRegistry = new HashMap<Material, IAmmo>();
        Registry.inventoryRegistry = new HashMap<String, IInventory>();
        Registry.consumableRegistry = new HashMap<Material, IConsumable>();
        Registry.gunRegistry = new HashMap<List<Object>, Gun>();
        Registry.structureRegistry = new HashMap<List<Object>, IStructure>();
        Registry.toolRegistry = new HashMap<Material, ITool>();
        Registry.taskRegistry = new HashMap<String, ITask>();
        Registry.mobDropRegistry = new HashMap<Material, MobDrop>();
        Registry.recipeRegistry = new HashMap<List<Object>, IRecipe>();
        Registry.miscItemRegistry = new HashMap<List<Object>, String>();
        Registry.armorRegistry = new HashMap<Material, Armor>();
        Registry.projectileRegistry = new HashMap<List<Object>, IProjectile>();
    }

    public static HashMap<Material, IAmmo> getAmmoRegistry() {
        return Registry.ammoRegistry;
    }

    public static HashMap<String, IInventory> getInventoryRegistry() {
        return Registry.inventoryRegistry;
    }

    public static HashMap<Material, IConsumable> getConsumableRegistry() {
        return Registry.consumableRegistry;
    }

    public static HashMap<List<Object>, Gun> getGunRegistry() {
        return Registry.gunRegistry;
    }

    public static HashMap<List<Object>, IStructure> getStructureRegistry() {
        return Registry.structureRegistry;
    }

    public static HashMap<Material, ITool> getToolRegistry() {
        return Registry.toolRegistry;
    }

    public static HashMap<String, ITask> getTaskRegistry() {
        return Registry.taskRegistry;
    }

    public static HashMap<Material, MobDrop> getMobDropRegistry() {
        return Registry.mobDropRegistry;
    }

    public static HashMap<List<Object>, IRecipe> getRecipeRegistry() {
        return Registry.recipeRegistry;
    }

    public static HashMap<List<Object>, String> getMiscItemRegistry() {
        return Registry.miscItemRegistry;
    }

    public static HashMap<Material, Armor> getArmorRegistry() {
        return Registry.armorRegistry;
    }

    public static HashMap<List<Object>, IProjectile> getProjectileRegistry() {
        return Registry.projectileRegistry;
    }

    public static void registerAmmo(Material material, IAmmo ammo) {
        Registry.ammoRegistry.put(material, ammo);
    }

    public static void registerInventory(String name, IInventory inventory) {
        Registry.inventoryRegistry.put(name, inventory);
    }

    public static void registerConsumable(Material material,
            IConsumable consumable) {
        Registry.consumableRegistry.put(material, consumable);
    }

    public static void registerGun(Material material, short metadata, Gun gun) {
        Registry.gunRegistry
                .put(Registry.convertToKey(material, metadata), gun);
    }

    public static void registerStructure(Material material, IStructure structure) {
        Registry.structureRegistry.put(
                Registry.convertToKey(material, (short) 0), structure);

    }

    public static void registerStructure(Material material, short metadata,
            IStructure structure) {
        Registry.structureRegistry.put(
                Registry.convertToKey(material, metadata), structure);
    }

    public static void registerTool(Material material, ITool tool) {
        Registry.toolRegistry.put(material, tool);
    }

    public static void registerTask(String string, ITask task) {
        Registry.taskRegistry.put(string, task);
    }

    public static void registerMobDrop(Material material, String string) {
        Registry.mobDropRegistry.put(material, new MobDrop(string));
    }

    public static void registerRecipe(Material material, short metadata,
            IRecipe recipe) {
        Registry.recipeRegistry.put(convertToKey(material, metadata), recipe);
    }

    public static void registerMiscItem(Material material, short metadata,
            String displayName) {
        Registry.miscItemRegistry.put(convertToKey(material, metadata),
                displayName);
    }

    public static void registerArmor(Material material, Armor armor) {
        Registry.armorRegistry.put(material, armor);
    }

    public static void registerProjectile(Material material, short metadata,
            IProjectile projectile) {
        Registry.projectileRegistry.put(convertToKey(material, metadata),
                projectile);
    }

    private static List<Object> convertToKey(Material material, short metadata) {
        List<Object> materialData = new ArrayList<Object>();
        materialData.add(material);
        materialData.add(metadata);
        return materialData;
    }
}
