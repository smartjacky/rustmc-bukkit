package me.smartjacky.rustmc.api;

public class Armor {
    private int bulletP, meleeP, explosionP, coldP, radiationP;
    private String displayName;

    public Armor(int bulletProtection, int meleeProtection,
            int explosionProtection, int coldProtection,
            int radiationProtection, String displayName) {
        this.bulletP = bulletProtection;
        this.meleeP = meleeProtection;
        this.explosionP = explosionProtection;
        this.coldP = coldProtection;
        this.radiationP = radiationProtection;
        this.displayName = displayName;
    }

    public void setBulletProtection(int arg1) {
        this.bulletP = arg1;
    }

    public void setMelleProtection(int arg1) {
        this.meleeP = arg1;
    }

    public void setExplosionProtection(int arg1) {
        this.explosionP = arg1;
    }

    public void setColdProtection(int arg1) {
        this.coldP = arg1;
    }

    public void setRaidiationProtection(int arg1) {
        this.radiationP = arg1;
    }

    public void setDisplayName(String arg1) {
        this.displayName = arg1;
    }

    public int getBulletProtection() {
        return this.bulletP;
    }

    public int getMeleeProtection() {
        return this.meleeP;
    }

    public int getExplosionProtection() {
        return this.explosionP;
    }

    public int getColdProtection() {
        return this.coldP;
    }

    public int getRadiationProtection() {
        return this.radiationP;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
