package me.smartjacky.rustmc.api;

import org.bukkit.entity.Player;

public interface IConsumable {
    public void getEffect(Player player);

    public String getDisplayName();

    public int getFoodLevel();

    public int getHealthLevel();
}
