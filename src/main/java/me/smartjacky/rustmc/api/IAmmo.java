package me.smartjacky.rustmc.api;

public interface IAmmo {
    public String getDisplayName();
}
