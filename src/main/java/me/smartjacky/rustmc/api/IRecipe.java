package me.smartjacky.rustmc.api;

import org.bukkit.inventory.ItemStack;

public interface IRecipe {
    public ItemStack[] toItems();

    public ItemStack[] fromItems();

    public int craftTime();

    public boolean hasBluePrint();

    public String permission();
}
