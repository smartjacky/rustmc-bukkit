package me.smartjacky.rustmc.api;

import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface IStructure {
    public int getHealth();

    public int getDataValue(Face face);

    public String getDisplayName();

    public boolean keepOriginalBlock();

    public Material newItemType();

    public String[] structure(Player player);

    public boolean validation(Block block, Player player);

    public boolean mutltiTypeStructure();

    public boolean destroyable();
}
