package me.smartjacky.rustmc.api;

import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public interface IProjectile {
    public void main(World world, Item item, Player player);

    public int getDelay();

    public int getSpeed();

    public String getDisplayName();
}
