package me.smartjacky.rustmc.armor;

import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.smartjacky.rustmc.api.Armor;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

public class ArmorHandler {
	public enum DamageType {
		GUN, MELEE, EXPLOSION, COLD, RADIATION
	}

	public static void init() {
		Registry.registerArmor(Material.LEATHER_HELMET, new Armor(10, 10, 5, 5, 0, "Cloth Helmet"));
		Registry.registerArmor(Material.LEATHER_CHESTPLATE, new Armor(10, 10, 5, 10, 0, "Cloth Vest"));
		Registry.registerArmor(Material.LEATHER_LEGGINGS, new Armor(5, 5, 5, 5, 0, "Cloth Pants"));
		Registry.registerArmor(Material.LEATHER_BOOTS, new Armor(8, 8, 5, 5, 0, "Cloth Boots"));
		Registry.registerArmor(Material.CHAINMAIL_HELMET, new Armor(10, 15, 10, 15, 10, "Leather Helmet"));
		Registry.registerArmor(Material.CHAINMAIL_CHESTPLATE, new Armor(10, 15, 10, 15, 10, "Leather Vest"));
		Registry.registerArmor(Material.CHAINMAIL_LEGGINGS, new Armor(10, 10, 10, 10, 10, "Leather Pants"));
		Registry.registerArmor(Material.CHAINMAIL_BOOTS, new Armor(10, 10, 10, 10, 10, "Leather Boots"));
		Registry.registerArmor(Material.IRON_HELMET, new Armor(25, 25, 15, 10, 0, "Kevlar Helmet"));
		Registry.registerArmor(Material.IRON_CHESTPLATE, new Armor(30, 30, 15, 10, 0, "Kevlar Vest"));
		Registry.registerArmor(Material.IRON_LEGGINGS, new Armor(15, 15, 15, 10, 10, "Kevlar Pants"));
		Registry.registerArmor(Material.IRON_BOOTS, new Armor(15, 15, 15, 10, 10, "Kevlar Boots"));
		Registry.registerArmor(Material.GOLD_HELMET, new Armor(5, 5, 20, 25, 30, "Rad Suit Helmet"));
		Registry.registerArmor(Material.GOLD_CHESTPLATE, new Armor(5, 5, 25, 25, 50, "Rad Suit Vest"));
		Registry.registerArmor(Material.GOLD_LEGGINGS, new Armor(5, 5, 25, 25, 40, "Rad Suit Pants"));
		Registry.registerArmor(Material.GOLD_BOOTS, new Armor(5, 5, 25, 25, 20, "Rad Suit Boots"));
		Registry.registerArmor(Material.DIAMOND_HELMET, new Armor(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, Integer.MAX_VALUE, "Anniversary Helment"));
		Registry.registerArmor(Material.DIAMOND_CHESTPLATE, new Armor(Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, "Anniversary Vest"));
		Registry.registerArmor(Material.DIAMOND_LEGGINGS, new Armor(Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, "Anniversary Pants"));
		Registry.registerArmor(Material.DIAMOND_BOOTS, new Armor(Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, "Anniversary Boots"));

	}

	public static double getDamage(double damage, LivingEntity entity, DamageType damageType) {
		int protection = 0;
		ItemStack[] armorContent = { entity.getEquipment().getHelmet(), entity.getEquipment().getChestplate(),
				entity.getEquipment().getLeggings(), entity.getEquipment().getBoots() };
		switch (damageType) {
		case GUN:
			for (ItemStack armor : armorContent) {
				if (armor != null && Registry.getArmorRegistry().containsKey(armor.getType())) {
					protection += Registry.getArmorRegistry().get(armor.getType()).getBulletProtection();
				}
			}
			break;
		case MELEE:
			for (ItemStack armor : armorContent) {
				if (armor != null && Registry.getArmorRegistry().containsKey(armor.getType())) {
					protection += Registry.getArmorRegistry().get(armor.getType()).getMeleeProtection();
				}
			}
			break;
		case EXPLOSION:
			for (ItemStack armor : armorContent) {
				if (armor != null && Registry.getArmorRegistry().containsKey(armor.getType())) {
					protection += Registry.getArmorRegistry().get(armor.getType()).getExplosionProtection();
				}
			}
			break;
		case COLD:
			for (ItemStack armor : armorContent) {
				if (armor != null && Registry.getArmorRegistry().containsKey(armor.getType())) {
					protection += Registry.getArmorRegistry().get(armor.getType()).getColdProtection();
				}
			}
			break;
		case RADIATION:
			for (ItemStack armor : armorContent) {
				if (armor != null && Registry.getArmorRegistry().containsKey(armor.getType())) {
					protection += Registry.getArmorRegistry().get(armor.getType()).getRadiationProtection();
				}
			}
			break;
		}
		return damage - (damage * ((protection * 0.553) / 100));
	}

	public static boolean isArmor(Material material) {
		if (Registry.getArmorRegistry().containsKey(material)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isArmor(String string) {
		for (Armor armor : Registry.getArmorRegistry().values()) {
			if (armor.getDisplayName().replaceAll(" ", "").equalsIgnoreCase(string)) {
				return true;
			}
		}
		return false;
	}

	public static ItemStack getArmor(String string) {
		for (Armor armor : Registry.getArmorRegistry().values()) {
			if (armor.getDisplayName().replaceAll(" ", "").equalsIgnoreCase(string)) {
				for (Entry<Material, Armor> entry : Registry.getArmorRegistry().entrySet()) {
					if (entry.getValue() == armor) {
						ItemStack item = new ItemStack(entry.getKey(), 1);
						ItemMeta im = item.getItemMeta();
						im.setDisplayName(ChatUtil
								.format("&e" + ItemUtil.getItemDisplayName(item.getType(), item.getDurability())));
						item.setItemMeta(im);
						return item;
					}
				}
			}
		}
		return null;
	}
}
