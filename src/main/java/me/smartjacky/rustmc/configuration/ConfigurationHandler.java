package me.smartjacky.rustmc.configuration;

import java.util.List;

import me.smartjacky.rustmc.RustMC;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigurationHandler {
    public static boolean hudEnabled = true;
    public static boolean joinMessageEnabled = false;
    public static boolean quitMessageEnabled = false;
    public static boolean stopMessageEnabled = false;
    public static boolean mysqlDatabaseEnabled = false;
    public static String chatPrefix = null;
    public static String hudTitle = null;
    public static String joinMessage = null;
    public static String quitMessage = null;
    public static String stopMessage = null;
    private static World world = null;
    private static int worldBorderSize = 0;

    public static void load() {
        final RustMC plugin = RustMC.instance();
        final FileConfiguration config = plugin.getConfig();
        plugin.saveDefaultConfig();
        ConfigurationHandler.chatPrefix = config.getString("chat.prefix");
        ConfigurationHandler.hudEnabled = config.getBoolean("hud.enabled");
        ConfigurationHandler.hudTitle = config.getString("hud.title");
        ConfigurationHandler.joinMessage = config
                .getString("chat.join.message");
        ConfigurationHandler.joinMessageEnabled = config
                .getBoolean("chat.join.enabled");
        ConfigurationHandler.mysqlDatabaseEnabled = config
                .getBoolean("mysql.enabled");
        ConfigurationHandler.stopMessage = config
                .getString("chat.restart.message");
        ConfigurationHandler.stopMessageEnabled = config
                .getBoolean("chat.restart.enabled");
        ConfigurationHandler.quitMessage = config
                .getString("chat.quit.message");
        ConfigurationHandler.quitMessageEnabled = config
                .getBoolean("chat.quit.enabled");
        ConfigurationHandler.world = Bukkit.getWorld(config
                .getString("world.name"));
        ConfigurationHandler.worldBorderSize = config.getInt("world.size");
    }

    public static String[] getItems(String path) {
        final RustMC plugin = RustMC.instance();
        final FileConfiguration config = plugin.getConfig();
        List<String> items = config.getStringList(path);
        return (String[]) items.toArray(new String[items.size()]);
    }

    public static World getWorld() {
        return ConfigurationHandler.world;
    }

    public static int getWorldBorderSize() {
        return ConfigurationHandler.worldBorderSize;
    }
}
