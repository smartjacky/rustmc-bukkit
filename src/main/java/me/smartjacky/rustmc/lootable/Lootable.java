package me.smartjacky.rustmc.lootable;

public enum Lootable {
    SMALL(1, 9, "Small Crate"), MEDIUM(2, 36, "Medium Crate"), LARGE(2, 50,
            "Large Crate"), ANNIVERSARY(2, 54, "Anniversary Crate");

    private int chestSize;
    private int inventorySize;
    private String displayName;

    Lootable(int chestSize, int inventorySize, String displayName) {
        this.chestSize = chestSize;
        this.inventorySize = inventorySize;
        this.displayName = displayName;
    }

    public int getChestSize() {
        return this.chestSize;
    }

    public int getInventorySize() {
        return this.inventorySize;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
