package me.smartjacky.rustmc.radiation;

import java.util.List;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.armor.ArmorHandler;
import me.smartjacky.rustmc.armor.ArmorHandler.DamageType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class RadiationHandler {
    public static void radiate(final Player player) {
        int radiation = Bukkit.getScheduler().scheduleSyncRepeatingTask(
                RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        setPlayerRadiation(
                                player,
                                (int) (getPlayerRadiation(player) + ArmorHandler
                                        .getDamage(4, player,
                                                DamageType.RADIATION)));
                    }
                }, 0L, 20L);
        if (!player.hasMetadata("RADIATING")) {
            player.setMetadata("RADIATING",
                    new FixedMetadataValue(RustMC.instance(), radiation));
        }
    }

    public static void setPlayerRadiation(Player player, int amount) {
        player.setMetadata("RADIATION",
                new FixedMetadataValue(RustMC.instance(), amount));
    }

    public static int getPlayerRadiation(Player player) {
        if (player.hasMetadata("RADIATION")) {
            int radiation = 0;
            List<MetadataValue> radiationList = player.getMetadata("RADIATION");
            for (MetadataValue val : radiationList) {
                radiation = (Integer) val.value();
            }
            return radiation;
        }
        return 0;
    }

    public static boolean isRadiating(Player player) {
        if (getPlayerRadiation(player) >= 500) {
            return true;
        }
        return false;
    }
}
