package me.smartjacky.rustmc.drop;

public class MobDrop {
    private String displayName;

    public MobDrop(String string) {
        this.setDisplayName(string);
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String string) {
        displayName = string;
    }
}
