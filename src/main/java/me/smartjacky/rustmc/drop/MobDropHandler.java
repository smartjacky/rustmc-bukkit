package me.smartjacky.rustmc.drop;

import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

public class MobDropHandler {
    public static void init() {
        Registry.registerMobDrop(Material.BLAZE_POWDER, "Animal Fat");
        Registry.registerMobDrop(Material.REDSTONE, "Blood");
    }

    public static boolean isMobDrop(Material material) {
        if (Registry.getMobDropRegistry().containsKey(material)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isMobDrop(String string) {
        for (MobDrop mobdrop : Registry.getMobDropRegistry().values()) {
            if (mobdrop.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getMobDrop(String string) {
        for (MobDrop mobdrop : Registry.getMobDropRegistry().values()) {
            if (mobdrop.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<Material, MobDrop> entry : Registry
                        .getMobDropRegistry().entrySet()) {
                    if (entry.getValue() == mobdrop) {
                        ItemStack item = new ItemStack(
                                (Material) entry.getKey(), 1);
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
