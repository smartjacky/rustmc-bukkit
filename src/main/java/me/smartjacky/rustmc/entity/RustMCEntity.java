package me.smartjacky.rustmc.entity;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import net.minecraft.server.v1_8_R3.BiomeBase;
import net.minecraft.server.v1_8_R3.BiomeBase.BiomeMeta;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityTypes;
import net.minecraft.server.v1_8_R3.EntityWolf;

import org.bukkit.entity.EntityType;

public enum RustMCEntity {
    WOLF("Wolf", 95, EntityType.WOLF, EntityWolf.class, RustMCEntityWolf.class);

    private String name;
    private int id;
    private EntityType entityType;
    private Class<? extends EntityInsentient> nmsClass;
    private Class<? extends EntityInsentient> customClass;

    private RustMCEntity(String name, int id, EntityType entityType,
            Class<? extends EntityInsentient> nmsClass,
            Class<? extends EntityInsentient> customClass) {
        this.name = name;
        this.id = id;
        this.entityType = entityType;
        this.nmsClass = nmsClass;
        this.customClass = customClass;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return id;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public Class<? extends EntityInsentient> getNMSClass() {
        return nmsClass;
    }

    public Class<? extends EntityInsentient> getCustomClass() {
        return customClass;
    }

    public static void registerEntities() {
        for (RustMCEntity entity : values())
            a(entity.getCustomClass(), entity.getName(), entity.getID());

        for (BiomeBase biomeBase : BiomeBase.getBiomes()) {
            if (biomeBase == null)
                break;
            for (String field : new String[] { "at", "au", "av", "aw" })
                try {
                    Field list = BiomeBase.class.getDeclaredField(field);
                    list.setAccessible(true);
                    @SuppressWarnings("unchecked")
                    List<BiomeMeta> mobList = (List<BiomeMeta>) list
                            .get(biomeBase);

                    for (BiomeMeta meta : mobList)
                        for (RustMCEntity entity : values())
                            if (entity.getNMSClass().equals(meta.b))
                                meta.b = entity.getCustomClass();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @SuppressWarnings("rawtypes")
    public static void unregisterEntities() {
        for (RustMCEntity entity : values()) {
            try {
                ((Map) getPrivateStatic(EntityTypes.class, "d")).remove(entity
                        .getCustomClass());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                ((Map) getPrivateStatic(EntityTypes.class, "f")).remove(entity
                        .getCustomClass());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (RustMCEntity entity : values())
            try {
                a(entity.getNMSClass(), entity.getName(), entity.getID());
            } catch (Exception e) {
                e.printStackTrace();
            }

        for (BiomeBase biomeBase : BiomeBase.getBiomes()) {
            if (biomeBase == null)
                break;

            for (String field : new String[] { "at", "au", "av", "aw" })
                try {
                    Field list = BiomeBase.class.getDeclaredField(field);
                    list.setAccessible(true);
                    @SuppressWarnings("unchecked")
                    List<BiomeMeta> mobList = (List<BiomeMeta>) list
                            .get(biomeBase);

                    for (BiomeMeta meta : mobList)
                        for (RustMCEntity entity : values())
                            if (entity.getCustomClass().equals(meta.b))
                                meta.b = entity.getNMSClass();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @SuppressWarnings("rawtypes")
    private static Object getPrivateStatic(Class clazz, String f)
            throws Exception {
        Field field = clazz.getDeclaredField(f);
        field.setAccessible(true);
        return field.get(null);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void a(Class paramClass, String paramString, int paramInt) {
        try {
            ((Map) getPrivateStatic(EntityTypes.class, "c")).put(paramString,
                    paramClass);
            ((Map) getPrivateStatic(EntityTypes.class, "d")).put(paramClass,
                    paramString);
            ((Map) getPrivateStatic(EntityTypes.class, "e")).put(
                    Integer.valueOf(paramInt), paramClass);
            ((Map) getPrivateStatic(EntityTypes.class, "f")).put(paramClass,
                    Integer.valueOf(paramInt));
            ((Map) getPrivateStatic(EntityTypes.class, "g")).put(paramString,
                    Integer.valueOf(paramInt));
        } catch (Exception exc) {
        }
    }
}
