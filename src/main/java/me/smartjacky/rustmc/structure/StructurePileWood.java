package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructurePileWood implements IStructure {
    @Override
    public int getDataValue(Face face) {
        return 3;
    }

    @Override
    public String getDisplayName() {
        return "Wood Pile";
    }

    @Override
    public boolean keepOriginalBlock() {
        return true;
    }

    @Override
    public Material newItemType() {
        return Material.LOG;
    }

    @Override
    public String[] structure(Player player) {
        return null;
    }

    @Override
    public boolean validation(Block block, Player player) {
        World world = block.getWorld();
        Block checkBlock = world.getBlockAt(block.getX(), block.getY() - 1,
                block.getZ());
        if (!StructureHandler.isStucture(checkBlock)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return false;
    }

    @Override
    public int getHealth() {
        return 0;
    }
}
