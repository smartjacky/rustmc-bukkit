package me.smartjacky.rustmc.structure;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.lootable.Lootable;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

public class StructureLootable implements IStructure {
    private Lootable lootable;

    public StructureLootable(Lootable lootable) {
        this.lootable = lootable;
    }

    @Override
    public int getHealth() {
        return 300;
    }

    @Override
    public String getDisplayName() {
        return lootable.getDisplayName();
    }

    @Override
    public boolean keepOriginalBlock() {
        return true;
    }

    @Override
    public Material newItemType() {
        return Material.CHEST;
    }

    @Override
    public String[] structure(Player player) {
        Face face = StructureHandler.getPlayerFacing(player);
        switch (lootable) {
        case SMALL:
            return new String[] { "0.0.0" };
        case MEDIUM:
            if (face == Face.SOUTH || face == Face.NORTH) {
                return new String[] { "0.0.0", "1.0.0" };
            } else {
                return new String[] { "0.0.0", "0.0.1" };
            }
        case LARGE:
            if (face == Face.SOUTH || face == Face.NORTH) {
                return new String[] { "0.0.0", "1.0.0" };
            } else {
                return new String[] { "0.0.0", "0.0.1" };
            }
        default:
            return null;
        }
    }

    @Override
    public boolean validation(Block block, Player player) {
        Face face = StructureHandler.getPlayerFacing(player);
        switch (lootable) {
        case SMALL:
            if (block.getType() == Material.AIR) {
                return true;
            }
        case MEDIUM:
            if (face == Face.SOUTH || face == Face.NORTH) {
                if (StructureHandler.isNearByBlockVaild(block, Face.EAST, 1,
                        Material.AIR, (short) 0)
                        && block.getType() == Material.AIR) {
                    return true;
                }
            } else if (StructureHandler.isNearByBlockVaild(block, Face.NORTH,
                    1, Material.AIR, (short) 0)
                    && block.getType() == Material.AIR) {
                return true;
            }
        case LARGE:
            if (face == Face.SOUTH || face == Face.NORTH) {
                if (StructureHandler.isNearByBlockVaild(block, Face.EAST, 1,
                        Material.AIR, (short) 0)
                        && block.getType() == Material.AIR) {
                    return true;
                }
            } else if (StructureHandler.isNearByBlockVaild(block, Face.NORTH,
                    1, Material.AIR, (short) 0)
                    && block.getType() == Material.AIR) {
                return true;
            }
        default:
            return false;
        }
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getDataValue(Face face) {
        return 0;
    }
}
