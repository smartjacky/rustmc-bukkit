package me.smartjacky.rustmc.structure;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

public class StructurePillarWood implements IStructure {
    @Override
    public String getDisplayName() {
        return "Wood Pillar";
    }

    @Override
    public boolean keepOriginalBlock() {
        return true;
    }

    @Override
    public Material newItemType() {
        return Material.WOOL;
    }

    @Override
    public String[] structure(Player player) {
        return new String[] { "0,0,0", "0,1,0", "0,2,0" };
    }

    @Override
    public boolean validation(Block block, Player player) {
        if (StructureHandler.isNearByBlockVaild(block, Face.DOWN, 1,
                Material.WOOL, (short) 0)
                && StructureHandler.isSurroundedBy(block, 1, Material.AIR,
                        (short) 0)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return false;
    }

    @Override
    public int getHealth() {
        return 0;
    }

    @Override
    public int getDataValue(Face face) {
        return 1;
    }
}
