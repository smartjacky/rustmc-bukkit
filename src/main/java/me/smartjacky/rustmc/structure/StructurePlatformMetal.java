package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructurePlatformMetal implements IStructure {
    @Override
    public String getDisplayName() {
        return "Metal Platform";
    }

    @Override
    public boolean keepOriginalBlock() {
        return false;
    }

    @Override
    public Material newItemType() {
        return Material.WOOL;
    }

    @Override
    public String[] structure(Player player) {
        return new String[] { "0,-1,0", "1,-1,0", "2,-1,0", "-1,-1,0",
                "-2,-1,0", "0,-1,-1", "1,-1,-1", "2,-1,-1", "-1,-1,-1",
                "-2,-1,-1", "0,-1,1", "1,-1,1", "2,-1,1", "-1,-1,1", "-2,-1,1",
                "0,-1,2", "1,-1,2", "2,-1,2", "-1,-1,2", "-2,-1,2", "0,-1,-2",
                "1,-1,-2", "2,-1,-2", "-1,-1,-2", "-2,-1,-2" };
    }

    @Override
    public boolean validation(Block block, Player player) {
        World world = block.getWorld();
        Block checkBlock = world.getBlockAt(block.getX(), block.getY() - 1,
                block.getZ());
        if (!StructureHandler.isStucture(checkBlock.getType(), checkBlock
                .getState().getData().toItemStack().getDurability())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return false;
    }

    @Override
    public int getHealth() {
        return 0;
    }

    @Override
    public int getDataValue(Face face) {
        return 6;
    }
}
