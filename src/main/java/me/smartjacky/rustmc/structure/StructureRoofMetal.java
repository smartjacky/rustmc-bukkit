package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureRoofMetal implements IStructure {
    @Override
    public String getDisplayName() {
        return "Metal Roof";
    }

    @Override
    public boolean keepOriginalBlock() {
        return false;
    }

    @Override
    public Material newItemType() {
        return Material.WOOL;
    }

    @Override
    public String[] structure(Player player) {
        return new String[] { "0,2,0", "1,2,0", "-1,2,0", "0,2,1", "0,2,-1",
                "1,2,1", "1,2,-1", "-1,2,1", "-1,2,-1" };
    }

    @Override
    public boolean validation(Block block, Player player) {
        if (StructureHandler.isNearByBlockVaild(block, Face.NORTHEAST, 2,
                Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, Face.NORTHEAST,
                        2, Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, Face.SOUTHWEST,
                        2, Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, Face.NORTHWEST,
                        2, Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, Face.DOWN, 1,
                        Material.WOOL, (short) 0)
                && !StructureHandler.isNearByBlockVaild(block, Face.UP, 2,
                        Material.WOOL, (short) 3)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return false;
    }

    @Override
    public int getHealth() {
        return 0;
    }

    @Override
    public int getDataValue(Face face) {
        return 9;
    }
}
