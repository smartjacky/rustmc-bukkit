package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureSlopeWood implements IStructure {
    @Override
    public String getDisplayName() {
        return "Wooden Slope";
    }

    @Override
    public int getDataValue(Face face) {
        switch (face) {
        case NORTH:
            return 3;
        case SOUTH:
            return 2;
        case WEST:
            return 1;
        case EAST:
            return 0;
        default:
            return 0;
        }
    }

    @Override
    public Material newItemType() {
        return Material.WOOD_STAIRS;
    }

    @Override
    public String[] structure(Player player) {
        switch (StructureHandler.getPlayerFacing(player)) {
        case NORTH:
            return new String[] {};
        case SOUTH:
            return new String[] {};
        case WEST:
            return new String[] {};
        case EAST:
            return new String[] {};
        default:
            return null;
        }
    }

    @Override
    public boolean validation(Block block, Player player) {
        return false;
    }

    @Override
    public boolean keepOriginalBlock() {
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getHealth() {
        return 200;
    }
}
