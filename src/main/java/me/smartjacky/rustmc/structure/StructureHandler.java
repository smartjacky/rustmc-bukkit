package me.smartjacky.rustmc.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.lootable.Lootable;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class StructureHandler {
    public enum Face {
        EAST(), SOUTH(), WEST(), NORTH(), SOUTHEAST(), SOUTHWEST(), NORTHEAST(), NORTHWEST(), UP(), DOWN(), NONE();
    }

    public static void init() {
        Registry.registerStructure(Material.WOOL, (short) 0,
                new StructurePlatformWood());
        Registry.registerStructure(Material.WOOL, (short) 1,
                new StructurePillarWood());
        Registry.registerStructure(Material.WOOL, (short) 2,
                new StructureWallWood());
        Registry.registerStructure(Material.WOOL, (short) 3,
                new StructureRoofWood());
        Registry.registerStructure(Material.WOOL, (short) 4,
                new StructureStairsWood());
        Registry.registerStructure(Material.WOOL, (short) 5,
                new StructureSlopeWood());
        Registry.registerStructure(Material.WOODEN_DOOR, (short) 0,
                new StructureDoorWood());
        Registry.registerStructure(Material.WOOL, (short) 6,
                new StructurePlatformMetal());
        Registry.registerStructure(Material.WOOL, (short) 7,
                new StructurePillarMetal());
        Registry.registerStructure(Material.WOOL, (short) 8,
                new StructureWallMetal());
        Registry.registerStructure(Material.WOOL, (short) 9,
                new StructureRoofMetal());
        Registry.registerStructure(Material.WOOL, (short) 10,
                new StructureStairsMetal());
        Registry.registerStructure(Material.WOOL, (short) 11,
                new StructureSlopeMetal());
        Registry.registerStructure(Material.WOOL, (short) 12,
                new StructureDoorwayWood());
        Registry.registerStructure(Material.WOOL, (short) 13,
                new StructureDoorwayMetal());
        Registry.registerStructure(Material.IRON_DOOR, (short) 0,
                new StructureDoorMetal());
        Registry.registerStructure(Material.CHEST, (short) 0,
                new StructureLootable(Lootable.SMALL));
        Registry.registerStructure(Material.CHEST, (short) 0,
                new StructureLootable(Lootable.MEDIUM));
        Registry.registerStructure(Material.CHEST, (short) 0,
                new StructureLootable(Lootable.LARGE));
    }

    public static void generateStructure(int generateTimes,
            IStructure structure, Face face) {
        for (int i = 0; i < generateTimes; i++) {
            int randX = new Random()
                    .nextInt(ConfigurationHandler.getWorldBorderSize() / 2);
            int randZ = new Random()
                    .nextInt(ConfigurationHandler.getWorldBorderSize() / 2);
            build(randX,
                    ConfigurationHandler.getWorld().getHighestBlockYAt(randX, randZ),
                    randZ, structure, face);
        }
    }

    @SuppressWarnings("deprecation")
    public static void build(Location location, Player player) {
        World world = ConfigurationHandler.getWorld();
        Block block = world.getBlockAt(location);
        List<Object> keys = new ArrayList<Object>();
        keys.add(block.getType());
        keys.add(block.getState().getData().toItemStack().getDurability());

        Location blockLocation = location;
        int x = blockLocation.getBlockX();
        int y = blockLocation.getBlockY();
        int z = blockLocation.getBlockZ();
        for (String newBlock : Registry.getStructureRegistry().get(keys)
                .structure(player)) {
            int a, b, c;
            String[] blockCoords = newBlock.split("\\,");
            a = Integer.parseInt(blockCoords[0]);
            b = Integer.parseInt(blockCoords[1]);
            c = Integer.parseInt(blockCoords[2]);
            Block newBlock1 = world.getBlockAt(x + a, y + b, z + c);
            newBlock1.setType(Registry.getStructureRegistry().get(keys)
                    .newItemType());
            newBlock1.setData((byte) Registry.getStructureRegistry().get(keys)
                    .getDataValue(StructureHandler.getPlayerFacing(player)));

            if (!Registry.getStructureRegistry().get(keys).keepOriginalBlock()) {
                world.getBlockAt(blockLocation).setType(Material.AIR);
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void build(final Block block, final Player player) {
        List<Object> keys = new ArrayList<Object>();
        keys.add(block.getType());
        keys.add(block.getState().getData().toItemStack().getDurability());

        Location blockLocation = block.getLocation();
        int x = blockLocation.getBlockX();
        int y = blockLocation.getBlockY();
        int z = blockLocation.getBlockZ();
        World world = block.getWorld();
        for (String newBlock : Registry.getStructureRegistry().get(keys)
                .structure(player)) {
            int a, b, c;
            String[] blockCoords = newBlock.split("\\,");
            a = Integer.parseInt(blockCoords[0]);
            b = Integer.parseInt(blockCoords[1]);
            c = Integer.parseInt(blockCoords[2]);
            Block newBlock1 = world.getBlockAt(x + a, y + b, z + c);
            newBlock1.setType(Registry.getStructureRegistry().get(keys)
                    .newItemType());
            newBlock1.setData((byte) Registry.getStructureRegistry().get(keys)
                    .getDataValue(StructureHandler.getPlayerFacing(player)));

            if (!Registry.getStructureRegistry().get(keys).keepOriginalBlock()) {
                world.getBlockAt(blockLocation).setType(Material.AIR);
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static void build(final int x, final int y, final int z,
            IStructure structure, Face face) {
        for (String newBlock : structure.structure(null)) {
            String[] blockCoords = newBlock.split("\\,");
            int a = Integer.parseInt(blockCoords[0]);
            int b = Integer.parseInt(blockCoords[1]);
            int c = Integer.parseInt(blockCoords[2]);
            World world = Bukkit.getWorld(RustMC.instance().getConfig()
                    .getString("world"));
            Block block = world.getBlockAt(x + a, y + b, z + c);
            block.setType(structure.newItemType());
            block.setData((byte) structure.getDataValue(face));
        }
    }

    public static void destroy(final Block block, final Block preB) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        if (StructureHandler.isStucture(block)
                                && StructureHandler.isDestroyable(block)) {
                            if (preB != null) {
                                preB.setType(Material.AIR);
                            }
                            for (Block b : StructureHandler
                                    .getSurroundedBlocks(block)) {
                                if (b.getType() == block.getType()) {
                                    StructureHandler.destroy(b, block);
                                }
                            }
                            block.setType(Material.AIR);
                            block.removeMetadata("HEALTH", RustMC.instance());
                        }
                    }
                }, 1L);
    }

    public static boolean isVaild(Block block, Player player, Material key,
            short key2) {
        if (Registry.getStructureRegistry()
                .get(ListUtil.convertToKey(key, key2))
                .validation(block, player)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNearByBlockVaild(Block block, Face face,
            int distance, Material material, short datavalue) {
        World world = block.getWorld();
        Location blockLocation = block.getLocation();
        int x = blockLocation.getBlockX();
        int y = blockLocation.getBlockY();
        int z = blockLocation.getBlockZ();
        Block targetBlock = null;
        if (face == Face.WEST) {
            targetBlock = world.getBlockAt(x - distance, y, z);
        } else if (face == Face.EAST) {
            targetBlock = world.getBlockAt(x + distance, y, z);
        } else if (face == Face.UP) {
            targetBlock = world.getBlockAt(x, y + distance, z);
        } else if (face == Face.DOWN) {
            targetBlock = world.getBlockAt(x, y - distance, z);
        } else if (face == Face.SOUTH) {
            targetBlock = world.getBlockAt(x, y, z + distance);
        } else if (face == Face.NORTH) {
            targetBlock = world.getBlockAt(x, y, z - distance);
        } else if (face == Face.SOUTHWEST) {
            targetBlock = world.getBlockAt(x - distance, y, z + distance);
        } else if (face == Face.SOUTHEAST) {
            targetBlock = world.getBlockAt(x - distance, y, z - distance);
        } else if (face == Face.NORTHEAST) {
            targetBlock = world.getBlockAt(x + distance, y, z + distance);
        } else if (face == Face.NORTHWEST) {
            targetBlock = world.getBlockAt(x + distance, y, z - distance);
        }
        if (targetBlock.getType() == material
                && targetBlock.getState().getData().toItemStack()
                        .getDurability() == datavalue) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSurroundedBy(Block block, int distance,
            Material surroundedBy, short dataValue) {
        if (isNearByBlockVaild(block, Face.EAST, distance, surroundedBy,
                (short) 0)
                && isNearByBlockVaild(block, Face.WEST, distance, surroundedBy,
                        (short) 0)
                && isNearByBlockVaild(block, Face.SOUTH, distance,
                        surroundedBy, (short) 0)
                && isNearByBlockVaild(block, Face.NORTH, distance,
                        surroundedBy, (short) 0)) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public static boolean isStucture(Block block) {
        if (block != null
                && Registry.getStructureRegistry()
                        .containsKey(
                                ListUtil.convertToKey(block.getType(),
                                        block.getData()))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isStructure(String string) {
        for (IStructure structure : Registry.getStructureRegistry().values()) {
            if (structure.getDisplayName() != null
                    && structure.getDisplayName().replaceAll(" ", "")
                            .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isStucture(Material material, short metadata) {
        if (Registry.getStructureRegistry().containsKey(
                ListUtil.convertToKey(material, metadata))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isDestroyable(Block block) {
        if (StructureHandler.isStucture(block)) {
            if (Registry
                    .getStructureRegistry()
                    .get(ListUtil.convertToKey(block.getType(), block
                            .getState().getData().toItemStack().getDurability()))
                    .destroyable()) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getStructure(String string) {
        for (IStructure structure : Registry.getStructureRegistry().values()) {
            if (structure.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<List<Object>, IStructure> entry : Registry
                        .getStructureRegistry().entrySet()) {
                    if (entry.getValue() == structure) {
                        ItemStack item = new ItemStack((Material) entry
                                .getKey().get(0), 1);
                        item.setDurability((Short) entry.getKey().get(1));
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }

    public static Block[] getSurroundedBlocks(Block block) {
        World world = block.getWorld();
        Block block1 = world.getBlockAt(block.getLocation().add(0, 1, 0));
        Block block2 = world.getBlockAt(block.getLocation().subtract(0, 1, 0));
        Block block4 = world.getBlockAt(block.getLocation().add(1, 0, 0));
        Block block3 = world.getBlockAt(block.getLocation().subtract(1, 0, 0));
        Block block6 = world.getBlockAt(block.getLocation().add(0, 0, 1));
        Block block5 = world.getBlockAt(block.getLocation().subtract(0, 0, 1));
        return new Block[] { block1, block2, block3, block4, block5, block6 };
    }

    public static Face getPlayerFacing(Player player) {
        float y = player.getLocation().getYaw();
        if (y < 0) {
            y += 360;
        }
        float i = y %= 360;
        if (i > 360) {
            i -= 360;
        }
        if (i > 45.1 && i < 135) {
            return Face.WEST;
        } else if (i > 135.1 && i < 225.0) {
            return Face.NORTH;
        } else if (i > 225.1 && i < 315.0) {
            return Face.EAST;
        } else {
            return Face.SOUTH;
        }
    }

    public static Face[] getLeftRightFromSide(Face face) {
        if (face == Face.NORTH || face == Face.SOUTH) {
            return new Face[] { Face.WEST, Face.EAST };
        } else {
            return new Face[] { Face.NORTH, Face.SOUTH };
        }
    }
}
