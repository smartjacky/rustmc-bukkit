package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureDoorMetal implements IStructure {
    @Override
    public int getDataValue(Face face) {
        return 0;
    }

    @Override
    public String getDisplayName() {
        return "Metal Door";
    }

    @Override
    public Material newItemType() {
        return Material.IRON_DOOR;
    }

    @Override
    public boolean keepOriginalBlock() {
        return true;
    }

    @Override
    public String[] structure(Player player) {
        return null;
    }

    @Override
    public boolean validation(Block block, Player player) {
        Face face = StructureHandler.getPlayerFacing(player);
        if (StructureHandler.isNearByBlockVaild(block, Face.DOWN, 1,
                Material.WOOL, (short) 6)
                && StructureHandler.isNearByBlockVaild(block,
                        StructureHandler.getLeftRightFromSide(face)[0], 1,
                        Material.WOOL, (short) 13)
                && StructureHandler.isNearByBlockVaild(block,
                        StructureHandler.getLeftRightFromSide(face)[1], 1,
                        Material.WOOL, (short) 13)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getHealth() {
        return 150;
    }
}
