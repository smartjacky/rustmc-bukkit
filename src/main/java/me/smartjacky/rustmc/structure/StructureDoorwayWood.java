package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureDoorwayWood implements IStructure {
    @Override
    public String getDisplayName() {
        return "Wooden Doorway";
    }

    @Override
    public int getDataValue(Face face) {
        return 12;
    }

    @Override
    public boolean keepOriginalBlock() {
        return false;
    }

    @Override
    public Material newItemType() {
        return Material.WOOL;
    }

    @Override
    public String[] structure(Player player) {
        String[] structure = null;
        Face side = StructureHandler.getPlayerFacing(player);
        if (side == Face.NORTH || side == Face.SOUTH) {
            structure = new String[] { "-1,0,0", "-1,1,0", "-1,2,0", "0,2,0",
                    "1,0,0", "1,1,0", "1,2,0" };
        }
        if (side == Face.EAST || side == Face.WEST) {
            structure = new String[] { "0,0,-1", "0,1,-1", "0,2,-1", "0,2,0",
                    "0,0,1", "0,1,1", "0,2,1" };
        }
        return structure;
    }

    @Override
    public boolean validation(Block block, Player player) {
        Face side = StructureHandler.getPlayerFacing(player);
        if (StructureHandler.isNearByBlockVaild(block,
                StructureHandler.getLeftRightFromSide(side)[0], 2,
                Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block,
                        StructureHandler.getLeftRightFromSide(side)[1], 2,
                        Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, Face.DOWN, 1,
                        Material.WOOL, (short) 0)
                && StructureHandler.isNearByBlockVaild(block, Face.UP, 2,
                        Material.AIR, (short) 0)
                && StructureHandler.isSurroundedBy(block, 1, Material.AIR,
                        (short) 0)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getHealth() {
        return 150;
    }
}
