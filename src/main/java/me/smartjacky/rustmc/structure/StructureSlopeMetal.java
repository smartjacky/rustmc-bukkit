package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureSlopeMetal implements IStructure {
    @Override
    public int getDataValue(Face face) {
        return 0;
    }

    @Override
    public String getDisplayName() {
        return "Metal Slope";
    }

    @Override
    public boolean keepOriginalBlock() {
        return false;
    }

    @Override
    public Material newItemType() {
        return Material.WOOD_STAIRS;
    }

    @Override
    public String[] structure(Player player) {
        return null;
    }

    @Override
    public boolean validation(Block block, Player player) {
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getHealth() {
        return 400;
    }
}
