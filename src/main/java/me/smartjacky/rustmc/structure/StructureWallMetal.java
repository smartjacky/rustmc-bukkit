package me.smartjacky.rustmc.structure;

import me.smartjacky.rustmc.api.IStructure;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class StructureWallMetal implements IStructure {
    @Override
    public int getDataValue(Face face) {
        return 8;
    }

    @Override
    public String getDisplayName() {
        return "Wood Wall";
    }

    @Override
    public boolean keepOriginalBlock() {
        return true;
    }

    @Override
    public Material newItemType() {
        return Material.WOOL;
    }

    @Override
    public String[] structure(Player player) {
        String[] structure = null;
        Face side = StructureHandler.getPlayerFacing(player);
        if (side == Face.NORTH || side == Face.SOUTH) {
            structure = new String[] { "0,0,0", "-1,0,0", "1,0,0", "-1,1,0",
                    "1,1,0", "-1,2,0", "0,2,0", "1,2,0", "0,1,0" };
        }
        if (side == Face.EAST || side == Face.WEST) {
            structure = new String[] { "0,0,0", "0,0,-1", "0,0,1", "0,1,-1",
                    "0,1,1", "0,2,-1", "0,2,0", "0,2,1", "0,1,0" };
        }
        Bukkit.broadcastMessage("Face: " + side);
        return structure;
    }

    @Override
    public boolean validation(Block block, Player player) {
        Face side = StructureHandler.getPlayerFacing(player);
        Face checkSideLeft, checkSideRight;
        if (side == Face.NORTH || side == Face.SOUTH) {
            checkSideLeft = Face.WEST;
            checkSideRight = Face.EAST;
        } else {
            checkSideLeft = Face.NORTH;
            checkSideRight = Face.SOUTH;
        }
        if (StructureHandler.isNearByBlockVaild(block, checkSideLeft, 2,
                Material.WOOL, (short) 1)
                && StructureHandler.isNearByBlockVaild(block, checkSideRight,
                        2, Material.WOOL, (short) 7)
                && StructureHandler.isNearByBlockVaild(block, Face.DOWN, 1,
                        Material.WOOL, (short) 6)
                && StructureHandler.isSurroundedBy(block, 1, Material.AIR,
                        (short) 0)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean mutltiTypeStructure() {
        return false;
    }

    @Override
    public boolean destroyable() {
        return true;
    }

    @Override
    public int getHealth() {
        return 400;
    }
}
