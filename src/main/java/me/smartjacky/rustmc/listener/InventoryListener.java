package me.smartjacky.rustmc.listener;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.crafting.CraftingHandler;
import me.smartjacky.rustmc.inventory.InventoryHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;

public class InventoryListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            final Player player = (Player) e.getWhoClicked();
            if (InventoryHandler
                    .isCraftingInventory(e.getInventory().getName())) {
                if (e.getCurrentItem() != null
                        && Registry.getInventoryRegistry().containsKey(
                                e.getInventory().getName())) {
                    final String[] action = Registry
                            .getInventoryRegistry()
                            .get(e.getInventory().getName())
                            .getAction(e.getCurrentItem().getType(),
                                    e.getCurrentItem().getDurability());
                    if (action != null && action[0] == "craft") {
                        CraftingHandler.craft(player,
                                Material.getMaterial(action[1]),
                                Short.parseShort(action[2]));
                        player.closeInventory();
                    } else if (action != null && action[0] == "open") {
                        player.openInventory(InventoryHandler.getInventory(
                                action[1], player));
                    } else if (action != null && action[0] == "unlock") {
                        if (!player.hasPermission(Registry
                                .getRecipeRegistry()
                                .get(ListUtil.convertToKey(
                                        Material.getMaterial(action[1]),
                                        Short.parseShort(action[2])))
                                .permission())) {
                            boolean hasItem = false;
                            for (final ItemStack inventoryItem : player
                                    .getInventory().getContents()) {
                                if (inventoryItem != null
                                        && inventoryItem.hasItemMeta()
                                        && inventoryItem.getItemMeta()
                                                .hasDisplayName()
                                        && inventoryItem
                                                .getItemMeta()
                                                .getDisplayName()
                                                .contains(
                                                        ItemUtil.getItemDisplayName(
                                                                Material.getMaterial(action[1]),
                                                                Short.parseShort(action[2])))) {
                                    Bukkit.getScheduler()
                                            .scheduleSyncDelayedTask(
                                                    RustMC.instance(),
                                                    new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            player.getInventory()
                                                                    .removeItem(
                                                                            inventoryItem);
                                                            ItemStack researchKit = new ItemStack(
                                                                    Material.CLAY_BRICK,
                                                                    1);
                                                            ItemMeta im = researchKit
                                                                    .getItemMeta();
                                                            im.setDisplayName(ChatColor.YELLOW
                                                                    + "Research Kit");
                                                            researchKit
                                                                    .setItemMeta(im);
                                                            player.getInventory()
                                                                    .removeItem(
                                                                            researchKit);
                                                        }
                                                    }, 1L);
                                    player.addAttachment(
                                            RustMC.instance(),
                                            Registry.getRecipeRegistry()
                                                    .get(ListUtil.convertToKey(
                                                            Material.getMaterial(action[1]),
                                                            Short.parseShort(action[2])))
                                                    .permission(), true);
                                    player.sendMessage(ChatUtil.format("%rustmc-prefix%&aYou can now craft "
                                            + ItemUtil.getItemDisplayName(
                                                    Material.getMaterial(action[1]),
                                                    Short.parseShort(action[2]))));
                                    hasItem = true;
                                }
                            }
                            if (!hasItem) {
                                player.sendMessage(ChatUtil
                                        .format("%rustmc-prefix%&cYou need to have this item in your inventory to unlock it"));
                            }
                        } else {
                            player.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&cYou have already unlocked this item"));
                        }
                        player.closeInventory();
                    }
                }
                e.setCancelled(true);
                player.playSound(player.getLocation(), Sound.CLICK, 1F, 1F);
            } else if (e.getInventory().getName().endsWith("Weapon Case")) {
                e.setCancelled(true);
            }
            if (e.getCurrentItem() != null
                    && e.getCurrentItem().getType() == Material.PAPER
                    && e.getCurrentItem().hasItemMeta()
                    && e.getCurrentItem().getItemMeta().getDisplayName()
                            .endsWith("Crafting Menu")) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onChestClose(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();
        if (e.getInventory().getType() == InventoryType.CHEST
                && e.getInventory().getName() == "container.chest") {
            Location loc = null;
            World world = player.getWorld();
            for (MetadataValue val : player.getMetadata("SUPPLY_DROP")) {
                loc = (Location) val.value();
            }
            e.getInventory().clear();
            world.getBlockAt(loc).setType(Material.AIR);
            player.removeMetadata("SUPPLY_DROP", RustMC.instance());
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryDrag(InventoryDragEvent e) {
        if (e.getOldCursor() != null
                && e.getOldCursor().getType() == Material.PAPER
                && e.getOldCursor().hasItemMeta()
                && e.getOldCursor().getItemMeta().getDisplayName()
                        .endsWith("Crafting Menu")) {
            e.setCancelled(true);
        }
    }
}
