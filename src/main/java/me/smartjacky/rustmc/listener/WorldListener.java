package me.smartjacky.rustmc.listener;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.misc.ExplosionHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class WorldListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityExplode(EntityExplodeEvent e) {
        int blockDamage = 0;
        if (ExplosionHandler.hasExplosionOccured(e.getLocation())) {
            blockDamage = ExplosionHandler.getDamage(e.getLocation());
            ExplosionHandler.removeExplosion(e.getLocation());
        }
        for (Block block : e.blockList()) {
            if (StructureHandler.isStucture(block.getType(), block.getState()
                    .getData().toItemStack().getDurability())) {
                int blockHealth = Registry
                        .getStructureRegistry()
                        .get(ListUtil.convertToKey(block.getType(), block
                                .getState().getData().toItemStack()
                                .getDurability())).getHealth();
                if (block.hasMetadata("HEALTH")) {
                    for (MetadataValue val : block.getMetadata("HEALTH")) {
                        blockHealth = (Integer) val.value();
                    }
                    if (blockHealth - blockDamage <= 0) {
                        StructureHandler.destroy(block, null);
                    } else {
                        block.setMetadata("HEALTH", new FixedMetadataValue(
                                RustMC.instance(), blockHealth - blockDamage));
                    }
                } else {
                    block.setMetadata("HEALTH",
                            new FixedMetadataValue(RustMC.instance(),
                                    blockHealth));
                }
            }
        }
        e.blockList().clear();
    }
}
