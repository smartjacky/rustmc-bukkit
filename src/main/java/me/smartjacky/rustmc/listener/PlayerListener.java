package me.smartjacky.rustmc.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.command.CommandBuildMode;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.consumable.ConsumableHandler;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.inventory.InventoryHandler;
import me.smartjacky.rustmc.misc.ExplosionHandler;
import me.smartjacky.rustmc.misc.MiscItemHandler;
import me.smartjacky.rustmc.misc.WeaponCaseHandler;
import me.smartjacky.rustmc.misc.WeaponCaseHandler.WeaponCase;
import me.smartjacky.rustmc.packet.PacketHandler;
import me.smartjacky.rustmc.projectile.ProjectileHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.tool.ToolHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.FileUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.PlayerUtil;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Button;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent e) {
        if (!CommandBuildMode.getBuildModePlayers().get(e.getPlayer())) {
            if (e.getBlock().getType() != Material.CHEST
                    || e.getBlock().getType() != Material.BED) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!CommandBuildMode.getBuildModePlayers().get(e.getPlayer())) {
            Player player = e.getPlayer();
            Material blockType = e.getBlock().getType();
            short blockData = e.getBlock().getState().getData().toItemStack()
                    .getDurability();
            if (player.getItemInHand().getType() == Material.CHEST
                    && player.getItemInHand().getItemMeta().getDisplayName()
                            .endsWith("Add Lootable")) {
                FileConfiguration config = RustMC.instance().getConfig();
                List<String> lootables = config.getStringList("lootables.list");
                Location location = player.getLocation();

                int x = location.getBlockX(), y = location.getBlockY(), z = location
                        .getBlockZ();
                if (!lootables.contains(x + "," + y + "," + z)) {
                    if (lootables.isEmpty()) {
                        List<String> newList = new ArrayList<String>();
                        newList.add(x + "," + y + "," + z);
                        config.set("lootables.list", newList);
                    } else {
                        lootables.add(x + "," + y + "," + z);
                    }
                    player.sendMessage(ChatUtil
                            .titleFormat("&aNew lootable has been added"));
                }
                e.setCancelled(true);
            } else if (StructureHandler.isStucture(e.getBlock())
                    && StructureHandler.isVaild(e.getBlock(), e.getPlayer(),
                            blockType, blockData)) {
                StructureHandler.build(e.getBlock(), e.getPlayer());
            } else if (blockType == Material.STONE_BUTTON) {
                Button button = (Button) e.getBlock().getState().getData();
                BlockFace face = button.getAttachedFace();
                Block attachedBlock = e.getBlock().getRelative(face);
                if (StructureHandler.isStucture(attachedBlock)) {
                    if (!player.hasMetadata("C4_LOCATION")) {
                        PlayerUtil.setMetadata(player, "C4_LOCATION",
                                new Block[] { e.getBlock() });
                    } else {
                        ArrayList<Block> buttonList = new ArrayList<Block>();
                        Block[] buttons = (Block[]) PlayerUtil.getMetadata(
                                player, "C4_LOCATION");
                        for (Block block : buttons) {
                            buttonList.add(block);
                        }
                        buttonList.add(e.getBlock());
                        PlayerUtil
                                .setMetadata(player, "C4_LOCATION", buttonList
                                        .toArray(new Block[buttonList.size()]));
                    }
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCraftItem(CraftItemEvent e) {
        if (!CommandBuildMode.getBuildModePlayers().get(
                (Player) e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.NORMAL)
    public void onMaterialHarvest(BlockBreakEvent e) {
        Player player = e.getPlayer();
        Block block = e.getBlock();
        if (ToolHandler.isTool(player.getItemInHand().getType())) {
            if (block.getType() == Material.LOG && block.getData() == (short) 3) {
                ToolHandler.onWoodHarvest(player, block);
                e.setCancelled(true);
            } else if (block.getType() == Material.GOLD_ORE
                    || block.getType() == Material.IRON_ORE) {
                ToolHandler.onMetalHarvest(player, block);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onChestOpen(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK
                && e.getClickedBlock().getType() == Material.CHEST) {
            Chest chest = (Chest) e.getClickedBlock().getState();
            if (chest.getInventory().getName() == "container.chest") {
                e.getPlayer().setMetadata(
                        "SUPPLY_DROP",
                        new FixedMetadataValue(RustMC.instance(), chest
                                .getLocation()));
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onWoodChop(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        Block block = e.getClickedBlock();
        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (ToolHandler.isTool(player.getItemInHand().getType())) {
                if (block.getType() == Material.LOG
                        && block.getState().getData().toItemStack()
                                .getDurability() == 0) {
                    ToolHandler.onWoodChop(player, block);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemConsume(PlayerInteractEvent e) {
        final Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_AIR
                || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (ConsumableHandler
                    .isConsumable(player.getItemInHand().getType())) {
                ConsumableHandler.consume(player.getItemInHand().getType(),
                        player);
            } else if (player.getItemInHand().getType() == Material.PAPER
                    && player.getItemInHand().hasItemMeta()
                    && player.getItemInHand().getItemMeta().getDisplayName()
                            .endsWith("Crafting Menu")) {
                player.openInventory(InventoryHandler.getInventory("Menu",
                        player));
            } else if (player.getItemInHand().getType() == Material.CLAY_BRICK
                    && player.getItemInHand().hasItemMeta()
                    && player.getItemInHand().getItemMeta().getDisplayName()
                            .endsWith("Research Kit")) {
                player.openInventory(InventoryHandler.getInventory(
                        "Research Kit", player));
            } else if (ProjectileHandler.isProjectile(player.getItemInHand()
                    .getType(), player.getItemInHand().getDurability())) {
                ProjectileHandler.createProjectile(player);
            } else if (MiscItemHandler.isMiscItem(player.getItemInHand()
                    .getType(), player.getItemInHand().getDurability())
                    && player.getItemInHand().hasItemMeta()
                    && player.getItemInHand().getItemMeta().getDisplayName()
                            .endsWith("C4 Detonator")) {
                if (player.hasMetadata("C4_LOCATION")) {
                    Block[] buttons = (Block[]) PlayerUtil.getMetadata(player,
                            "C4_LOCATION");
                    for (Block block : buttons) {
                        Button button = (Button) block.getState().getData();
                        Block attachedBlock = block.getRelative(button
                                .getAttachedFace());
                        block.setType(Material.AIR);
                        ExplosionHandler.addExplosion(
                                attachedBlock.getLocation(), 100);
                        block.getWorld().createExplosion(
                                attachedBlock.getLocation(), 1);

                        player.removeMetadata("C4_LOCATION", RustMC.instance());
                    }
                } else {
                    player.sendMessage(ChatUtil
                            .titleFormat("&cYou have not placed any C4"));
                }
            } else if (e.getItem().getType() == Material.IRON_BARDING) {
                e.getPlayer().openInventory(
                        WeaponCaseHandler.getWeaponCase(WeaponCase.REGULAR,
                                e.getPlayer()));
                e.getPlayer().getInventory()
                        .removeItem(e.getPlayer().getItemInHand());
            } else if (e.getItem().getType() == Material.GOLD_BARDING) {
                e.getPlayer().openInventory(
                        WeaponCaseHandler.getWeaponCase(WeaponCase.RARE,
                                e.getPlayer()));
                e.getPlayer().getInventory()
                        .removeItem(e.getPlayer().getItemInHand());
            } else if (e.getItem().getType() == Material.DIAMOND_BARDING) {
                e.getPlayer().openInventory(
                        WeaponCaseHandler.getWeaponCase(WeaponCase.ELITE,
                                e.getPlayer()));
                e.getPlayer().getInventory()
                        .removeItem(e.getPlayer().getItemInHand());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRegainHealth(EntityRegainHealthEvent e) {
        if (e.getRegainReason() == RegainReason.SATIATED) {
            if (e.getEntity() instanceof Player) {
                Player player = (Player) e.getEntity();
                if (!player.hasMetadata("BLEEDING")) {
                    if (player.getFoodLevel() > 400) {
                        if (player.hasMetadata("COMFORT")) {
                            e.setAmount(1);
                        } else {
                            e.setAmount(0.5);
                        }
                    } else {
                        e.setAmount(0);
                    }
                } else {
                    e.setAmount(0);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerPortal(PlayerPortalEvent e) {
        if (!CommandBuildMode.getBuildModePlayers().get(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onGunShot(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
                && player.getItemInHand().getType() == Material.INK_SACK) {
            if (player.isSneaking()) {
                ItemStack gun = player.getItemInHand();
                GunHandler.reloadGun(player, gun);
            } else {
                GunHandler.fire(player, e.getAction());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (!FileUtil.hasData(player.getUniqueId().toString())) {
            if (!ConfigurationHandler.mysqlDatabaseEnabled) {
                FileUtil.createUserData(e.getPlayer().getUniqueId().toString());
                player.getInventory().clear();
                player.setFoodLevel(1000);
                player.setMaxHealth(100);
                player.setHealth(100);
            }
            player.getInventory()
                    .addItem(
                            ItemUtil.setItemDisplayName(new ItemStack(
                                    Material.WOOD_AXE)));
            player.getInventory().addItem(
                    ItemUtil.setItemDisplayName(new ItemStack(
                            Material.GOLD_RECORD, 2)));
            player.getInventory().setItem(
                    8,
                    ItemUtil.setItemDisplayName(new ItemStack(Material.PAPER),
                            "&eCrafting Menu"));
        }
        CommandBuildMode.getBuildModePlayers().put(player, false);
        player.setMetadata("DELAY_KEY",
                new FixedMetadataValue(RustMC.instance(), 0));
        int delayTask = Bukkit.getServer().getScheduler()
                .scheduleSyncRepeatingTask(RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        int delay = (Integer) GunHandler.getPlayerMetadata(
                                player, "DELAY_KEY");
                        if (delay > 0) {
                            player.setMetadata("DELAY_KEY",
                                    new FixedMetadataValue(RustMC.instance(),
                                            delay - 1));
                        }
                    }
                }, 0L, 5L);
        player.setMetadata("DELAY_TASK",
                new FixedMetadataValue(RustMC.instance(), delayTask));
        player.setMetadata("RECOIL", new FixedMetadataValue(RustMC.instance(),
                0.0));
        player.setMetadata(
                "RADIATION",
                new FixedMetadataValue(RustMC.instance(), (Integer) FileUtil
                        .getPlayerData(e.getPlayer().getUniqueId().toString(),
                                "radiation")));
        try {
            this.setHeader(player);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        player.setMaximumNoDamageTicks(0);
        if (ConfigurationHandler.joinMessageEnabled) {
            e.setJoinMessage(ChatUtil.format(ConfigurationHandler.joinMessage,
                    player));
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerLeave(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        int gunDelay = 0;
        int radiation = 0;
        List<MetadataValue> gunDelayList = e.getPlayer().getMetadata(
                "DELAY_TASK");
        for (MetadataValue val : gunDelayList) {
            gunDelay = (Integer) val.value();
        }
        List<MetadataValue> radiationList = e.getPlayer().getMetadata(
                "RADIATION");
        for (MetadataValue val : radiationList) {
            radiation = (Integer) val.value();
        }
        Bukkit.getServer().getScheduler().cancelTask(gunDelay);
        if (ConfigurationHandler.quitMessageEnabled) {
            e.setQuitMessage(ChatUtil.format(ConfigurationHandler.quitMessage,
                    player));
        }
        FileUtil.setPlayerData(e.getPlayer().getUniqueId().toString(),
                "radiation", radiation);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Snowball) {
            Snowball snowball = (Snowball) e.getDamager();
            if (e.getEntity() instanceof Player) {
                Player player = (Player) e.getEntity();
                Player shooter = (Player) snowball.getShooter();
                if (player.getDisplayName() == shooter.getDisplayName()) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        final Player player = e.getPlayer();
        Bukkit.getServer().getScheduler()
                .scheduleSyncDelayedTask(RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        if (player.getBedSpawnLocation() == null) {
                            World world = player.getWorld();
                            Random rand = new Random();
                            int x = rand.nextInt(1000);
                            int z = rand.nextInt(1000);
                            Location RespawnLocation = new Location(world, x,
                                    world.getHighestBlockYAt(x, z), z);
                            player.teleport(RespawnLocation);
                        }
                        player.getInventory().clear();
                        player.setFoodLevel(1000);
                        String[] respawnItems = ConfigurationHandler
                                .getItems("respawnItems");
                        int[] respawnItemsAmount = { 1, 2 };
                        for (int i = 0; i < respawnItems.length; i++) {
                            ItemStack respawnItem = new ItemStack(Material
                                    .getMaterial(respawnItems[i]),
                                    respawnItemsAmount[i]);
                            ItemMeta im = respawnItem.getItemMeta();
                            if (ConsumableHandler.isConsumable(respawnItem
                                    .getType())) {
                                im.setDisplayName(ChatColor.YELLOW
                                        + Registry.getConsumableRegistry()
                                                .get(respawnItem.getType())
                                                .getDisplayName());
                            } else if (ToolHandler.isTool(respawnItem.getType())) {
                                im.setDisplayName(ChatColor.YELLOW
                                        + Registry.getToolRegistry()
                                                .get(respawnItem.getType())
                                                .getDisplayName());
                            }
                            respawnItem.setItemMeta(im);
                            player.getInventory().addItem(respawnItem);
                        }
                        player.setExp(0);
                        if (player.hasMetadata("RAIDIATING")) {
                            player.removeMetadata("RADIATING",
                                    RustMC.instance());
                        }
                        if (player.hasMetadata("BLEEDING")) {
                            player.removeMetadata("BLEEDING", RustMC.instance());
                        }
                        player.setMetadata("RADIATION", new FixedMetadataValue(
                                RustMC.instance(), 0));
                        ItemStack craftingMenu = new ItemStack(Material.PAPER,
                                1);
                        ItemMeta im = craftingMenu.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&eCrafting Menu"));
                        craftingMenu.setItemMeta(im);
                        player.getInventory().setItem(8, craftingMenu);
                    }
                }, 1L);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        if (e.getItemDrop().getItemStack().getType() == Material.INK_SACK) {
            ItemStack item = e.getItemDrop().getItemStack();
            ItemMeta im = item.getItemMeta();
            if (im.hasEnchants()) {
                e.setCancelled(true);
                e.getPlayer()
                        .sendMessage(
                                ChatUtil.format("%rustmc-prefix% &cYou cannot throw your weapon while reloading."));
            }
        } else if (e.getItemDrop().getItemStack().getType() == Material.PAPER
                && e.getItemDrop().getItemStack().hasItemMeta()
                && e.getItemDrop().getItemStack().getItemMeta()
                        .getDisplayName().endsWith("Crafting Menu")) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (e.getCause() == DamageCause.FALL) {
                if (player.getFallDistance() >= 10) {
                    player.addPotionEffect(new PotionEffect(
                            PotionEffectType.SLOW, 600, 2));
                }
                e.setDamage((player.getFallDistance() - 3) * 5);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
    }

    private void setHeader(Player player) throws Exception {
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        PacketHandler.setValue(packet, "a", ChatUtil
                .toIChatBaseComponet(ChatUtil.jsonConverter("&c&lRustMC")));
        PacketHandler.setValue(packet, "b", ChatUtil
                .toIChatBaseComponet(ChatUtil
                        .jsonConverter("&aWelcome to RustMC")));
        PacketHandler.sendPacket(player, packet);
    }
}
