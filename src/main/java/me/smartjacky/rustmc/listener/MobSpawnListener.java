package me.smartjacky.rustmc.listener;

import me.smartjacky.rustmc.entity.RustMCEntityWolf;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class MobSpawnListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        Entity entity = e.getEntity();
        EntityType creatureType = e.getEntityType();
        net.minecraft.server.v1_8_R3.Entity mcEntity = (((CraftEntity) entity)
                .getHandle());
        if (creatureType == EntityType.WOLF
                && mcEntity instanceof RustMCEntityWolf) {
        } else if (creatureType == EntityType.PIG) {

        } else if (creatureType == EntityType.CHICKEN) {

        } else {
            e.setCancelled(true);
        }
    }
}
