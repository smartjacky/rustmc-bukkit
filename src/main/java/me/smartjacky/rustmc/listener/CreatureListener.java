package me.smartjacky.rustmc.listener;

import java.util.List;
import java.util.Random;
import java.util.Set;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.armor.ArmorHandler;
import me.smartjacky.rustmc.armor.ArmorHandler.DamageType;
import me.smartjacky.rustmc.consumable.ConsumableHandler;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.misc.AirdropHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.tool.ToolHandler;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CreatureListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityCombust(EntityCombustEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntityType() == EntityType.ZOMBIE) {
            List<ItemStack> drops = e.getDrops();
            drops.clear();
            FileConfiguration config = RustMC.instance().getConfig();
            Set<String> mobDrops = config
                    .getConfigurationSection("zombieDrops").getKeys(false);
            int foodDropAmount = 0;
            for (String mobDrop : mobDrops) {
                Material material = Material.getMaterial(mobDrop);
                if (new Random().nextInt(100) <= config.getInt("zombieDrops."
                        + mobDrop + ".rarity")) {
                    ItemStack item = new ItemStack(material,
                            new Random().nextInt((config.getInt("zombieDrops."
                                    + mobDrop + ".amount")) + 1));
                    ItemMeta im = item.getItemMeta();
                    if (GunHandler.isGun(item.getType(), item.getDurability())) {
                        int gunType = new Random().nextInt(4);
                        item.setDurability((short) gunType);
                        GunHandler.setGun(item);
                    } else if (StructureHandler.isStucture(item.getType(),
                            item.getDurability())) {
                        item.setDurability((short) new Random().nextInt(3));
                    } else {
                        im.setDisplayName(ChatColor.YELLOW
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability()));
                        item.setItemMeta(im);
                    }
                    if (ConsumableHandler.isConsumable(material)) {
                        if (foodDropAmount <= 32) {
                            drops.add(item);
                            foodDropAmount++;
                        }
                    } else {
                        drops.add(item);
                    }
                }
            }
        } else if (!(e.getEntity() instanceof Player)) {
            FileConfiguration config = RustMC.instance().getConfig();
            Set<String> mobDrops = config
                    .getConfigurationSection("animalDrops").getKeys(false);
            List<ItemStack> drops = e.getDrops();
            drops.clear();
            for (String mobDrop : mobDrops) {
                if (new Random().nextInt(100) <= config.getInt("animalDrops."
                        + mobDrop + ".rarity")) {
                    Material material = Material.getMaterial(mobDrop);
                    ItemStack item = new ItemStack(material,
                            new Random().nextInt(config.getInt("animalDrops."
                                    + mobDrop + ".amount") - 1) + 1);
                    ItemMeta im = item.getItemMeta();
                    if (GunHandler.isGun(item.getType(), item.getDurability())) {
                        int gunType = new Random().nextInt(4);
                        item.setDurability((short) gunType);
                        GunHandler.setGun(item);
                    } else if (StructureHandler.isStucture(item.getType(),
                            item.getDurability())) {
                        item.setDurability((short) new Random().nextInt(3));
                    } else {
                        im.setDisplayName(ChatColor.YELLOW
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability()));
                        item.setItemMeta(im);
                    }
                    item.setItemMeta(im);
                    drops.add(item);
                }
            }
        } else if (e.getEntity() instanceof Player) {
            for (ItemStack item : e.getDrops()) {
                if (item != null
                        && item.getType() == Material.PAPER
                        && item.hasItemMeta()
                        && item.getItemMeta().getDisplayName()
                                .endsWith("Crafting Menu")) {
                    item.setType(Material.AIR);
                }
            }
        }
        e.setDroppedExp(0);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Snowball) {
            int damage = 0;
            List<MetadataValue> metadata = e.getDamager().getMetadata(
                    "DAMAGE_KEY");
            for (MetadataValue val : metadata) {
                damage = (Integer) val.value();
            }
            if (e.getEntity() instanceof LivingEntity) {
                LivingEntity entity = (LivingEntity) e.getEntity();
                if (e.getEntity() instanceof Player) {
                    entity.damage(ArmorHandler.getDamage(damage,
                            (Player) e.getEntity(), DamageType.GUN));
                } else {
                    entity.damage(damage);
                }
                entity.setVelocity(e.getDamager().getVelocity().multiply(0.1));
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Zombie) {
            if (e.getEntity() instanceof Player) {
                Player player = (Player) e.getEntity();
                player.damage(ArmorHandler.getDamage(20, player,
                        DamageType.MELEE));
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Wolf) {
            if (e.getEntity() instanceof Player) {
                Player player = (Player) e.getEntity();
                player.damage(ArmorHandler.getDamage(20, player,
                        DamageType.MELEE));
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Player) {
            Player player = (Player) e.getDamager();
            if (ToolHandler.isTool(player.getItemInHand().getType())) {
                if (!player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                    if (e.getEntity() instanceof LivingEntity) {
                        LivingEntity entity = (LivingEntity) e.getEntity();
                        entity.damage(ArmorHandler.getDamage(
                                Registry.getToolRegistry()
                                        .get(player.getItemInHand().getType())
                                        .getDamage(), player, DamageType.MELEE));
                        player.addPotionEffect(new PotionEffect(
                                PotionEffectType.SLOW_DIGGING, Registry
                                        .getToolRegistry()
                                        .get(player.getItemInHand().getType())
                                        .getDelay() * 20, 7));
                        ToolHandler.damage(player.getItemInHand());
                        e.setCancelled(true);
                    }
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
        }
        if (e.getEntity() instanceof Player) {
            bleed((Player) e.getEntity());
        }
    }

    public void bleed(Player player) {
        int randInt = new Random().nextInt(2);
        if (randInt == 1) {
            player.setMetadata("BLEEDING",
                    new FixedMetadataValue(RustMC.instance(), true));
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.ENTITY_EXPLOSION) {
            if (e.getEntity() instanceof LivingEntity) {
                LivingEntity entity = (LivingEntity) e.getEntity();
                entity.damage(ArmorHandler.getDamage(e.getDamage(), entity,
                        DamageType.EXPLOSION));
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        e.getEntity().setMaximumNoDamageTicks(0);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityChangeBlock(final EntityChangeBlockEvent e) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        World world = e.getBlock().getWorld();
                        if (world.getBlockAt(e.getBlock().getLocation())
                                .getType() == Material.CHEST) {
                            AirdropHandler.addChestContent((Chest) e.getBlock()
                                    .getState());
                        }
                    }
                }, 5L);
    }
}
