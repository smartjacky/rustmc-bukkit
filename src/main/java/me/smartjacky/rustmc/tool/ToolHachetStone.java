package me.smartjacky.rustmc.tool;

import me.smartjacky.rustmc.api.ITool;

public class ToolHachetStone implements ITool {
    @Override
    public int getDurability() {
        return 131;
    }

    @Override
    public int getDamage() {
        return 10;
    }

    @Override
    public int getDelay() {
        return 1;
    }

    @Override
    public String getDisplayName() {
        return "Stone Hatchet";
    }

    @Override
    public boolean isBreakable() {
        return true;
    }

    @Override
    public int getTreeObtainAmount() {
        return 2;
    }

    @Override
    public int getWoodPileObtainAmount() {
        return 2;
    }

    @Override
    public int getMetalObtainAmount() {
        return 0;
    }
}
