package me.smartjacky.rustmc.tool;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.ITool;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ToolHandler {
    private static HashMap<List<Integer>, Integer> woodData = null;

    public static void init() {
        Registry.registerTool(Material.WOOD_AXE, new ToolRock());
        Registry.registerTool(Material.STONE_AXE, new ToolHachetStone());
        Registry.registerTool(Material.IRON_AXE, new ToolHatchet());
        Registry.registerTool(Material.IRON_PICKAXE, new ToolPickaxe());
        Registry.registerTool(Material.DIAMOND_PICKAXE, new ToolAnniversaryPickaxe());
        ToolHandler.woodData = new HashMap<List<Integer>, Integer>();
    }

    public static void resetAllTreeData() {
        for (List<Integer> key : ToolHandler.woodData.keySet()) {
            ToolHandler.woodData.put(key, 7);
        }
    }

    public static void onWoodChop(final Player player, final Block block) {
        if (ToolHandler.woodData.containsKey(ListUtil.convertToKey(
                block.getX(), block.getZ()))) {
            Integer woodData = (Integer) ToolHandler.woodData.get(ListUtil
                    .convertToKey(block.getX(), block.getZ()));
            if (woodData <= 0) {
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cThere's no wood left on this tree."));
            } else {
                Material itemType = player.getItemInHand().getType();
                if (!player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                    if (player.getItemInHand().getDurability() < Registry
                            .getToolRegistry().get(itemType).getDurability()) {
                        player.getInventory().addItem(
                                new ItemStack(Material.WOOD, Registry
                                        .getToolRegistry().get(itemType)
                                        .getTreeObtainAmount()));
                        player.addPotionEffect(new PotionEffect(
                                PotionEffectType.SLOW_DIGGING, Registry
                                        .getToolRegistry().get(itemType)
                                        .getDelay() * 20, 7));
                        player.sendMessage(ChatUtil.format(Registry
                                .getToolRegistry().get(itemType)
                                .getTreeObtainAmount()
                                + "x Wood"));
                        ToolHandler.woodData.put(
                                ListUtil.convertToKey(block.getX(),
                                        block.getZ()), woodData - 1);
                        if (Registry.getToolRegistry().get(itemType)
                                .isBreakable()) {
                            damage(player.getItemInHand());
                        }
                    } else {
                        onToolBreaks(player);
                    }
                }
            }
        } else {
            ToolHandler.woodData.put(
                    ListUtil.convertToKey(block.getX(), block.getZ()), 7);
            onWoodChop(player, block);
        }
    }

    public static void onWoodHarvest(final Player player, Block block) {
        Material itemType = player.getItemInHand().getType();
        if (!player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
            if (!Registry.getToolRegistry().get(itemType).isBreakable()
                    || player.getItemInHand().getDurability() < Registry
                            .getToolRegistry().get(itemType).getDurability()) {
                if (player.getItemInHand().getDurability() < Registry
                        .getToolRegistry().get(itemType).getDurability()) {
                    player.getInventory().addItem(
                            new ItemStack(Material.WOOD, Registry
                                    .getToolRegistry().get(itemType)
                                    .getWoodPileObtainAmount()));
                    player.addPotionEffect(new PotionEffect(
                            PotionEffectType.SLOW_DIGGING,
                            Registry.getToolRegistry().get(itemType).getDelay() * 20,
                            7));
                    player.sendMessage(Registry.getToolRegistry().get(itemType)
                            .getWoodPileObtainAmount()
                            + "x Wood");
                    List<Entity> nearbyEntities = player.getNearbyEntities(5,
                            5, 5);
                    for (Entity entity : nearbyEntities) {
                        if (entity instanceof Player) {
                            Player nearbyPlayer = (Player) entity;
                            nearbyPlayer.playSound(player.getLocation(),
                                    Sound.ITEM_BREAK, 1, 1);
                        }
                    }
                    block.setType(Material.AIR);
                    damage(player.getItemInHand());
                }
            } else {
                onToolBreaks(player);
            }
        }
    }

    public static void onMetalHarvest(final Player player, Block block) {
        Material itemType = player.getItemInHand().getType();
        if (!player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
            if (!Registry.getToolRegistry().get(itemType).isBreakable()
                    || player.getItemInHand().getDurability() < Registry
                            .getToolRegistry().get(itemType).getDurability()) {
                if (block.getType() == Material.IRON_ORE
                        && player.getItemInHand().getDurability() < Registry
                                .getToolRegistry().get(itemType)
                                .getDurability()) {
                    Random rand = new Random();
                    int oreId = rand.nextInt(3);
                    Material[] ore = new Material[] { Material.IRON_ORE,
                            Material.GOLD_ORE, Material.STONE };
                    String[] oreName = new String[] { "Metal Ore",
                            "Sulfer Ore", "Stone" };
                    player.getInventory().addItem(
                            new ItemStack(ore[oreId], Registry
                                    .getToolRegistry().get(itemType)
                                    .getMetalObtainAmount()));
                    player.addPotionEffect(new PotionEffect(
                            PotionEffectType.SLOW_DIGGING,
                            Registry.getToolRegistry().get(itemType).getDelay() * 20,
                            7));
                    player.sendMessage(Registry.getToolRegistry().get(itemType)
                            .getMetalObtainAmount()
                            + "x" + oreName[oreId]);
                    damage(player.getItemInHand());
                }
            } else {
                onToolBreaks(player);
            }
        }
    }

    public static void onToolBreaks(final Player player) {
        Bukkit.getServer().getScheduler()
                .scheduleSyncDelayedTask(RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        player.getInventory().remove(
                                new ItemStack(player.getItemInHand().getType(),
                                        1));
                        player.playSound(player.getLocation(),
                                Sound.ITEM_BREAK, 1, 1);
                    }
                }, 1L);
    }

    public static void damage(ItemStack itemstack) {
        if (Registry.getToolRegistry().get(itemstack.getType()).isBreakable()) {
            itemstack
                    .setDurability((short) (itemstack.getDurability() + (short) 1));
        }
    }

    public static boolean isTool(Material key) {
        if (Registry.getToolRegistry().containsKey(key)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isTool(String string) {
        for (ITool tool : Registry.getToolRegistry().values()) {
            if (tool.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getTool(String string) {
        for (ITool tool : Registry.getToolRegistry().values()) {
            if (tool.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<Material, ITool> entry : Registry.getToolRegistry()
                        .entrySet()) {
                    if (entry.getValue() == tool) {
                        ItemStack item = new ItemStack(
                                (Material) entry.getKey(), 1);
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
