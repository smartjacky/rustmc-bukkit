package me.smartjacky.rustmc.tool;

import me.smartjacky.rustmc.api.ITool;

public class ToolPickaxe implements ITool {
    @Override
    public int getDurability() {
        return 250;
    }

    @Override
    public int getDamage() {
        return 20;
    }

    @Override
    public int getDelay() {
        return 3;
    }

    @Override
    public String getDisplayName() {
        return "Pickaxe";
    }

    @Override
    public boolean isBreakable() {
        return true;
    }

    @Override
    public int getTreeObtainAmount() {
        return 2;
    }

    @Override
    public int getWoodPileObtainAmount() {
        return 3;
    }

    @Override
    public int getMetalObtainAmount() {
        return 0;
    }
}
