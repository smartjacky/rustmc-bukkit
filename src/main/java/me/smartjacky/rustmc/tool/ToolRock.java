package me.smartjacky.rustmc.tool;

import me.smartjacky.rustmc.api.ITool;

public class ToolRock implements ITool {
    @Override
    public int getDurability() {
        return 1;
    }

    @Override
    public int getDamage() {
        return 5;
    }

    @Override
    public int getDelay() {
        return 3;
    }

    @Override
    public String getDisplayName() {
        return "Rock";
    }

    @Override
    public boolean isBreakable() {
        return false;
    }

    @Override
    public int getTreeObtainAmount() {
        return 1;
    }

    @Override
    public int getWoodPileObtainAmount() {
        return 1;
    }

    @Override
    public int getMetalObtainAmount() {
        return 0;
    }
}
