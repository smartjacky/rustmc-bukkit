package me.smartjacky.rustmc.tool;

import me.smartjacky.rustmc.api.ITool;

public class ToolAnniversaryPickaxe implements ITool {
    @Override
    public int getDurability() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int getDamage() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int getDelay() {
        return 0;
    }

    @Override
    public String getDisplayName() {
        return "&6Anniversary Pickaxe";
    }

    @Override
    public boolean isBreakable() {
        return false;
    }

    @Override
    public int getTreeObtainAmount() {
        return 999;
    }

    @Override
    public int getWoodPileObtainAmount() {
        return 999;
    }

    @Override
    public int getMetalObtainAmount() {
        return 999;
    }
}
