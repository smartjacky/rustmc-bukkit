package me.smartjacky.rustmc.task;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.ITask;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.lootable.Lootable;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.structure.StructureLootable;
import me.smartjacky.rustmc.structure.StructureHandler.Face;

public class TaskLootableRespawn implements ITask {
    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            public void run() {
                FileConfiguration config = RustMC.instance().getConfig();
                List<String> lootableLocations = config
                        .getStringList("lootables");
                for (String str : lootableLocations) {
                    int x, y, z;
                    String[] coords = str.split(",");
                    x = Integer.parseInt(coords[0]);
                    y = Integer.parseInt(coords[1]);
                    z = Integer.parseInt(coords[2]);
                    Block block = ConfigurationHandler.getWorld()
                            .getBlockAt(x, y, z);
                    if (block.getType() == Material.AIR) {
                        StructureHandler.build(x, y, z, new StructureLootable(
                                Lootable.SMALL), Face.NONE);
                    }
                    Bukkit.broadcastMessage("Lootable Location:" + x + y + z);
                }
            }
        };
    }

    @Override
    public long getPeriod() {
        return 6000L;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
