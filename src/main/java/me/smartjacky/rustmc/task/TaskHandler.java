package me.smartjacky.rustmc.task;

import org.bukkit.Bukkit;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.task.TaskStatusDecay.Status;

public class TaskHandler {
    public TaskHandler() {
        Registry.registerTask("UpdateScoreBoard", new TaskUpdateScoreboard());
        Registry.registerTask("ResetTreeData", new TaskResetTreeData());
        Registry.registerTask("RadZoneCheck", new TaskPlayerInRadZone());
        Registry.registerTask("BleedingDecay", new TaskStatusDecay(
                Status.BLEEDING));
        Registry.registerTask("RadiationDecay", new TaskStatusDecay(
                Status.RADIATION));
        Registry.registerTask("Radiate", new TaskStatusDecay(Status.RADIATE));
        Registry.registerTask("Recoil", new TaskStatusDecay(Status.RECOIL));
        run();
    }

    public static void run() {
        for (String key : Registry.getTaskRegistry().keySet()) {
            Runnable runnable = (Runnable) Registry.getTaskRegistry().get(key)
                    .getRunnable();
            Long delay = (Long) Registry.getTaskRegistry().get(key).getPeriod();
            Boolean isEnabled = (Boolean) Registry.getTaskRegistry().get(key)
                    .isEnabled();
            if (isEnabled) {
                Bukkit.getScheduler().scheduleSyncRepeatingTask(
                        RustMC.instance(), runnable, 0, delay);
            }
        }
    }
}
