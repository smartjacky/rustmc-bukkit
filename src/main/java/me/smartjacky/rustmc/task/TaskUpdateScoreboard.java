package me.smartjacky.rustmc.task;

import java.util.Collection;

import me.smartjacky.rustmc.api.ITask;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.scoreboard.ScoreboardHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TaskUpdateScoreboard implements ITask {
    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                Collection<? extends Player> onlinePlayers = Bukkit.getServer()
                        .getOnlinePlayers();
                for (Player player : onlinePlayers) {
                    ScoreboardHandler.updateScore(player);
                }
            }
        };
    }

    @Override
    public long getPeriod() {
        return 10L;
    }

    @Override
    public boolean isEnabled() {
        return ConfigurationHandler.hudEnabled;
    }
}
