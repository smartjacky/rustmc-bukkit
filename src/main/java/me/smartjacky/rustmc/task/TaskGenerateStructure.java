package me.smartjacky.rustmc.task;

import me.smartjacky.rustmc.api.ITask;

public class TaskGenerateStructure implements ITask {
    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
            }
        };
    }

    @Override
    public long getPeriod() {
        return 6000L;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
