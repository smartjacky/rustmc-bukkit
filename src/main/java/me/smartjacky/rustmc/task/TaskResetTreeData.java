package me.smartjacky.rustmc.task;

import org.bukkit.Bukkit;

import me.smartjacky.rustmc.api.ITask;
import me.smartjacky.rustmc.tool.ToolHandler;
import me.smartjacky.rustmc.util.ChatUtil;

public class TaskResetTreeData implements ITask {
    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                ToolHandler.resetAllTreeData();
                Bukkit.broadcastMessage(ChatUtil
                        .format("%rustmc-prefix%&aAll trees can now be chopped again."));
            }
        };
    }

    @Override
    public long getPeriod() {
        return 6000;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
