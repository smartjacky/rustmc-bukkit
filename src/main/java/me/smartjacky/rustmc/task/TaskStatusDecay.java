package me.smartjacky.rustmc.task;

import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.ITask;
import me.smartjacky.rustmc.radiation.RadiationHandler;

public class TaskStatusDecay implements ITask {
    public enum Status {
        RADIATION, BLEEDING, RADIATE, RECOIL
    }

    private String stats;
    private String effect;
    private Status status;

    public TaskStatusDecay(Status status) {
        this.status = status;
    }

    public void setPlayerStatus(Player player, int amount) {
        player.setMetadata(effect, new FixedMetadataValue(RustMC.instance(),
                amount));
    }

    public int getPlayerStatus(Player player) {
        if (player.hasMetadata(effect)) {
            int radiation = 0;
            if (player.hasMetadata(effect)) {
                List<MetadataValue> radiationList = player.getMetadata(effect);
                for (MetadataValue val : radiationList) {
                    radiation = (Integer) val.value();
                }
                return radiation;
            } else {
                RustMC.log.info("Cannot find metadata for " + player.getName());
            }
        }
        return 0;
    }

    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                Collection<? extends Player> onlinePlayers = Bukkit
                        .getOnlinePlayers();
                for (Player player : onlinePlayers) {
                    switch (status) {
                    case RADIATION:
                        stats = "RADIATING";
                        effect = "RADIATION";
                        if (getPlayerStatus(player) > 0
                                && !player.hasMetadata(stats)) {
                            setPlayerStatus(player, getPlayerStatus(player) - 1);
                        }
                        break;
                    case BLEEDING:
                        stats = "BLEEDING";
                        if (player.hasMetadata(stats)) {
                            if (player.getHealth() - 1 <= 0) {
                                player.setHealth(0.0D);
                            } else {
                                player.setHealth(player.getHealth() - 1);
                            }
                        }
                        break;
                    case RADIATE:
                        stats = "RADIATION";
                        if (player.hasMetadata(stats)) {
                            if (RadiationHandler.isRadiating(player)) {
                                if (player.getHealth() - 1 <= 0) {
                                    player.setHealth(0.0D);
                                } else {
                                    player.setHealth(player.getHealth() - 1);
                                }
                            }
                        }
                        break;
                    case RECOIL:
                        stats = "RECOIL";
                        if (player.hasMetadata(stats)) {
                            double recoil = 0.0;
                            for (MetadataValue val : player.getMetadata(stats)) {
                                recoil = (Double) val.value();
                            }
                            if (recoil >= 0.0) {
                                player.setMetadata(
                                        "RECOIL",
                                        new FixedMetadataValue(RustMC
                                                .instance(), recoil - 0.0125));
                            }
                        }
                    default:
                        break;
                    }
                }
            }
        };
    }

    @Override
    public long getPeriod() {
        switch (status) {
        case RADIATION:
            return 20L;
        case BLEEDING:
            return 20L;
        case RADIATE:
            return 20L;
        case RECOIL:
            return 10L;
        default:
            return 0L;
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
