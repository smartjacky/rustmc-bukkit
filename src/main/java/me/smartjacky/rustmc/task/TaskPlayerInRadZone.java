package me.smartjacky.rustmc.task;

import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.ITask;
import me.smartjacky.rustmc.radiation.RadiationHandler;

public class TaskPlayerInRadZone implements ITask {
    @Override
    public Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                Collection<? extends Player> onlinePlayers = Bukkit
                        .getOnlinePlayers();
                for (Player player : onlinePlayers) {
                    boolean inRadZone = false;
                    FileConfiguration config = RustMC.instance().getConfig();
                    ConfigurationSection radZones = config
                            .getConfigurationSection("radiationZones");
                    if (config.get("radiationZones") != null) {
                        for (String radZone : radZones.getKeys(false)) {
                            int fromX = config.getInt("radiationZones."
                                    + radZone + ".from.x");
                            int fromZ = config.getInt("radiationZones."
                                    + radZone + ".from.z");
                            int toX = config.getInt("radiationZones." + radZone
                                    + ".to.x");
                            int toZ = config.getInt("radiationZones." + radZone
                                    + ".to.z");
                            if (player.getLocation().getBlockX() <= getMaximumInt(
                                    fromX, toX)
                                    && player.getLocation().getBlockX() >= getMinimumInt(
                                            fromX, toX)) {
                                if (player.getLocation().getBlockZ() <= getMaximumInt(
                                        fromZ, toZ)
                                        && player.getLocation().getBlockZ() >= getMinimumInt(
                                                fromZ, toZ)) {
                                    inRadZone = true;
                                }
                            }
                        }
                        if (inRadZone == true) {
                            if (!player.hasMetadata("RADIATING")) {
                                RadiationHandler.radiate(player);
                            }
                        } else {
                            if (player.hasMetadata("RADIATING")) {
                                int task = 0;
                                List<MetadataValue> taskList = player
                                        .getMetadata("RADIATING");
                                for (MetadataValue val : taskList) {
                                    task = (Integer) val.value();
                                }
                                Bukkit.getScheduler().cancelTask(task);
                                player.removeMetadata("RADIATING",
                                        RustMC.instance());
                            }
                        }
                    }
                }
            }
        };
    }

    @Override
    public long getPeriod() {
        return 20L;
    }

    public static int getMaximumInt(int int1, int int2) {
        if (int1 >= int2) {
            return int1;
        } else {
            return int2;
        }
    }

    public static int getMinimumInt(int int1, int int2) {
        if (int1 <= int2) {
            return int1;
        } else {
            return int2;
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
