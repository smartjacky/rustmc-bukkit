package me.smartjacky.rustmc.scoreboard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.radiation.RadiationHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ScoreboardUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class ScoreboardHandler {
    private static ScoreboardManager manager = Bukkit.getScoreboardManager();

    public static Scoreboard createScoreboard(Player player) {
        final String PLUGIN_NAME = RustMC.instance().getDescription().getName()
                .toLowerCase();
        Scoreboard board = manager.getNewScoreboard();
        final Objective obj = board.registerNewObjective(PLUGIN_NAME, "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(ChatColor.translateAlternateColorCodes('&',
                ConfigurationHandler.hudTitle));
        for (String item : ConfigurationHandler.getItems("hud.items")) {
            String str = null;
            Pattern pat = Pattern.compile("(%\\w+%)");
            Matcher m = pat.matcher(item);
            while (m.find()) {
                str = m.group(1);
            }
            obj.getScore(
                    ChatColor.translateAlternateColorCodes(
                            '&',
                            item.replaceAll(str,
                                    ScoreboardUtil.parseItem(str, player))))
                    .setScore(1);
        }
        if (RadiationHandler.isRadiating(player)) {
            obj.getScore(ChatUtil.format("&lRadiation")).setScore(0);
        }
        if (player.hasMetadata("BLEEDING")) {
            obj.getScore(ChatUtil.format("&lBleeding")).setScore(0);
        }
        return board;
    }

    public static void updateScore(Player player) {
        player.setScoreboard(createScoreboard(player));
    }
}
