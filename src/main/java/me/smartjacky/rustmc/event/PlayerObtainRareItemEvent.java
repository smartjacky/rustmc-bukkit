package me.smartjacky.rustmc.event;

import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class PlayerObtainRareItemEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private ItemStack item;
    private Player player;
    private String message;
    private boolean cancelled;

    public PlayerObtainRareItemEvent(ItemStack item, Player player) {
        this.item = item;
        this.player = player;
        this.message = "&6"
                + player.getName()
                + " has found "
                + item.getAmount()
                + " &e"
                + ItemUtil.getItemDisplayName(item.getType(),
                        item.getDurability());
    }

    public ItemStack getItem() {
        return this.item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
