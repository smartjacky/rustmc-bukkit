package me.smartjacky.rustmc.misc;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.packet.PacketHandler;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class ParticleHandler {
    public static void createParticleEffect(Player player,
            EnumParticle particleType, Location location, float offX,
            float offY, float offZ, float data, int amount) throws Exception {
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles();
        PacketHandler.setValue(packet, "a", particleType);
        PacketHandler.setValue(packet, "b", (float) location.getX());
        PacketHandler.setValue(packet, "c", (float) location.getY());
        PacketHandler.setValue(packet, "d", (float) location.getZ());
        PacketHandler.setValue(packet, "e", offX);
        PacketHandler.setValue(packet, "f", offY);
        PacketHandler.setValue(packet, "g", offZ);
        PacketHandler.setValue(packet, "h", data);
        PacketHandler.setValue(packet, "i", amount);
        PacketHandler.sendPacket(player, packet);
    }

    public static void createRepeatingParticleEffect(final Player player,
            final EnumParticle particleType, final Entity entity,
            final float offX, final float offY, final float offZ,
            final float data, final int amount, int period, int time)
            throws Exception {
        final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(
                RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ParticleHandler.createParticleEffect(player,
                                    particleType, entity.getLocation(), offX,
                                    offY, offZ, data, amount);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, 0L, period);
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getScheduler().cancelTask(task);
                    }
                }, time * 20L);
    }

    public static void createRepeatingParticleEffect(final Player player,
            final EnumParticle particleType, final Location location,
            final float offX, final float offY, final float offZ,
            final float data, final int amount, long time) throws Exception {
        final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(
                RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ParticleHandler.createParticleEffect(player,
                                    particleType, location, offX, offY, offZ,
                                    data, amount);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, 0L, time * 20L);
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getScheduler().cancelTask(task);
                    }
                }, time * 20L);
    }
}
