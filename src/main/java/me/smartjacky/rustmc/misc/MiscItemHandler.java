package me.smartjacky.rustmc.misc;

import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ListUtil;

public class MiscItemHandler {
    public static void init() {
        Registry.registerMiscItem(Material.IRON_INGOT, (short) 0,
                "Metal Fragments");
        Registry.registerMiscItem(Material.STONE, (short) 0, "Stone");
        Registry.registerMiscItem(Material.WOOD, (short) 0, "Wood");
        Registry.registerMiscItem(Material.STRING, (short) 0, "Cloth");
        Registry.registerMiscItem(Material.IRON_PLATE, (short) 0,
                "Low Quaility Metal");
        Registry.registerMiscItem(Material.WOOD_STEP, (short) 0, "Wood Planks");
        Registry.registerMiscItem(Material.LEATHER, (short) 0, "Leather");
        Registry.registerMiscItem(Material.WOOD_DOOR, (short) 0, "Wooden Door");
        Registry.registerMiscItem(Material.ARROW, (short) 0, "Arrow");
        Registry.registerMiscItem(Material.SULPHUR, (short) 0, "Gun Powder");
        Registry.registerMiscItem(Material.CLAY_BRICK, (short) 0,
                "Research Kit");
        Registry.registerMiscItem(Material.RECORD_4, (short) 0, "C4 Detonator");
        Registry.registerMiscItem(Material.IRON_BARDING, (short) 0,
                "Regular Weapon Case");
        Registry.registerMiscItem(Material.GOLD_BARDING, (short) 0,
                "Rare Weapon Case");
        Registry.registerMiscItem(Material.DIAMOND_BARDING, (short) 0,
                "Elite Weapon Case");
        Registry.registerMiscItem(Material.CHEST, (short) 0, "Add Lootable");

    }

    public static boolean isMiscItem(String string) {
        for (String str : Registry.getMiscItemRegistry().values()) {
            if (str.replaceAll(" ", "").equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMiscItem(Material material, short metadata) {
        if (Registry.getMiscItemRegistry().containsKey(
                ListUtil.convertToKey(material, metadata))) {
            return true;
        } else {
            return false;
        }
    }

    public static ItemStack getMiscItem(String string) {
        for (String str : Registry.getMiscItemRegistry().values()) {
            if (str.replaceAll(" ", "").equalsIgnoreCase(string)) {
                for (Entry<List<Object>, String> entry : Registry
                        .getMiscItemRegistry().entrySet()) {
                    if (entry.getValue() == str) {
                        ItemStack item = new ItemStack((Material) entry
                                .getKey().get(0), 1);
                        item.setDurability((Short) entry.getKey().get(1));
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + entry.getValue()));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
