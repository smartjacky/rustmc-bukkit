package me.smartjacky.rustmc.misc;

import java.util.HashMap;

import org.bukkit.Location;

public class ExplosionHandler {
    private static HashMap<Location, Integer> explosions = null;

    public static void init() {
        ExplosionHandler.explosions = new HashMap<Location, Integer>();
    }

    public static void addExplosion(Location location, int damage) {
        ExplosionHandler.explosions.put(
                new Location(location.getWorld(), location.getBlockX(),
                        location.getBlockY(), location.getBlockZ()), damage);
    }

    public static int getDamage(Location location) {
        return ExplosionHandler.explosions.get(new Location(
                location.getWorld(), location.getBlockX(),
                location.getBlockY(), location.getBlockZ()));
    }

    public static void removeExplosion(Location location) {
        ExplosionHandler.explosions.remove(location);
    }

    public static boolean hasExplosionOccured(Location location) {
        if (ExplosionHandler.explosions.containsKey(new Location(location
                .getWorld(), location.getBlockX(), location.getBlockY(),
                location.getBlockZ()))) {
            return true;
        }
        return false;
    }
}
