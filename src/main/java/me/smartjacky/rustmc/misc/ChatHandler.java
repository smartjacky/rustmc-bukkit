package me.smartjacky.rustmc.misc;

import me.smartjacky.rustmc.packet.PacketHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ChatHandler {
    public static void message(Player player, String title, String subtitle,
            int time) {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(
                EnumTitleAction.TITLE, ChatUtil.toIChatBaseComponet(ChatUtil
                        .jsonConverter(title)), 0, time * 20, time * 20 + 1);
        PacketPlayOutTitle packet2 = new PacketPlayOutTitle(
                EnumTitleAction.SUBTITLE, ChatUtil.toIChatBaseComponet(ChatUtil
                        .jsonConverter(subtitle)), 0, time * 20, time * 20 + 1);
        PacketHandler.sendPacket(player, packet);
        PacketHandler.sendPacket(player, packet2);
    }

    public static void broadcast(String title, String subtitle, int time) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PacketPlayOutTitle packet = new PacketPlayOutTitle(
                    EnumTitleAction.TITLE,
                    ChatUtil.toIChatBaseComponet(ChatUtil.jsonConverter(title)),
                    0, time * 20, time * 20 + 1);
            PacketPlayOutTitle packet2 = new PacketPlayOutTitle(
                    EnumTitleAction.SUBTITLE,
                    ChatUtil.toIChatBaseComponet(ChatUtil
                            .jsonConverter(subtitle)), 0, time * 20,
                    time * 20 + 1);
            PacketHandler.sendPacket(player, packet);
            PacketHandler.sendPacket(player, packet2);
        }
    }

    public static void broadcast(String message) {
        Bukkit.broadcastMessage(ChatUtil.titleFormat(message));
    }
}
