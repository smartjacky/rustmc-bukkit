package me.smartjacky.rustmc.misc;

import java.util.Random;
import java.util.Set;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class AirdropHandler {
    @SuppressWarnings("deprecation")
    public static void createAirdrop(final Location location, int time) {
        final World world = ConfigurationHandler.getWorld();
        if (location != null) {
            if (!world.isChunkLoaded(world.getChunkAt(location))) {
                world.loadChunk(location.getChunk());
            }
            ChatHandler.broadcast("Location:" + location);
            ChatHandler.broadcast("&6New airdrop arriving at "
                    + location.getBlockX()
                    + " "
                    + location
                            .getWorld()
                            .getHighestBlockAt(location.getBlockX(),
                                    location.getBlockZ()).getLocation()
                            .getBlockY() + " " + location.getBlockZ());
            ChatHandler.broadcast("&6Airdrop arrival time", "&c&l" + time
                    + " seconds", 4);
            Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                    new Runnable() {
                        @Override
                        public void run() {
                            final FallingBlock fb = (FallingBlock) world
                                    .spawnFallingBlock(location,
                                            Material.CHEST, (byte) 0);
                            AirdropHandler.createFireworks(fb);
                            ChatHandler.broadcast("&6Airdrop is arrving",
                                    "&c&lNow", 4);
                        }
                    }, time * 20L);
        }
    }

    public static void addChestContent(Chest chest) {
        FileConfiguration config = RustMC.instance().getConfig();
        Set<String> availableItems = config.getConfigurationSection(
                "airDropItems").getKeys(false);
        for (String availableItem : availableItems) {
            if (new Random().nextInt(100) <= config.getInt("airDropItems."
                    + availableItem + ".rarity")) {
                ItemStack item = new ItemStack(
                        Material.getMaterial(availableItem),
                        new Random().nextInt(config.getInt("airDropItems."
                                + availableItem + ".amount")) + 1);
                ItemMeta im = item.getItemMeta();
                if (GunHandler.isGun(item.getType(), item.getDurability())) {
                    int gunType = new Random().nextInt(7);
                    item.setDurability((short) gunType);
                    im.setDisplayName(ChatColor.YELLOW
                            + ItemUtil.getItemDisplayName(item.getType(),
                                    item.getDurability()));
                } else if (StructureHandler.isStucture(item.getType(),
                        item.getDurability())) {
                    item.setDurability((short) new Random().nextInt(3));
                    im.setDisplayName(ChatColor.YELLOW
                            + ItemUtil.getItemDisplayName(item.getType(),
                                    item.getDurability()));
                } else {
                    im.setDisplayName(ChatColor.YELLOW
                            + ItemUtil.getItemDisplayName(item.getType(),
                                    item.getDurability()));
                }
                item.setItemMeta(im);
                chest.getInventory().addItem(item);
            }
        }

    }

    private static void createFireworks(final FallingBlock entity) {
        for (Entity nearEntity : entity.getNearbyEntities(50, 255, 50)) {
            if (nearEntity instanceof Player) {
                Player player = (Player) nearEntity;
                int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(
                        RustMC.instance(), new Runnable() {
                            @Override
                            public void run() {
                                if (!(entity.getLocation().getBlockY() == entity
                                        .getWorld().getHighestBlockYAt(
                                                entity.getLocation()
                                                        .getBlockX(),
                                                entity.getLocation()
                                                        .getBlockZ()))) {
                                    Firework fw = (Firework) entity.getWorld()
                                            .spawnEntity(entity.getLocation(),
                                                    EntityType.FIREWORK);
                                    FireworkMeta fm = fw.getFireworkMeta();
                                    FireworkEffect effect = FireworkEffect
                                            .builder().withColor(Color.ORANGE)
                                            .withColor(Color.YELLOW)
                                            .withColor(Color.WHITE).build();
                                    fm.setPower(0);
                                    fm.addEffect(effect);
                                    fw.setFireworkMeta(fm);
                                } else {
                                    int taskID = 0;
                                    for (MetadataValue val : entity
                                            .getMetadata("TASK_ID")) {
                                        taskID = (Integer) val.value();
                                    }
                                    Bukkit.getScheduler().cancelTask(taskID);
                                }
                            }
                        }, 0L, 1L);
                entity.setMetadata("TASK_ID",
                        new FixedMetadataValue(RustMC.instance(), task));
                player.playSound(player.getLocation(), Sound.FIREWORK_BLAST, 1,
                        1);
            }
        }
    }
}
