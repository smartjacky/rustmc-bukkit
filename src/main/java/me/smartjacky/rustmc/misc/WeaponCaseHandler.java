package me.smartjacky.rustmc.misc;

import java.util.Random;
import java.util.Set;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.event.PlayerObtainRareItemEvent;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WeaponCaseHandler {
    public enum WeaponCase {
        REGULAR(9, "Regular Weapon Case"), RARE(27, "Rare Weapon Case"), ELITE(
                54, "Elite Weapon Case");

        private int size;
        private String displayName;

        WeaponCase(int size, String displayName) {
            this.size = size;
            this.displayName = displayName;
        }

        public int getSize() {
            return size;
        }

        public String getDisplayName() {
            return displayName;
        }
    }

    public static Inventory getWeaponCase(final WeaponCase wc,
            final Player player) {
        final Inventory inventory = Bukkit.getServer().createInventory(null,
                wc.getSize(), wc.getDisplayName());
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, WeaponCaseHandler.getRandomItem(inventory));
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        WeaponCaseHandler.run(inventory, wc, player);
                    }
                }, 1L);
        return inventory;
    }

    private static ItemStack getRandomItem(Inventory inventory) {
        FileConfiguration config = RustMC.instance().getConfig();
        Set<String> wcSet = config.getConfigurationSection("weaponCaseItems")
                .getKeys(false);
        String[] wcString = wcSet.toArray(new String[wcSet.size()]);
        int randomInt = new Random().nextInt(wcString.length);
        String material = config.getString("weaponCaseItems."
                + wcString[randomInt] + ".type");
        int rarity = config.getInt("weaponCaseItems." + wcString[randomInt]
                + ".rarity");
        int amount = config.getInt("weaponCaseItems." + wcString[randomInt]
                + ".amount");
        short durability = (short) config.getInt("weaponCaseItems."
                + wcString[randomInt] + ".durability");
        if (new Random().nextInt(100) < rarity) {
            ItemStack item = new ItemStack(Material.getMaterial(material), 1);
            ItemMeta im = item.getItemMeta();
            item.setAmount(amount);
            item.setDurability(durability);
            if (GunHandler.isGun(item.getType(), item.getDurability())) {
                GunHandler.setGun(item);
            } else {
                im.setDisplayName(ChatColor.YELLOW
                        + ItemUtil.getItemDisplayName(item.getType(),
                                item.getDurability()));

            }
            return item;
        } else {
            return WeaponCaseHandler.getRandomItem(inventory);
        }
    }

    private static void run(final Inventory inventory, final WeaponCase wc,
            final Player player) {
        final ItemStack[] items = inventory.getContents();
        int randInt = new Random().nextInt(wc.getSize());
        for (int i = 0; i < randInt; i++) {
            if (i + 1 == randInt) {
                WeaponCaseHandler.randomizeItem(i, player, inventory, items,
                        true);
            } else {
                WeaponCaseHandler.randomizeItem(i, player, inventory, items,
                        false);
            }
        }
    }

    public static int getRarity(ItemStack item) {
        String displayName = ItemUtil
                .getItemDisplayName(item.getType(), item.getDurability())
                .toUpperCase().replaceAll(" ", "_");
        return RustMC.instance().getConfig()
                .getInt("weaponCaseItems." + displayName + ".rarity");
    }

    private static void randomizeItem(int times, final Player player,
            final Inventory inventory, final ItemStack[] items,
            final boolean takeItem) {
        final int randInt = new Random().nextInt(inventory.getSize());
        Material material;
        Sound sound;
        if (takeItem) {
            material = Material.EMERALD_BLOCK;
            sound = Sound.LEVEL_UP;
        } else {
            material = Material.REDSTONE_BLOCK;
            sound = Sound.NOTE_PIANO;
        }
        final Material displayMat = material;
        final Sound displaySound = sound;
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        inventory
                                .setItem(randInt, new ItemStack(displayMat, 1));
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                                RustMC.instance(), new Runnable() {
                                    @Override
                                    public void run() {
                                        inventory.setItem(randInt,
                                                items[randInt]);
                                    }
                                }, 15L);
                        player.playSound(player.getLocation(), displaySound, 1,
                                1);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(
                                RustMC.instance(), new Runnable() {
                                    @Override
                                    public void run() {
                                        if (takeItem == true) {
                                            player.sendMessage(ChatUtil.titleFormat("&aCongratulation! You have received "
                                                    + items[randInt]
                                                            .getAmount()
                                                    + " &e"
                                                    + ItemUtil
                                                            .getItemDisplayName(
                                                                    items[randInt]
                                                                            .getType(),
                                                                    items[randInt]
                                                                            .getDurability())
                                                    + "!"));
                                            player.closeInventory();
                                            if (WeaponCaseHandler
                                                    .getRarity(items[randInt]) <= 10) {
                                                PlayerObtainRareItemEvent event = new PlayerObtainRareItemEvent(
                                                        items[randInt], player);
                                                Bukkit.getPluginManager()
                                                        .callEvent(event);
                                                if (!event.isCancelled()) {
                                                    for (Player p : Bukkit
                                                            .getOnlinePlayers()) {
                                                        p.playSound(
                                                                p.getLocation(),
                                                                Sound.ENDERDRAGON_DEATH,
                                                                1, 1);
                                                    }
                                                    Bukkit.broadcastMessage(ChatUtil.titleFormat(event
                                                            .getMessage()));
                                                }
                                            }
                                            player.getInventory().addItem(
                                                    items[randInt]);
                                        }
                                    }
                                }, 20L);
                    }
                }, times * 15L);
    }
}
