package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableChickenCooked implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Cooked Chicken Breast";
    }

    @Override
    public void getEffect(Player player) {
    }

    @Override
    public int getFoodLevel() {
        return 500;
    }

    @Override
    public int getHealthLevel() {
        return 10;
    }
}
