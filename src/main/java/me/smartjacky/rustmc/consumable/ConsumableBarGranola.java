package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableBarGranola implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Granola Bar";
    }

    @Override
    public void getEffect(Player player) {
    }

    @Override
    public int getFoodLevel() {
        return 150;
    }

    @Override
    public int getHealthLevel() {
        return 5;
    }
}
