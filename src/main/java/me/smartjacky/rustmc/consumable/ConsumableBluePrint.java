package me.smartjacky.rustmc.consumable;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

public class ConsumableBluePrint implements IConsumable {

    @Override
    public void getEffect(Player player) {
        ItemStack blueprint = player.getItemInHand();
        if (blueprint != null
                && blueprint.hasItemMeta()
                && blueprint.getItemMeta().hasDisplayName()
                && blueprint.getItemMeta().getDisplayName()
                        .endsWith("Blue Print")) {
            String blueprintType = blueprint.getItemMeta().getDisplayName()
                    .replaceAll((char) 167 + "e", "")
                    .replaceAll("Blue Print", "").replaceAll(" ", "")
                    .toLowerCase();
            ItemStack item = ItemUtil.getItem(blueprintType);
            if (item != null
                    && Registry
                            .getRecipeRegistry()
                            .get(ListUtil.convertToKey(item.getType(),
                                    item.getDurability())).hasBluePrint()) {
                RustMC.permission.playerAdd(
                        player,
                        Registry.getRecipeRegistry()
                                .get(ListUtil.convertToKey(item.getType(),
                                        item.getDurability())).permission());
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&aYou can now craft "
                                + blueprint.getItemMeta().getDisplayName()
                                        .replace((char) 167 + "e", "")
                                        .replaceAll("Blue Print", "")));
            } else {
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cThis blue print is no longer usable"));
            }
        } else {
            player.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&cUnvaild Blue Print"));
        }
    }

    @Override
    public String getDisplayName() {
        return "Blue Print";
    }

    @Override
    public int getFoodLevel() {
        return 0;
    }

    @Override
    public int getHealthLevel() {
        return 0;
    }

}
