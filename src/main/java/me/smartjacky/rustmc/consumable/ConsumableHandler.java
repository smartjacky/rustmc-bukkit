package me.smartjacky.rustmc.consumable;

import java.util.Map.Entry;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ConsumableHandler {
    public static void init() {
        Registry.registerConsumable(Material.GOLD_RECORD,
                new ConsumableBandage());
        Registry.registerConsumable(Material.GREEN_RECORD,
                new ConsumableMedkitSmall());
        Registry.registerConsumable(Material.RECORD_3,
                new ConsumableMedkitLarge());
        Registry.registerConsumable(Material.BAKED_POTATO,
                new ConsumableCanTuna());
        Registry.getConsumableRegistry().put(Material.CARROT_ITEM,
                new ConsumableBarGranola());
        Registry.registerConsumable(Material.PUMPKIN_PIE,
                new ConsumableRations());
        Registry.registerConsumable(Material.BREAD,
                new ConsumableBarCholocate());
        Registry.registerConsumable(Material.APPLE, new ConsumableCanBeans());
        Registry.registerConsumable(Material.RABBIT,
                new ConsumableAntiRadiationPills());
        Registry.registerConsumable(Material.RAW_CHICKEN,
                new ConsumableChickenRaw());
        Registry.registerConsumable(Material.COOKED_CHICKEN,
                new ConsumableChickenCooked());
        Registry.registerConsumable(Material.REDSTONE_TORCH_ON,
                new ConsumableKitBloodDraw());
        Registry.registerConsumable(Material.NETHER_BRICK_ITEM,
                new ConsumableBluePrint());
    }

    public static void consume(Material key, final Player player) {
        if (Registry.getConsumableRegistry().containsKey(key)) {
            if (Registry.getConsumableRegistry().get(key).getFoodLevel() > 0) {
                if (player.getFoodLevel() == 1000) {
                } else if (player.getFoodLevel()
                        + Registry.getConsumableRegistry().get(key)
                                .getFoodLevel() > 1000
                        || player.getFoodLevel()
                                + Registry.getConsumableRegistry().get(key)
                                        .getFoodLevel() == 1000) {
                    player.setFoodLevel(1000);
                } else {
                    player.setFoodLevel(player.getFoodLevel()
                            + Registry.getConsumableRegistry().get(key)
                                    .getFoodLevel());
                }
            }
            if (Registry.getConsumableRegistry().get(key).getHealthLevel() > 0) {
                if (player.getHealth() < 100) {
                    if (player.getHealth()
                            + Registry.getConsumableRegistry().get(key)
                                    .getHealthLevel() > 100
                            || player.getHealth()
                                    + Registry.getConsumableRegistry().get(key)
                                            .getHealthLevel() == 100) {
                        player.setHealth(20);
                    } else {
                        player.setHealth(player.getHealth()
                                + Registry.getConsumableRegistry().get(key)
                                        .getHealthLevel());
                    }
                }
            } else if (Registry.getConsumableRegistry().get(key)
                    .getHealthLevel() < 0) {
                if (player.getHealth()
                        + Registry.getConsumableRegistry().get(key)
                                .getHealthLevel() <= 0) {
                    player.setHealth(0.0D);
                } else {
                    player.setHealth(player.getHealth()
                            + Registry.getConsumableRegistry().get(key)
                                    .getHealthLevel());
                }
            }
            player.playSound(player.getLocation(), Sound.EAT, 1, 1);
            Registry.getConsumableRegistry().get(key).getEffect(player);
            Bukkit.getServer().getScheduler()
                    .scheduleSyncDelayedTask(RustMC.instance(), new Runnable() {
                        @Override
                        public void run() {
                            if (player.getItemInHand().getAmount() == 1) {
                                player.getInventory().setItemInHand(
                                        new ItemStack(Material.AIR));
                            } else {
                                player.getItemInHand().setAmount(
                                        player.getItemInHand().getAmount() - 1);
                            }
                        }
                    }, 1L);
        }
    }

    public static boolean isConsumable(Material key) {
        if (Registry.getConsumableRegistry().containsKey(key)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isConsumable(String key) {
        for (IConsumable consumable : Registry.getConsumableRegistry().values()) {
            if (consumable.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(key)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getConsumale(String string) {
        for (IConsumable consumable : Registry.getConsumableRegistry().values()) {
            if (consumable.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<Material, IConsumable> entry : Registry
                        .getConsumableRegistry().entrySet()) {
                    if (entry.getValue() == consumable) {
                        ItemStack item = new ItemStack(entry.getKey(), 1);
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
