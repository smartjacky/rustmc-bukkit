package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableChickenRaw implements IConsumable {
    @Override
    public void getEffect(Player player) {
    }

    @Override
    public String getDisplayName() {
        return "Raw Chicken Breast";
    }

    @Override
    public int getFoodLevel() {
        return 80;
    }

    @Override
    public int getHealthLevel() {
        return -10;
    }
}
