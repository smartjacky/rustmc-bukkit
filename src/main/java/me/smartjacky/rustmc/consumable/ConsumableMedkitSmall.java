package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ConsumableMedkitSmall implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Small Medkit";
    }

    @Override
    public void getEffect(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,
                100, 4));
        if (player.hasMetadata("BLEEDING")) {
            player.removeMetadata("BLEEDING", RustMC.instance());
        }
    }

    @Override
    public int getFoodLevel() {
        return 0;
    }

    @Override
    public int getHealthLevel() {
        return 0;
    }
}
