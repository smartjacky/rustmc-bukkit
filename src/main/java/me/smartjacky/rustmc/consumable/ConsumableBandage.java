package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableBandage implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Bandage";
    }

    @Override
    public void getEffect(Player player) {
        if (player.hasMetadata("BLEEDING")) {
            player.removeMetadata("BLEEDING", RustMC.instance());
        }
    }

    @Override
    public int getFoodLevel() {
        return 0;
    }

    @Override
    public int getHealthLevel() {
        return 0;
    }
}
