package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;
import me.smartjacky.rustmc.radiation.RadiationHandler;

import org.bukkit.entity.Player;

public class ConsumableRations implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Small Rations";
    }

    @Override
    public void getEffect(Player player) {
        if (RadiationHandler.getPlayerRadiation(player) >= 300) {
            RadiationHandler.setPlayerRadiation(player,
                    RadiationHandler.getPlayerRadiation(player) - 300);
        } else {
            RadiationHandler.setPlayerRadiation(player, 0);
        }
        if (player.hasMetadata("BLEEDING")) {
            player.removeMetadata("BLEEDING", RustMC.instance());
        }
    }

    @Override
    public int getFoodLevel() {
        return 750;
    }

    @Override
    public int getHealthLevel() {
        return 25;
    }
}
