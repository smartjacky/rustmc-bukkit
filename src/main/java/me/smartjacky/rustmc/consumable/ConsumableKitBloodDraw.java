package me.smartjacky.rustmc.consumable;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IConsumable;

public class ConsumableKitBloodDraw implements IConsumable {

    @Override
    public void getEffect(final Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        player.getInventory().addItem(
                                new ItemStack(Material.REDSTONE, 1));

                    }
                }, 1L);
    }

    @Override
    public String getDisplayName() {
        return "Blood Draw Kit";
    }

    @Override
    public int getFoodLevel() {
        return 0;
    }

    @Override
    public int getHealthLevel() {
        return -25;
    }

}
