package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableCanBeans implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Can of Beans";
    }

    @Override
    public void getEffect(Player player) {
    }

    @Override
    public int getFoodLevel() {
        return 300;
    }

    @Override
    public int getHealthLevel() {
        return 5;
    }
}
