package me.smartjacky.rustmc.consumable;

import me.smartjacky.rustmc.api.IConsumable;

import org.bukkit.entity.Player;

public class ConsumableBarCholocate implements IConsumable {
    @Override
    public String getDisplayName() {
        return "Chocolate Bar";
    }

    @Override
    public void getEffect(Player player) {
    }

    @Override
    public int getFoodLevel() {
        return 350;
    }

    @Override
    public int getHealthLevel() {
        return 8;
    }
}
