package me.smartjacky.rustmc.consumable;

import org.bukkit.entity.Player;

import me.smartjacky.rustmc.api.IConsumable;
import me.smartjacky.rustmc.radiation.RadiationHandler;

public class ConsumableAntiRadiationPills implements IConsumable {

    @Override
    public void getEffect(Player player) {
        if (RadiationHandler.getPlayerRadiation(player) >= 200) {
            RadiationHandler.setPlayerRadiation(player,
                    RadiationHandler.getPlayerRadiation(player) - 200);
        } else {
            RadiationHandler.setPlayerRadiation(player, 0);
        }
    }

    @Override
    public String getDisplayName() {
        return "Anti-Radiation Pills";
    }

    @Override
    public int getFoodLevel() {
        return 0;
    }

    @Override
    public int getHealthLevel() {
        return 0;
    }

}
