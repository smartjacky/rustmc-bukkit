package me.smartjacky.rustmc.ammo;

import me.smartjacky.rustmc.api.IAmmo;

public class Ammo556 implements IAmmo {
    @Override
    public String getDisplayName() {
        return "5.56 Ammo";
    }
}
