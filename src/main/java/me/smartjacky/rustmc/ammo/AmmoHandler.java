package me.smartjacky.rustmc.ammo;

import java.util.Map.Entry;

import me.smartjacky.rustmc.api.IAmmo;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AmmoHandler {
    public static void init() {
        Registry.registerAmmo(Material.MELON_SEEDS, new AmmoShotgun());
        Registry.registerAmmo(Material.NETHER_STALK, new AmmoHandmade());
        Registry.registerAmmo(Material.PUMPKIN_SEEDS, new Ammo9MM());
        Registry.registerAmmo(Material.SEEDS, new Ammo556());
    }

    public static boolean isAmmo(Material material) {
        if (Registry.getAmmoRegistry().containsKey(material) == true) {
            return true;
        }
        return false;
    }

    public static boolean isAmmo(String string) {
        for (IAmmo ammo : Registry.getAmmoRegistry().values()) {
            if (ammo.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getAmmo(String string) {
        for (IAmmo ammo : Registry.getAmmoRegistry().values()) {
            if (ammo.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<Material, IAmmo> entry : Registry.getAmmoRegistry()
                        .entrySet()) {
                    if (entry.getValue() == ammo) {
                        ItemStack item = new ItemStack(entry.getKey(), 1);
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
