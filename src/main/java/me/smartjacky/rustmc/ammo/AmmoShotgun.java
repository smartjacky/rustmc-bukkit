package me.smartjacky.rustmc.ammo;

import me.smartjacky.rustmc.api.IAmmo;

public class AmmoShotgun implements IAmmo {
    @Override
    public String getDisplayName() {
        return "Shotgun Shell";
    }
}
