package me.smartjacky.rustmc.ammo;

import me.smartjacky.rustmc.api.IAmmo;

public class Ammo9MM implements IAmmo {
    @Override
    public String getDisplayName() {
        return "9mm Ammo";
    }
}
