package me.smartjacky.rustmc.ammo;

import me.smartjacky.rustmc.api.IAmmo;

public class AmmoHandmade implements IAmmo {
    @Override
    public String getDisplayName() {
        return "Handmade Shell";
    }
}
