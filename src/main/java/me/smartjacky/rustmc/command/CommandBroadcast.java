package me.smartjacky.rustmc.command;

import me.smartjacky.rustmc.misc.ChatHandler;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class CommandBroadcast implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length == 2) {
            ChatHandler.broadcast(args[0].replaceAll("_", " "),
                    args[1].replaceAll("_", " "), 5);
        } else if (args.length == 1) {
            Bukkit.broadcastMessage(ChatUtil.titleFormat(args[0].replaceAll(
                    "_", " ")));
        } else {
            sender.sendMessage(ChatUtil
                    .titleFormat("&cInvaild Usage. Correct Usage: /rustmc broadcast <msg> or /rustmc broadcast <title> <subtitle>"));
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.broadcast";
    }

    @Override
    public boolean conConsoleExecute() {
        return true;
    }
}
