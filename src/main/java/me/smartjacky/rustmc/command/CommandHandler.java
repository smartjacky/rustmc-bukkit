package me.smartjacky.rustmc.command;

import java.util.Arrays;
import java.util.HashMap;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public class CommandHandler implements CommandExecutor {
    private HashMap<String, ICommand> command;

    public CommandHandler() {
        this.command = new HashMap<String, ICommand>();
        this.command.put("startairdrop", new CommandAirdrop());
        this.command.put("buildmode", new CommandBuildMode());
        this.command.put("getradiation", new CommandGetRadiation());
        this.command.put("pos1", new CommandPosition1());
        this.command.put("pos2", new CommandPosition2());
        this.command.put("setradzone", new CommandSetRadiationZone());
        this.command.put("getradiation", new CommandSetPlayerRadiation());
        this.command.put("give", new CommandGive());
        this.command.put("broadcast", new CommandBroadcast());
        this.command.put("setlootable", new CommandLootable());
    }

    @Override
    public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
            String[] arg3) {
        if (arg3.length > 0 && this.command.containsKey(arg3[0])
                && arg0 instanceof ConsoleCommandSender
                && this.command.get(arg3[0]).conConsoleExecute() == false) {
            RustMC.log
                    .warning(ChatUtil
                            .format("%rustmc-prefix%This command cannot be executed from the console"));
        } else {
            if (arg1.getName().equalsIgnoreCase(
                    RustMC.instance().getDescription().getName().toLowerCase())) {
                if (arg3.length > 0 && this.command.containsKey(arg3[0])) {
                    if (arg3.length > 0) {
                        if (this.command.get(arg3[0]) != null) {
                            if (arg0.hasPermission(this.command.get(arg3[0])
                                    .getPermission())) {
                                this.command.get(arg3[0])
                                        .onCommand(
                                                arg0,
                                                Arrays.copyOfRange(arg3, 1,
                                                        arg3.length));
                            } else {
                                arg0.sendMessage(ChatUtil
                                        .format("&cYou do not have enough permission to execute this command"));
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
