package me.smartjacky.rustmc.command;

import java.util.ArrayList;
import java.util.List;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CommandLootable implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                FileConfiguration config = RustMC.instance().getConfig();
                List<String> lootables = config.getStringList("lootables");
                Location location = player.getLocation();

                int x = location.getBlockX(), y = location.getBlockY(), z = location
                        .getBlockZ();
                if (!lootables.contains(x + "," + y + "," + z)) {
                    if (lootables.isEmpty()) {
                        List<String> newList = new ArrayList<String>();
                        newList.add(x + "," + y + "," + z);
                        config.set("lootables", newList);
                    } else {
                        lootables.add(x + "," + y + "," + z);
                    }
                    player.sendMessage(ChatUtil
                            .titleFormat("&aNew lootable has been added"));
                } else {
                    player.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cThis lootable already exist"));
                }
            } else {
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cInvaild Usage /rustmc setlootable"));
            }
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.setlootable";
    }

    @Override
    public boolean conConsoleExecute() {
        return false;
    }
}
