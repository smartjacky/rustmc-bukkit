package me.smartjacky.rustmc.command;

import java.util.Collection;
import java.util.List;

import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class CommandGetRadiation implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length == 0) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (player.hasMetadata("RADIATION")) {
                    int radiation = 0;
                    List<MetadataValue> radiationList = player
                            .getMetadata("RADIATION");
                    for (MetadataValue val : radiationList) {
                        radiation = (Integer) val.value();
                    }
                    player.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&eYou have " + radiation
                                    + " points"));
                }
            } else {
                sender.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cConsole has 0 radiation point"));
            }
        } else if (args.length == 1) {
            boolean isPlayerOnline = false;
            Collection<? extends Player> onlinePlayers = Bukkit.getServer()
                    .getOnlinePlayers();
            for (Player player : onlinePlayers) {
                if (player.getName().equals(args[0])) {
                    if (player.hasMetadata("RADIATION")) {
                        int radiation = 0;
                        List<MetadataValue> radiationList = player
                                .getMetadata("RADIATION");
                        for (MetadataValue val : radiationList) {
                            radiation = (Integer) val.value();
                        }
                        player.sendMessage(ChatUtil.format("%rustmc-prefix%&e"
                                + player.getName() + " has " + radiation
                                + " points."));
                        isPlayerOnline = true;
                        break;
                    }
                } else {
                    sender.sendMessage(player.getName() + args[0]);
                }

            }
            if (isPlayerOnline == false) {
                sender.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cPlayer not found"));
            }
        } else {
            sender.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&cInvaild Usage. /rustmc radiation <player>"));
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.getradiation";
    }

    @Override
    public boolean conConsoleExecute() {
        return true;
    }
}
