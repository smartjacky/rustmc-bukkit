package me.smartjacky.rustmc.command;

import java.util.Random;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.misc.AirdropHandler;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAirdrop implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        World world = ConfigurationHandler.getWorld();
        Random random = new Random();
        Location airdropLocation = null;
        int x = 0, z = 0, time = 0;
        if (args.length == 1) {
            x = random.nextInt(1000);
            z = random.nextInt(1000);
            if (CommandHandler.isInteger(args[0])) {
                time = Integer.parseInt(args[0]);
                airdropLocation = new Location(world, x, 255, z);
            }
        } else if (args.length == 3) {
            if (CommandHandler.isInteger(args[0])
                    && CommandHandler.isInteger(args[1])
                    && CommandHandler.isInteger(args[0])) {
                x = Integer.parseInt(args[0]);
                z = Integer.parseInt(args[1]);
                time = Integer.parseInt(args[2]);
                airdropLocation = new Location(world, x, 255, z);
            } else {
                airdropLocation = null;
            }
        }
        if (airdropLocation != null) {
            AirdropHandler.createAirdrop(airdropLocation, time);
        } else {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cInvalid arguments. /rustmc startairtop <time> or /rustmc startairdrop <x> <z> <time>"));
            } else {
                RustMC.log
                        .info(ChatUtil
                                .format("%rustmc-prefix%&cInvalid arguments. /rustmc startairtop <time> or /rustmc startairdrop <x> <z> <time>"));
            }
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.airdrop";
    }

    @Override
    public boolean conConsoleExecute() {
        return true;
    }
}
