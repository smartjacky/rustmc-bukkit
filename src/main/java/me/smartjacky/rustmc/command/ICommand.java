package me.smartjacky.rustmc.command;

import org.bukkit.command.CommandSender;

public interface ICommand {
    public void onCommand(CommandSender sender, String[] args);

    public String getPermission();

    public boolean conConsoleExecute();
}
