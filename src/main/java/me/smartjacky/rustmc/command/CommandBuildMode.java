package me.smartjacky.rustmc.command;

import java.util.HashMap;

import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBuildMode implements ICommand {
    private static HashMap<Player, Boolean> buildModePlayers = null;

    public static void init() {
        CommandBuildMode.buildModePlayers = new HashMap<Player, Boolean>();
    }

    public static HashMap<Player, Boolean> getBuildModePlayers() {
        return buildModePlayers;

    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length == 0) {
            if (buildModePlayers.containsKey((Player) sender)) {
                if (buildModePlayers.get((Player) sender) == true) {
                    buildModePlayers.put((Player) sender, false);
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cBuild mode is off."));
                } else {
                    buildModePlayers.put((Player) sender, true);
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&aBuild mode is on."));
                }
            } else {
                buildModePlayers.put((Player) sender, true);
                sender.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&aBuild mode is on."));
            }
        } else {
            sender.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&cInvaild Useage. /rustmc buildmode"));
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.buildMode";
    }

    @Override
    public boolean conConsoleExecute() {
        return false;
    }
}
