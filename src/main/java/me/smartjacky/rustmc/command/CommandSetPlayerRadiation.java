package me.smartjacky.rustmc.command;

import me.smartjacky.rustmc.radiation.RadiationHandler;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CommandSetPlayerRadiation implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length == 2) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().equals(args[0])) {
                    if (CommandHandler.isInteger(args[1])) {
                        RadiationHandler.setPlayerRadiation(player,
                                Integer.parseInt(args[1]));
                        sender.sendMessage(ChatUtil.format("%rustmc-prefix%&a"
                                + player.getName() + " now has" + args[1]
                                + " radiation points"));
                    } else {
                        sender.sendMessage(ChatUtil.format("%rustmc-prefix%&c"
                                + args[1] + "is not an integer"));
                    }
                } else {
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cPlayer not found"));
                }
            }
        } else if (args.length == 1) {
            if (sender instanceof ConsoleCommandSender) {
                sender.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cYou cannot set radiation points for console"));
            } else {
                Player player = (Player) sender;
                if (CommandHandler.isInteger(args[1])) {
                    RadiationHandler.setPlayerRadiation(player,
                            Integer.parseInt(args[1]));
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&aYou now have" + args[1]
                                    + " radiation points"));
                }
            }
        } else {
            sender.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&cInvaild command. /rustmc setrad <player> <amount>"));
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.setradiation";
    }

    @Override
    public boolean conConsoleExecute() {
        return true;
    }
}
