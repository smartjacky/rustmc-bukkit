package me.smartjacky.rustmc.command;

import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandGive implements ICommand {
    @Override
    public void onCommand(final CommandSender sender, final String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            if (args.length == 3) {
                boolean foundPlayer = false;
                for (final Player player : Bukkit.getOnlinePlayers()) {
                    if (args[1].equals(player.getName())) {
                        foundPlayer = true;
                        ItemStack item = ItemUtil.getItem(args[2]);
                        if (item != null) {
                            if (CommandHandler.isInteger(args[3])) {
                                item.setAmount(Integer.parseInt(args[3]));
                            } else {
                                sender.sendMessage(ChatUtil
                                        .format("%rustmc-prefix%&cPlease use the correct give command. /rustmc give <player> <item> <amount>"));
                            }
                            player.getInventory().addItem(item);
                            sender.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&6Giving "
                                            + player.getName()
                                            + " "
                                            + args[3]
                                            + " "
                                            + item.getItemMeta()
                                                    .getDisplayName()));
                        } else {
                            sender.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&cItem not found"));
                        }
                        break;
                    }
                }
                if (foundPlayer == false) {
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cPlayer not found"));
                }
            } else {
                sender.sendMessage(ChatUtil
                        .format("&cYou must select a player"));
            }
        } else if (sender instanceof Player) {
            if (args.length == 2) {
                final Player player = (Player) sender;
                ItemStack item = ItemUtil.getItem(args[0]);
                if (item != null) {
                    if (CommandHandler.isInteger(args[1])) {
                        item.setAmount(Integer.parseInt(args[1]));
                    } else {
                        sender.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&cPlease use the correct give command. /rustmc give <item> <amount>"));
                    }
                    player.getInventory().addItem(item);
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&6Giving you " + args[1]
                                    + " " + item.getItemMeta().getDisplayName()));
                } else {
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cItem not found"));
                }
            } else if (args.length == 3) {
                boolean foundPlayer = false;
                for (final Player receiver : Bukkit.getOnlinePlayers()) {
                    if (args[0].equals(receiver.getName())) {
                        foundPlayer = true;
                        ItemStack item = ItemUtil.getItem(args[1]);
                        if (item != null) {
                            if (CommandHandler.isInteger(args[2])) {
                                item.setAmount(Integer.parseInt(args[2]));
                            } else {
                                sender.sendMessage(ChatUtil
                                        .format("%rustmc-prefix%&cPlease use the correct give command. /rustmc give <player> <item> <amount>"));
                            }
                            receiver.getInventory().addItem(item);
                            sender.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&6Giving "
                                            + receiver.getName()
                                            + " "
                                            + args[2]
                                            + " "
                                            + item.getItemMeta()
                                                    .getDisplayName()));
                        } else {
                            sender.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&cItem not found"));
                        }
                        ;
                    }
                }
                if (foundPlayer == false) {
                    sender.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cPlayer not found"));
                }
            } else {
                sender.sendMessage(ChatUtil
                        .format("&cPlease use the correct give command. /rustmc give <player> <item> <amount>"));
            }
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.give";
    }

    @Override
    public boolean conConsoleExecute() {
        return true;
    }
}
