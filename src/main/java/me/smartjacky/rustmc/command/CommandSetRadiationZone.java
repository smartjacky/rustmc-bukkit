package me.smartjacky.rustmc.command;

import java.util.List;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.PlayerUtil;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class CommandSetRadiationZone implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 1) {
                FileConfiguration config = RustMC.instance().getConfig();
                if (!config.contains("radiationZones." + args[0])) {
                    if (player.hasMetadata("POS_1")) {
                        if (player.hasMetadata("POS_2")) {
                            Location pos1 = (Location) PlayerUtil.getMetadata(
                                    player, "POS_1");
                            Location pos2 = (Location) PlayerUtil.getMetadata(
                                    player, "POS_2");
                            config.set("radiationZones." + args[0] + ".from.x",
                                    pos1.getBlockX());
                            config.set("radiationZones." + args[0] + ".from.z",
                                    pos1.getBlockZ());
                            config.set("radiationZones." + args[0] + ".to.x",
                                    pos2.getBlockX());
                            config.set("radiationZones." + args[0] + ".to.z",
                                    pos2.getBlockZ());
                            player.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&aNew radiation zone has been set"));
                            RustMC.instance().saveConfig();
                        } else {
                            player.sendMessage(ChatUtil
                                    .format("%rustmc-prefix%&cPosition 2 is not set"));
                        }
                    } else {
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&cPosition 1 is not set"));
                    }
                } else {
                    player.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cThis radiation zone already exist"));
                }
            } else {
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cInvaild Usage /rustmc setradiationzone <name>"));
            }
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.setradiationzone";
    }

    @Override
    public boolean conConsoleExecute() {
        return false;
    }

    public static Object getPlayerMetadata(Player player, String key) {
        List<MetadataValue> object = player.getMetadata(key);
        for (MetadataValue val : object) {
            Object value = val.value();
            return value;
        }
        return null;
    }
}
