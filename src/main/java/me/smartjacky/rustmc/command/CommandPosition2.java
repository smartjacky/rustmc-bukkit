package me.smartjacky.rustmc.command;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.util.ChatUtil;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

public class CommandPosition2 implements ICommand {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location loc = player.getLocation();
            player.setMetadata("POS_2",
                    new FixedMetadataValue(RustMC.instance(), loc));
            player.sendMessage(ChatUtil
                    .format("%rustmc-prefix%&ePosition 2 has set to "
                            + loc.getBlockX() + " " + loc.getBlockY() + " "
                            + loc.getBlockZ()));
        } else if (args.length == 3) {
            if (sender instanceof Player) {
                if (CommandHandler.isInteger(args[0])
                        && CommandHandler.isInteger(args[1])
                        && CommandHandler.isInteger(args[2])) {
                    Location loc = new Location(ConfigurationHandler.getWorld(),
                            Integer.parseInt(args[0]),
                            Integer.parseInt(args[1]),
                            Integer.parseInt(args[2]));
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        player.setMetadata("POS_2", new FixedMetadataValue(
                                RustMC.instance(), loc));
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&ePosition 2 has set to "
                                        + loc.getBlockX()
                                        + " "
                                        + loc.getBlockY()
                                        + " "
                                        + loc.getBlockZ()));
                    }
                }
            }
        } else {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cInvaild Usage. /rustmc pos2 <x> <y> <z>"));
            }
        }
    }

    @Override
    public String getPermission() {
        return "rustmc.wand";
    }

    @Override
    public boolean conConsoleExecute() {
        return false;
    }
}
