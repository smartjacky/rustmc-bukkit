package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeBoltActionRifle implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.INK_SACK, 5);
        item.setDurability((short) 5);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_PLATE, 30),
                new ItemStack(Material.WOOD, 50),
                new ItemStack(Material.LEATHER, 20) };
    }

    @Override
    public int craftTime() {
        return 60;
    }

    @Override
    public String permission() {
        return "rustmc.craft.gun.botactionrifle";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
