package me.smartjacky.rustmc.crafting;

import me.smartjacky.rustmc.api.IRecipe;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class RecipeM4 implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.INK_SACK, 1);
        item.setDurability((short) 0);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_PLATE, 40) };
    }

    @Override
    public int craftTime() {
        return 60;
    }

    @Override
    public String permission() {
        return "rustmc.craft.gun.m4";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
