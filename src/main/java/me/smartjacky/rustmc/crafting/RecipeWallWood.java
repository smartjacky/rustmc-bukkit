package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeWallWood implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.WOOL, 1);
        item.setDurability((short) 2);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.WOOD_STEP, 4) };
    }

    @Override
    public int craftTime() {
        return 30;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.woodwall";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
