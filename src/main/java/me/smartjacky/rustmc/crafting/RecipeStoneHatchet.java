package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeStoneHatchet implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.STONE_AXE, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STONE, 5),
                new ItemStack(Material.WOOD, 10) };
    }

    @Override
    public int craftTime() {
        return 20;
    }

    @Override
    public String permission() {
        return "rustmc.craft.tool.stonehatchet";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
