package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeAmmoShotgunShell implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.MELON_SEEDS, 1);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_INGOT, 2),
                new ItemStack(Material.SULPHUR, 5) };
    }

    @Override
    public int craftTime() {
        return 1;
    }

    @Override
    public String permission() {
        return "rustmc.craft.ammo.shotgunshell";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
