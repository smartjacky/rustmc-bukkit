package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeBandage implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.GOLD_RECORD, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STRING, 2) };
    }

    @Override
    public int craftTime() {
        return 10;
    }

    @Override
    public String permission() {
        return "rustmc.craft.medical.bandage";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
