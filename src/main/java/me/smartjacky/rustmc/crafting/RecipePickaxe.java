package me.smartjacky.rustmc.crafting;

import me.smartjacky.rustmc.api.IRecipe;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class RecipePickaxe implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_PICKAXE, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_INGOT, 15),
                new ItemStack(Material.WOOD, 40) };
    }

    @Override
    public int craftTime() {
        return 20;
    }

    @Override
    public String permission() {
        return "rustmc.craft.tool.pickaxe";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
