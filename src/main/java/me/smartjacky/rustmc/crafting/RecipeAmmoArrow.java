package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeAmmoArrow implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.ARROW, 4);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STONE, 1),
                new ItemStack(Material.WOOD, 4) };
    }

    @Override
    public int craftTime() {
        return 10;
    }

    @Override
    public String permission() {
        return "rustmc.craft.ammo.arrow";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
