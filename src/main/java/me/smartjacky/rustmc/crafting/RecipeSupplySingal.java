package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeSupplySingal implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.GOLD_BARDING) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.SULPHUR, 80),
                new ItemStack(Material.IRON_INGOT, 40) };
    }

    @Override
    public int craftTime() {
        return 30;
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }

    @Override
    public String permission() {
        return "rustmc.craft.weapons.supplysingal";
    }
}
