package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeFurnace implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.FURNACE, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.FURNACE, 1) };
    }

    @Override
    public int craftTime() {
        return 5;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.furnace";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
