package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeClothHelmet implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.LEATHER_HELMET, 1);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STRING, 5) };
    }

    @Override
    public int craftTime() {
        return 0;
    }

    @Override
    public String permission() {
        return "rustmc.craft.armor.clothhelmet";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
