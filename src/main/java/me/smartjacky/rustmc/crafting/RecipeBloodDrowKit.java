package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeBloodDrowKit implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.REDSTONE_TORCH_ON, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STRING, 10),
                new ItemStack(Material.IRON_INGOT, 100) };
    }

    @Override
    public int craftTime() {
        return 120;
    }

    @Override
    public String permission() {
        return "rustmc.craft.medical.blooddrawkit";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
