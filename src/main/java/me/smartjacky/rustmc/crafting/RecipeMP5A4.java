package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeMP5A4 implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.INK_SACK, 4);
        item.setDurability((short) 4);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_PLATE, 20) };
    }

    @Override
    public int craftTime() {
        return 60;
    }

    @Override
    public String permission() {
        return "rustmc.craft.gun.mp5a4";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
