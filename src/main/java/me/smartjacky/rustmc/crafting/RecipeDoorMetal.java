package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeDoorMetal implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_DOOR, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_INGOT, 200) };
    }

    @Override
    public int craftTime() {
        return 30;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.metaldoor";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
