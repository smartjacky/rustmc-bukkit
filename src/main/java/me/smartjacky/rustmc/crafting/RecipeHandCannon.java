package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeHandCannon implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.INK_SACK, 6);
        item.setDurability((short) 6);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_INGOT, 10),
                new ItemStack(Material.WOOD, 20) };
    }

    @Override
    public int craftTime() {
        return 60;
    }

    @Override
    public String permission() {
        return "rustmc.craft.gun.handCannon";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
