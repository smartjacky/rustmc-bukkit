package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeDoorWood implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.WOOD_DOOR, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.WOOD, 20) };
    }

    @Override
    public int craftTime() {
        return 30;
    }

    @Override
    public String permission() {
        return "rustmc.craft.strucutre.wooddoor";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
