package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeSlopeMetal implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.WOOL, 1);
        item.setDurability((short) 11);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_PLATE, 5) };
    }

    @Override
    public int craftTime() {
        return 15;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.metalslope";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
