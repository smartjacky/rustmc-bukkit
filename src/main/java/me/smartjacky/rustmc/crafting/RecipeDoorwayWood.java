package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeDoorwayWood implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.WOOL, 1);
        item.setDurability((short) 12);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.WOOD_STEP, 4) };
    }

    @Override
    public int craftTime() {
        return 10;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.wooddoorway";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
