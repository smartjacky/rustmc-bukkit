package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeAmmo9mm implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.PUMPKIN_SEEDS, 1);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.IRON_INGOT, 1),
                new ItemStack(Material.SULPHUR, 3) };
    }

    @Override
    public int craftTime() {
        return 5;
    }

    @Override
    public String permission() {
        return "rustmc.craft.ammo.9mm";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
