package me.smartjacky.rustmc.crafting;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

public class CraftingHandler {
    public static void init() {
        Registry.registerRecipe(Material.FURNACE, (short) 0,
                new RecipeFurnace());
        Registry.registerRecipe(Material.STONE_AXE, (short) 0,
                new RecipeStoneHatchet());
        Registry.registerRecipe(Material.IRON_AXE, (short) 0,
                new RecipeHatchet());
        Registry.registerRecipe(Material.IRON_PICKAXE, (short) 0,
                new RecipePickaxe());
        Registry.registerRecipe(Material.INK_SACK, (short) 0, new RecipeM4());
        Registry.registerRecipe(Material.INK_SACK, (short) 1, new Recipe9mm());
        Registry.registerRecipe(Material.INK_SACK, (short) 2,
                new RecipeShotgun());
        Registry.registerRecipe(Material.INK_SACK, (short) 3, new RecipeP250());
        Registry.registerRecipe(Material.INK_SACK, (short) 4, new RecipeMP5A4());
        Registry.registerRecipe(Material.INK_SACK, (short) 5,
                new RecipeBoltActionRifle());
        Registry.registerRecipe(Material.INK_SACK, (short) 6,
                new RecipeHandCannon());
        Registry.registerRecipe(Material.INK_SACK, (short) 7,
                new RecipePipeShotgun());
        Registry.registerRecipe(Material.INK_SACK, (short) 8,
                new RecipeRevolver());
        Registry.registerRecipe(Material.WOOL, (short) 0,
                new RecipePlatformWood());
        Registry.registerRecipe(Material.WOOL, (short) 6,
                new RecipePlatformMetal());
        Registry.registerRecipe(Material.WOOL, (short) 12,
                new RecipeDoorwayWood());
        Registry.registerRecipe(Material.WOOL, (short) 13,
                new RecipeDoorwayMetal());
        Registry.registerRecipe(Material.WOOL, (short) 1,
                new RecipePillarWood());
        Registry.registerRecipe(Material.WOOL, (short) 7,
                new RecipePillarMetal());
        Registry.registerRecipe(Material.WOOL, (short) 9, new RecipeRoofMetal());
        Registry.registerRecipe(Material.WOOL, (short) 3, new RecipeRoofWood());
        Registry.registerRecipe(Material.WOOL, (short) 5, new RecipeSlopeWood());
        Registry.registerRecipe(Material.WOOL, (short) 11,
                new RecipeSlopeMetal());
        Registry.registerRecipe(Material.WOOL, (short) 2, new RecipeWallWood());
        Registry.registerRecipe(Material.WOOL, (short) 8, new RecipeWallMetal());
        Registry.registerRecipe(Material.WOOL, (short) 4,
                new RecipeStairsWood());
        Registry.registerRecipe(Material.WOOL, (short) 10,
                new RecipeStairsMetal());
        Registry.registerRecipe(Material.IRON_DOOR, (short) 0,
                new RecipeDoorMetal());
        Registry.registerRecipe(Material.WOOD_DOOR, (short) 0,
                new RecipeDoorWood());
        Registry.registerRecipe(Material.LEATHER_HELMET, (short) 0,
                new RecipeClothHelmet());
        Registry.registerRecipe(Material.CHAINMAIL_HELMET, (short) 0,
                new RecipeLeatherHelmet());
        Registry.registerRecipe(Material.IRON_HELMET, (short) 0,
                new RecipeKevlarHelmet());
        Registry.registerRecipe(Material.GOLD_HELMET, (short) 0,
                new RecipeRadiationHelmet());
        Registry.registerRecipe(Material.LEATHER_CHESTPLATE, (short) 0,
                new RecipeClothVest());
        Registry.registerRecipe(Material.CHAINMAIL_CHESTPLATE, (short) 0,
                new RecipeLeatherVest());
        Registry.registerRecipe(Material.IRON_CHESTPLATE, (short) 0,
                new RecipeKevlarVest());
        Registry.registerRecipe(Material.GOLD_CHESTPLATE, (short) 0,
                new RecipeRadiationVest());
        Registry.registerRecipe(Material.LEATHER_LEGGINGS, (short) 0,
                new RecipeClothPants());
        Registry.registerRecipe(Material.CHAINMAIL_LEGGINGS, (short) 0,
                new RecipeLeatherPants());
        Registry.registerRecipe(Material.IRON_LEGGINGS, (short) 0,
                new RecipeKevlarPants());
        Registry.registerRecipe(Material.GOLD_LEGGINGS, (short) 0,
                new RecipeRadiationPants());
        Registry.registerRecipe(Material.LEATHER_BOOTS, (short) 0,
                new RecipeClothBoots());
        Registry.registerRecipe(Material.CHAINMAIL_BOOTS, (short) 0,
                new RecipeLeatherBoots());
        Registry.registerRecipe(Material.IRON_BOOTS, (short) 0,
                new RecipeKevlarBoots());
        Registry.registerRecipe(Material.GOLD_BOOTS, (short) 0,
                new RecipeRadiationBoots());
        Registry.registerRecipe(Material.GOLD_RECORD, (short) 0,
                new RecipeBandage());
        Registry.registerRecipe(Material.GREEN_RECORD, (short) 0,
                new RecipeSmallMedkit());
        Registry.registerRecipe(Material.RECORD_3, (short) 0,
                new RecipeLargeMedkit());
        Registry.registerRecipe(Material.REDSTONE_TORCH_ON, (short) 0,
                new RecipeBloodDrowKit());
        Registry.registerRecipe(Material.PUMPKIN_SEEDS, (short) 0,
                new RecipeAmmo9mm());
        Registry.registerRecipe(Material.ARROW, (short) 0,
                new RecipeAmmoArrow());
        Registry.registerRecipe(Material.NETHER_STALK, (short) 0,
                new RecipeAmmoHandmadeShell());
        Registry.registerRecipe(Material.SEEDS, (short) 0, new RecipeAmmo556());
        Registry.registerRecipe(Material.MELON_SEEDS, (short) 0,
                new RecipeAmmoShotgunShell());
        Registry.registerRecipe(Material.WHEAT, (short) 0,
                new RecipeGrenadeF1());
        Registry.registerRecipe(Material.CLAY_BALL, (short) 0,
                new RecipeGrenadeSmoke());
        Registry.registerRecipe(Material.COAL, (short) 0, new RecipeFlashbang());
    }

    public static void craft(final Player player, Material material,
            short metadata) {
        final PlayerInventory inventory = player.getInventory();
        if (!player.hasMetadata("CRAFTING")) {
            if (Registry.getRecipeRegistry().containsKey(
                    ListUtil.convertToKey(material, metadata))) {
                if (Registry.getRecipeRegistry()
                        .get(ListUtil.convertToKey(material, metadata))
                        .hasBluePrint()
                        && !player.hasPermission(Registry.getRecipeRegistry()
                                .get(ListUtil.convertToKey(material, metadata))
                                .permission())) {
                    player.sendMessage(ChatUtil
                            .format("%rustmc-prefix%&cThis item is not craftable for you. Please unlock it with a blue print or a research kit first"));
                } else {
                    boolean hasItems = true;
                    for (ItemStack item : Registry.getRecipeRegistry()
                            .get(ListUtil.convertToKey(material, metadata))
                            .fromItems()) {
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatColor.YELLOW
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability()));
                        item.setItemMeta(im);
                        if (!inventory.containsAtLeast(item, item.getAmount())) {
                            hasItems = false;
                        }
                    }
                    if (hasItems) {
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&aCrafting Item...\nTime required: "
                                        + Registry
                                                .getRecipeRegistry()
                                                .get(ListUtil.convertToKey(
                                                        material, metadata))
                                                .craftTime() + " seconds"));
                        player.setMetadata("CRAFTING", new FixedMetadataValue(
                                RustMC.instance(), true));
                        player.addPotionEffect(new PotionEffect(
                                PotionEffectType.SLOW, Registry
                                        .getRecipeRegistry()
                                        .get(ListUtil.convertToKey(material,
                                                metadata)).craftTime() * 20, 1));
                        for (final ItemStack item : Registry
                                .getRecipeRegistry()
                                .get(ListUtil.convertToKey(material, metadata))
                                .fromItems()) {

                            ItemStack itemStack = new ItemStack(item.getType());
                            ItemMeta im = itemStack.getItemMeta();
                            im.setDisplayName(ChatColor.YELLOW
                                    + ItemUtil.getItemDisplayName(
                                            item.getType(),
                                            item.getDurability()));
                            itemStack.setItemMeta(im);
                            itemStack.setAmount(item.getAmount());
                            inventory.removeItem(itemStack);
                        }
                        for (final ItemStack item : Registry
                                .getRecipeRegistry()
                                .get(ListUtil.convertToKey(material, metadata))
                                .toItems()) {
                            Bukkit.getScheduler().scheduleSyncDelayedTask(
                                    RustMC.instance(),
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            ItemMeta im = item.getItemMeta();
                                            im.setDisplayName(ChatUtil.format("&e"
                                                    + ItemUtil
                                                            .getItemDisplayName(
                                                                    item.getType(),
                                                                    item.getDurability())));
                                            item.setItemMeta(im);
                                            player.sendMessage(ChatUtil.format("%rustmc-prefix%&aYou have crafted "
                                                    + ItemUtil
                                                            .getItemDisplayName(
                                                                    item.getType(),
                                                                    item.getDurability())));
                                            inventory.addItem(item);
                                            player.removeMetadata("CRAFTING",
                                                    RustMC.instance());
                                        }
                                    },
                                    Registry.getRecipeRegistry()
                                            .get(ListUtil.convertToKey(
                                                    material, metadata))
                                            .craftTime() * 20L);
                        }
                    } else {
                        player.sendMessage(ChatUtil
                                .format("%rustmc-prefix%&cYou do not have enough material to craft this item"));
                    }
                }
            } else {
                player.sendMessage(ChatUtil
                        .format("%rustmc-prefix%&cThis item is currenly uncraftable"));
            }
        } else {
            player.sendMessage(ChatUtil
                    .titleFormat("&cYou are crafting an item, please wait for it to finish"));
        }
    }

    public static boolean hasRecipe(Material material, short metadata) {
        if (Registry.getRecipeRegistry().containsKey(
                ListUtil.convertToKey(material, metadata))) {
            return true;
        } else {
            return false;
        }
    }
}
