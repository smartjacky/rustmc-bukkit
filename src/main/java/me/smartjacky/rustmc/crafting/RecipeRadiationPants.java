package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeRadiationPants implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.GOLD_LEGGINGS, 1);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STRING, 15),
                new ItemStack(Material.IRON_INGOT, 40) };
    }

    @Override
    public int craftTime() {
        return 0;
    }

    @Override
    public String permission() {
        return "rustmc.craft.armor.radiationpants";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
