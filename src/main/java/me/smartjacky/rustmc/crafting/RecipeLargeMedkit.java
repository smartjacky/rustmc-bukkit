package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeLargeMedkit implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        return new ItemStack[] { new ItemStack(Material.RECORD_3, 1) };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.STRING, 3),
                new ItemStack(Material.REDSTONE, 3) };
    }

    @Override
    public int craftTime() {
        return 10;
    }

    @Override
    public String permission() {
        return "rustmc.craft.medical.largemedkit";
    }

    @Override
    public boolean hasBluePrint() {
        return true;
    }
}
