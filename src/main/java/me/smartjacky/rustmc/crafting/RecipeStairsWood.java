package me.smartjacky.rustmc.crafting;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IRecipe;

public class RecipeStairsWood implements IRecipe {
    @Override
    public ItemStack[] toItems() {
        ItemStack item = new ItemStack(Material.WOOL, 1);
        item.setDurability((short) 4);
        return new ItemStack[] { item };
    }

    @Override
    public ItemStack[] fromItems() {
        return new ItemStack[] { new ItemStack(Material.WOOD_STEP, 5) };
    }

    @Override
    public int craftTime() {
        return 20;
    }

    @Override
    public String permission() {
        return "rustmc.craft.structure.woodstairs";
    }

    @Override
    public boolean hasBluePrint() {
        return false;
    }
}
