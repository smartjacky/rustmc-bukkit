package me.smartjacky.rustmc.projectile;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IProjectile;
import me.smartjacky.rustmc.misc.ParticleHandler;
import net.minecraft.server.v1_8_R3.EnumParticle;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public class ProjectileSmoke implements IProjectile {
    @Override
    public void main(final World world, final Item item, final Player player) {
        final int smoke = Bukkit.getScheduler().scheduleSyncRepeatingTask(
                RustMC.instance(), new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 5; i++) {
                            for (Entity entity : item.getNearbyEntities(30, 30,
                                    30)) {
                                if (entity instanceof Player) {
                                    Player nearPlayer = (Player) entity;
                                    nearPlayer.playSound(item.getLocation(),
                                            Sound.FIZZ, 1, 1);
                                    try {
                                        sendPacket(nearPlayer,
                                                item.getLocation());
                                        sendPacket(player, item.getLocation());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }, 0L, 5L);
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getScheduler().cancelTask(smoke);
                    }
                }, 200L);
    }

    public void sendPacket(Player player, Location location) throws Exception {
        ParticleHandler.createParticleEffect(player,
                EnumParticle.EXPLOSION_LARGE, location, 2F, 2F, 2F, 0F, 30);
    }

    @Override
    public int getDelay() {
        return 2;
    }

    @Override
    public int getSpeed() {
        return 1;
    }

    @Override
    public String getDisplayName() {
        return "Smoke Grenade";
    }
}
