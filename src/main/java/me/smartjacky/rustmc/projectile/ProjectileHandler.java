package me.smartjacky.rustmc.projectile;

import java.util.List;
import java.util.Map.Entry;

import me.smartjacky.rustmc.RustMC;
import me.smartjacky.rustmc.api.IProjectile;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class ProjectileHandler {
    public static void init() {
        Registry.registerProjectile(Material.WHEAT, (short) 0,
                new ProjectileGrenade());
        Registry.registerProjectile(Material.CLAY_BALL, (short) 0,
                new ProjectileSmoke());
        Registry.registerProjectile(Material.GLOWSTONE_DUST, (short) 0,
                new ProjectileSupplySignal());
        Registry.registerProjectile(Material.COAL, (short) 0,
                new ProjectileFlashbang());
    }

    public static void createProjectile(final Player player) {
        final IProjectile projectile = Registry.getProjectileRegistry().get(
                ListUtil.convertToKey(player.getItemInHand().getType(), player
                        .getItemInHand().getDurability()));
        final World world = ConfigurationHandler.getWorld();
        final Item item = world.dropItemNaturally(player.getEyeLocation(),
                new ItemStack(player.getItemInHand().getType(), 1));
        item.setVelocity(vector(player).multiply(projectile.getSpeed()));
        item.setPickupDelay(10000);
        Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance(),
                new Runnable() {
                    @Override
                    public void run() {
                        item.remove();
                        projectile.main(world, item, player);
                        ;
                    }
                }, projectile.getDelay() * 20);
        ItemStack itemInHand = new ItemStack(player.getItemInHand().getType(),
                1);
        ItemMeta im = itemInHand.getItemMeta();
        im.setDisplayName(ChatColor.YELLOW
                + ItemUtil.getItemDisplayName(player.getItemInHand().getType(),
                        player.getItemInHand().getDurability()));
        itemInHand.setItemMeta(im);
        player.getInventory().removeItem(itemInHand);
    }

    public static Vector vector(Player player) {
        double pitch = 0, yaw = 0;
        pitch = ((player.getLocation().getPitch() + 90) * Math.PI) / 180;
        yaw = ((player.getLocation().getYaw() + 90) * Math.PI) / 180;
        double x = Math.sin(pitch) * Math.cos(yaw);
        double y = Math.sin(pitch) * Math.sin(yaw);
        double z = Math.cos(pitch);
        Vector vector = new Vector(x, z, y);
        return vector;
    }

    public static boolean isProjectile(Material material, short metadata) {
        if (Registry.getProjectileRegistry().containsKey(
                ListUtil.convertToKey(material, metadata))) {
            return true;
        }
        return false;
    }

    public static boolean isProjectile(String string) {
        for (IProjectile projectile : Registry.getProjectileRegistry().values()) {
            if (projectile.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static ItemStack getProjectile(String string) {
        for (IProjectile projectile : Registry.getProjectileRegistry().values()) {
            if (projectile.getDisplayName().replaceAll(" ", "")
                    .equalsIgnoreCase(string)) {
                for (Entry<List<Object>, IProjectile> entry : Registry
                        .getProjectileRegistry().entrySet()) {
                    if (entry.getValue() == projectile) {
                        ItemStack item = new ItemStack((Material) entry
                                .getKey().get(0), 1);
                        item.setDurability((Short) entry.getKey().get(1));
                        ItemMeta im = item.getItemMeta();
                        im.setDisplayName(ChatUtil.format("&e"
                                + ItemUtil.getItemDisplayName(item.getType(),
                                        item.getDurability())));
                        item.setItemMeta(im);
                        return item;
                    }
                }
            }
        }
        return null;
    }
}
