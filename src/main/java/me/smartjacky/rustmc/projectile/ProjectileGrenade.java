package me.smartjacky.rustmc.projectile;

import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import me.smartjacky.rustmc.api.IProjectile;
import me.smartjacky.rustmc.misc.ExplosionHandler;

public class ProjectileGrenade implements IProjectile {
    @Override
    public void main(World world, Item item, Player player) {
        ExplosionHandler.addExplosion(world.getBlockAt(item.getLocation())
                .getLocation(), 50);
        world.createExplosion(item.getLocation(), 2);
    }

    @Override
    public int getDelay() {
        return 2;
    }

    @Override
    public int getSpeed() {
        return 1;
    }

    @Override
    public String getDisplayName() {
        return "F1 Grenade";
    }
}
