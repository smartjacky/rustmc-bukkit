package me.smartjacky.rustmc.projectile;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.smartjacky.rustmc.api.IProjectile;

public class ProjectileFlashbang implements IProjectile {
    @Override
    public void main(World world, Item item, Player player) {
        for (Entity entity : item.getNearbyEntities(20, 20, 20)) {
            if (entity instanceof Player) {
                Player p = (Player) entity;
                p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,
                        100, 4));
            }
        }
    }

    @Override
    public int getDelay() {
        return 2;
    }

    @Override
    public int getSpeed() {
        return 1;
    }

    @Override
    public String getDisplayName() {
        return "Flashbang";
    }
}
