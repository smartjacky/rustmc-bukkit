package me.smartjacky.rustmc.projectile;

import me.smartjacky.rustmc.api.IProjectile;
import me.smartjacky.rustmc.misc.AirdropHandler;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public class ProjectileSupplySignal implements IProjectile {
    @Override
    public void main(World world, Item item, Player player) {
        AirdropHandler.createAirdrop(
                new Location(item.getWorld(), item.getLocation().getBlockX(),
                        255, item.getLocation().getBlockZ()), 60);
    }

    @Override
    public int getDelay() {
        return 2;
    }

    @Override
    public int getSpeed() {
        return 1;
    }

    @Override
    public String getDisplayName() {
        return "Supply Singal";
    }
}
