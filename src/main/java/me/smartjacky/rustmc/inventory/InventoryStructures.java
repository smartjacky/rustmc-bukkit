package me.smartjacky.rustmc.inventory;

import me.smartjacky.rustmc.api.IInventory;

import org.bukkit.Material;

public class InventoryStructures implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        if (material == Material.WOOL && metadata == (short) 0) {
            return new String[] { "craft", "WOOL", "0" };
        } else if (material == Material.WOOL && metadata == (short) 1) {
            return new String[] { "craft", "WOOL", "1" };
        } else if (material == Material.WOOL && metadata == (short) 2) {
            return new String[] { "craft", "WOOL", "2" };
        } else if (material == Material.WOOL && metadata == (short) 3) {
            return new String[] { "craft", "WOOL", "3" };
        } else if (material == Material.WOOL && metadata == (short) 4) {
            return new String[] { "craft", "WOOL", "4" };
        } else if (material == Material.WOOL && metadata == (short) 5) {
            return new String[] { "craft", "WOOL", "5" };
        } else if (material == Material.WOOL && metadata == (short) 6) {
            return new String[] { "craft", "WOOL", "6" };
        } else if (material == Material.WOOL && metadata == (short) 7) {
            return new String[] { "craft", "WOOL", "7" };
        } else if (material == Material.WOOL && metadata == (short) 8) {
            return new String[] { "craft", "WOOL", "8" };
        } else if (material == Material.WOOL && metadata == (short) 9) {
            return new String[] { "craft", "WOOL", "9" };
        } else if (material == Material.WOOL && metadata == (short) 10) {
            return new String[] { "craft", "WOOL", "10" };
        } else if (material == Material.WOOL && metadata == (short) 11) {
            return new String[] { "craft", "WOOL", "11" };
        } else if (material == Material.WOOL && metadata == (short) 12) {
            return new String[] { "craft", "WOOL", "12" };
        } else if (material == Material.WOOL && metadata == (short) 13) {
            return new String[] { "craft", "WOOL", "13" };
        } else if (material == Material.WOOD_DOOR && metadata == (short) 0) {
            return new String[] { "craft", "WOOD_DOOR", "0" };
        } else if (material == Material.IRON_DOOR && metadata == (short) 0) {
            return new String[] { "craft", "IRON_DOOR", "0" };
        } else {
            return new String[] { "open", "Structures" };
        }
    }

    @Override
    public String getInventoryName() {
        return "Structures";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 0, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 1, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 2, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 3, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 4, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 5, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 6, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 7, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 8, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.WOOL, (short) 9, 1),
                InventoryHandler.CraftInventoryItem(Material.WOOL, (short) 10,
                        1),
                InventoryHandler.CraftInventoryItem(Material.WOOL, (short) 11,
                        1),
                InventoryHandler.CraftInventoryItem(Material.WOOL, (short) 12,
                        1),
                InventoryHandler.CraftInventoryItem(Material.WOOL, (short) 13,
                        1),
                InventoryHandler.CraftInventoryItem(Material.WOOD_DOOR,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_DOOR,
                        (short) 0, 1), };
        return items;
    }
}
