package me.smartjacky.rustmc.inventory;

import java.util.ArrayList;
import java.util.List;

import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryItemStack {
    private Material material;
    private int amount;
    private short metadata;
    private String displayName;
    private String[] lures;
    private ItemStack[] items;

    public InventoryItemStack(Material mat, int amo, short meta, String name,
            String[] lur) {
        material = mat;
        metadata = meta;
        amount = amo;
        displayName = name;
        lures = lur;
    }

    public InventoryItemStack(Material mat, int amo, short meta, String name,
            String[] lur, ItemStack[] ites) {
        material = mat;
        metadata = meta;
        amount = amo;
        displayName = name;
        lures = lur;
        items = ites;
    }

    public ItemStack toItemStack() {
        List<String> lu = new ArrayList<String>();
        ItemStack item = new ItemStack(material, amount);
        item.setDurability(metadata);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(ChatColor.BLUE + displayName);
        if (items != null) {
            for (ItemStack ite : items) {
                lu.add(ChatUtil.format("&7"
                        + ite.getAmount()
                        + " "
                        + ItemUtil.getItemDisplayName(ite.getType(),
                                ite.getDurability())));
            }
        }
        for (String lure : lures) {
            lu.add(ChatColor.GRAY + lure);
        }
        im.setLore(lu);
        item.setItemMeta(im);
        return item;
    }
}
