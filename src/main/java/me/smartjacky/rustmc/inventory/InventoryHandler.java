package me.smartjacky.rustmc.inventory;

import java.util.ArrayList;
import java.util.List;

import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.crafting.CraftingHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;
import me.smartjacky.rustmc.util.ListUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryHandler {
    public static void init() {
        Registry.registerInventory("Menu", new InventoryMenu());
        Registry.registerInventory("Weapons", new InventoryWeapons());
        Registry.registerInventory("Tools", new InventoryTools());
        Registry.registerInventory("Structures", new InventoryStructures());
        Registry.registerInventory("Medical", new InventoryMedical());
        Registry.registerInventory("Armor", new InventoryArmor());
        Registry.registerInventory("Ammunition", new InventoryAmmunition());
        Registry.registerInventory("Research Kit", new InventoryResearchKit());
    }

    public static Inventory getInventory(String key, Player player) {
        int formatSize = 0, switchTimes = 0;
        Number chestSize = 0;
        if (Registry.getInventoryRegistry().get(key).getItems().length > 27) {
            chestSize = 6;
        } else {
            chestSize = new Double(Math.ceil(Registry.getInventoryRegistry()
                    .get(key).getItems().length * 2 / 9.0));
        }
        Inventory inv = Bukkit.getServer().createInventory(null,
                9 * chestSize.intValue(),
                Registry.getInventoryRegistry().get(key).getInventoryName());
        for (int i = 0; i < Registry.getInventoryRegistry().get(key).getItems().length; i++) {
            if (Registry.getInventoryRegistry().get(key).getItems().length < 27) {
                formatSize = i;
            }
            ItemStack item = Registry.getInventoryRegistry().get(key)
                    .getItems()[i].toItemStack();
            if (item.hasItemMeta() && item.getItemMeta().hasLore()) {
                String lure = "";
                for (String str : item.getItemMeta().getLore()) {
                    lure = str;
                }
                if (lure.endsWith("Section")) {
                    inv.setItem(i + formatSize - switchTimes, item);
                    if (formatSize == i && i != 0 && (i + 1) % 5 == 0) {
                        switchTimes += 1;
                    }
                } else if (key != "Research Kit"
                        && CraftingHandler.hasRecipe(item.getType(),
                                item.getDurability())) {
                    if (Registry
                            .getRecipeRegistry()
                            .get(ListUtil.convertToKey(item.getType(),
                                    item.getDurability())).hasBluePrint()
                            && !player
                                    .hasPermission(Registry
                                            .getRecipeRegistry()
                                            .get(ListUtil.convertToKey(
                                                    item.getType(),
                                                    item.getDurability()))
                                            .permission())) {
                        List<String> lu = new ArrayList<String>();
                        String[] strs = {
                                ChatUtil.format("&c&lYou cannot craft this item"),
                                ChatUtil.format("&7&lWays To Unlock:"),
                                ChatUtil.format("&7&l- Research Kit"),
                                ChatUtil.format("&7&l- "
                                        + ItemUtil.getItemDisplayName(
                                                item.getType(),
                                                item.getDurability())
                                        + " Blue Print") };
                        ItemMeta im = item.getItemMeta();
                        for (String str : strs) {
                            lu.add(str);
                        }
                        im.getLore().clear();
                        im.setLore(lu);
                        ;
                        item.setItemMeta(im);
                    }
                    inv.setItem(i + formatSize - switchTimes, item);
                    if (formatSize == i && i != 0 && (i + 1) % 5 == 0) {
                        switchTimes += 1;
                    }
                } else if (key == "Research Kit") {
                    if (player.hasPermission(Registry
                            .getRecipeRegistry()
                            .get(ListUtil.convertToKey(item.getType(),
                                    item.getDurability())).permission())) {
                        List<String> lu = new ArrayList<String>();
                        ItemMeta im = item.getItemMeta();
                        lu.add(ChatUtil.format("&a&lUnlocked"));
                        im.getLore().clear();
                        im.setLore(lu);
                        item.setItemMeta(im);
                    }
                    inv.setItem(i + formatSize - switchTimes, item);
                    if (formatSize == i && i != 0 && (i + 1) % 5 == 0) {
                        switchTimes += 1;
                    }
                }
            }
        }
        return inv;
    }

    public static boolean isCraftingInventory(String key) {
        if (Registry.getInventoryRegistry().containsKey(key)) {
            return true;
        }
        return false;
    }

    public static InventoryItemStack CraftInventoryItem(Material material,
            short metadata, int amount) {
        return new InventoryItemStack(material, amount, metadata,
                ItemUtil.getItemDisplayName(material, metadata),
                new String[] { Registry.getRecipeRegistry()
                        .get(ListUtil.convertToKey(material, metadata))
                        .craftTime()
                        + " seoncds to craft" }, Registry.getRecipeRegistry()
                        .get(ListUtil.convertToKey(material, metadata))
                        .fromItems());
    }
}
