package me.smartjacky.rustmc.inventory;

import org.bukkit.Material;

import me.smartjacky.rustmc.api.IInventory;

public class InventoryArmor implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        switch (material) {
        case LEATHER_HELMET:
            return new String[] { "craft", material.toString(), "0" };
        case LEATHER_CHESTPLATE:
            return new String[] { "craft", material.toString(), "0" };
        case LEATHER_LEGGINGS:
            return new String[] { "craft", material.toString(), "0" };
        case LEATHER_BOOTS:
            return new String[] { "craft", material.toString(), "0" };
        case CHAINMAIL_HELMET:
            return new String[] { "craft", material.toString(), "0" };
        case CHAINMAIL_CHESTPLATE:
            return new String[] { "craft", material.toString(), "0" };
        case CHAINMAIL_LEGGINGS:
            return new String[] { "craft", material.toString(), "0" };
        case CHAINMAIL_BOOTS:
            return new String[] { "craft", material.toString(), "0" };
        case IRON_HELMET:
            return new String[] { "craft", material.toString(), "0" };
        case IRON_CHESTPLATE:
            return new String[] { "craft", material.toString(), "0" };
        case IRON_LEGGINGS:
            return new String[] { "craft", material.toString(), "0" };
        case IRON_BOOTS:
            return new String[] { "craft", material.toString(), "0" };
        case GOLD_HELMET:
            return new String[] { "craft", material.toString(), "0" };
        case GOLD_CHESTPLATE:
            return new String[] { "craft", material.toString(), "0" };
        case GOLD_LEGGINGS:
            return new String[] { "craft", material.toString(), "0" };
        case GOLD_BOOTS:
            return new String[] { "craft", material.toString(), "0" };
        default:
            return new String[] { "open", "Armor" };
        }
    }

    @Override
    public String getInventoryName() {
        return "Armor";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler.CraftInventoryItem(Material.LEATHER_HELMET,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(
                        Material.LEATHER_CHESTPLATE, (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.LEATHER_LEGGINGS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.LEATHER_BOOTS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.CHAINMAIL_HELMET,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(
                        Material.CHAINMAIL_CHESTPLATE, (short) 0, 1),
                InventoryHandler.CraftInventoryItem(
                        Material.CHAINMAIL_LEGGINGS, (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.CHAINMAIL_BOOTS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_HELMET,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_CHESTPLATE,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_LEGGINGS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_BOOTS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.GOLD_HELMET,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.GOLD_CHESTPLATE,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.GOLD_LEGGINGS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.GOLD_BOOTS,
                        (short) 0, 1) };

        return items;
    }
}
