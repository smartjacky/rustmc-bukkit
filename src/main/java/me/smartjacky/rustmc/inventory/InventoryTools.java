package me.smartjacky.rustmc.inventory;

import me.smartjacky.rustmc.api.IInventory;

import org.bukkit.Material;

public class InventoryTools implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        switch (material) {
        case STONE_AXE:
            return new String[] { "craft", "STONE_AXE", "0" };
        case IRON_AXE:
            return new String[] { "craft", "IRON_AXE", "0" };
        case IRON_PICKAXE:
            return new String[] { "craft", "IRON_PICKAXE", "0" };
        default:
            return new String[] { "open", "Tools" };

        }
    }

    @Override
    public String getInventoryName() {
        return "Tools";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler.CraftInventoryItem(Material.STONE_AXE,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_AXE,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.IRON_PICKAXE,
                        (short) 0, 1) };
        return items;
    }
}
