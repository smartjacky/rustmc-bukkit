package me.smartjacky.rustmc.inventory;

import me.smartjacky.rustmc.api.IInventory;

import org.bukkit.Material;

public class InventoryMenu implements IInventory {
    @Override
    public String[] getAction(Material item, short metadata) {
        String[] action = { "open", "Menu" };
        if (item == Material.IRON_PICKAXE) {
            action = new String[] { "open", "Tools" };
        } else if (item == Material.BRICK) {
            action = new String[] { "open", "Structures" };
        } else if (item == Material.INK_SACK) {
            action = new String[] { "open", "Weapons" };
            ;
        } else if (item == Material.GOLD_RECORD) {
            action = new String[] { "open", "Medical" };
        } else if (item == Material.IRON_CHESTPLATE) {
            action = new String[] { "open", "Armor" };
        } else if (item == Material.SEEDS) {
            action = new String[] { "open", "Ammunition" };
        }
        return action;
    }

    @Override
    public String getInventoryName() {
        return "Menu";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                new InventoryItemStack(Material.IRON_PICKAXE, 1, (short) 0,
                        "Tools", new String[] { "Tools Section" }),
                new InventoryItemStack(Material.BRICK, 1, (short) 0,
                        "Structures", new String[] { "Structures Section" }),
                new InventoryItemStack(Material.INK_SACK, 1, (short) 0,
                        "Weapons", new String[] { "Weapons Section" }),
                new InventoryItemStack(Material.GOLD_RECORD, 1, (short) 0,
                        "Medical", new String[] { "Medical Section" }),
                new InventoryItemStack(Material.IRON_CHESTPLATE, 1, (short) 0,
                        "Armor", new String[] { "Armor Section" }),
                new InventoryItemStack(Material.SEEDS, 1, (short) 0,
                        "Ammunition", new String[] { "Ammunition Section" }) };
        return items;
    }
}
