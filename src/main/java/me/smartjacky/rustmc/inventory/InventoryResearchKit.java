package me.smartjacky.rustmc.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.smartjacky.rustmc.api.IInventory;
import me.smartjacky.rustmc.api.IRecipe;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.ItemUtil;

public class InventoryResearchKit implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        for (InventoryItemStack item : this.getItems()) {
            if (material == item.toItemStack().getType()
                    && metadata == item.toItemStack().getDurability()) {
                return new String[] { "unlock", material.toString(),
                        Short.toString(metadata) };
            }
        }
        return null;
    }

    @Override
    public String getInventoryName() {
        return "Research Kit";
    }

    @Override
    public InventoryItemStack[] getItems() {
        List<InventoryItemStack> itemsList = new ArrayList<InventoryItemStack>();
        for (IRecipe recipe : Registry.getRecipeRegistry().values()) {
            if (recipe.hasBluePrint()) {
                for (ItemStack item : recipe.toItems()) {
                    itemsList.add(new InventoryItemStack(item.getType(), item
                            .getAmount(), item.getDurability(), ItemUtil
                            .getItemDisplayName(item.getType(),
                                    item.getDurability()),
                            new String[] { ChatUtil.format("&6&lUnlock "
                                    + ItemUtil.getItemDisplayName(
                                            item.getType(),
                                            item.getDurability())) }));
                }
            }
        }
        return itemsList.toArray(new InventoryItemStack[itemsList.size()]);
    }
}
