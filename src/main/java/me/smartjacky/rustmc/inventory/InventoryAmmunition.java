package me.smartjacky.rustmc.inventory;

import org.bukkit.Material;

import me.smartjacky.rustmc.api.IInventory;

public class InventoryAmmunition implements IInventory {
    @Override
    public String[] getAction(Material item, short metadata) {
        switch (item) {
        case SEEDS:
            return new String[] { "craft", item.toString(),
                    Short.toString(metadata) };
        case PUMPKIN_SEEDS:
            return new String[] { "craft", item.toString(),
                    Short.toString(metadata) };
        case MELON_SEEDS:
            return new String[] { "craft", item.toString(),
                    Short.toString(metadata) };
        case NETHER_STALK:
            return new String[] { "craft", item.toString(),
                    Short.toString(metadata) };
        case ARROW:
            return new String[] { "craft", item.toString(),
                    Short.toString(metadata) };
        default:
            return new String[] { "open", "Ammunition" };
        }
    }

    @Override
    public String getInventoryName() {
        return "Ammunition";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler.CraftInventoryItem(Material.SEEDS, (short) 0,
                        1),
                InventoryHandler.CraftInventoryItem(Material.MELON_SEEDS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.PUMPKIN_SEEDS,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.NETHER_STALK,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.ARROW, (short) 0,
                        1) };
        return items;
    }
}
