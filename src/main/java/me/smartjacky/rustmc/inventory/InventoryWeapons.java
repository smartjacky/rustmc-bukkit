package me.smartjacky.rustmc.inventory;

import me.smartjacky.rustmc.api.IInventory;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class InventoryWeapons implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        for (InventoryItemStack initem : this.getItems()) {
            ItemStack item = initem.toItemStack();
            if (material == item.getType() && metadata == item.getDurability()) {
                return new String[] { "craft", item.getType().toString(),
                        Short.toString(metadata) };
            }
        }
        return null;
    }

    @Override
    public String getInventoryName() {
        return "Weapons";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 1, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 2, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 3, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 4, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 5, 1),
                InventoryHandler.CraftInventoryItem(Material.INK_SACK,
                        (short) 6, 1),
                InventoryHandler.CraftInventoryItem(Material.WHEAT, (short) 0,
                        1),
                InventoryHandler.CraftInventoryItem(Material.CLAY_BALL,
                        (short) 0, 1),
                InventoryHandler
                        .CraftInventoryItem(Material.COAL, (short) 0, 1) };
        return items;
    }
}
