package me.smartjacky.rustmc.inventory;

import me.smartjacky.rustmc.api.IInventory;

import org.bukkit.Material;

public class InventoryMedical implements IInventory {
    @Override
    public String[] getAction(Material material, short metadata) {
        for (InventoryItemStack item : this.getItems()) {
            if (item.toItemStack().getType() == material
                    && item.toItemStack().getDurability() == metadata) {
                return new String[] { "craft", material.toString(),
                        Short.toString(metadata) };
            }
        }
        return new String[] { "open", "Medical" };
    }

    @Override
    public String getInventoryName() {
        return "Medical";
    }

    @Override
    public InventoryItemStack[] getItems() {
        InventoryItemStack[] items = {
                InventoryHandler.CraftInventoryItem(Material.GOLD_RECORD,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.GREEN_RECORD,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.RECORD_3,
                        (short) 0, 1),
                InventoryHandler.CraftInventoryItem(Material.REDSTONE_TORCH_ON,
                        (short) 0, 1) };
        return items;
    }
}
