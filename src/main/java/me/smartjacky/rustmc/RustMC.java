package me.smartjacky.rustmc;

import java.io.IOException;
import java.util.logging.Logger;

import me.smartjacky.rustmc.ammo.AmmoHandler;
import me.smartjacky.rustmc.api.Registry;
import me.smartjacky.rustmc.armor.ArmorHandler;
import me.smartjacky.rustmc.command.CommandBuildMode;
import me.smartjacky.rustmc.command.CommandHandler;
import me.smartjacky.rustmc.configuration.ConfigurationHandler;
import me.smartjacky.rustmc.consumable.ConsumableHandler;
import me.smartjacky.rustmc.crafting.CraftingHandler;
import me.smartjacky.rustmc.drop.MobDropHandler;
import me.smartjacky.rustmc.entity.RustMCEntity;
import me.smartjacky.rustmc.gun.GunHandler;
import me.smartjacky.rustmc.inventory.InventoryHandler;
import me.smartjacky.rustmc.listener.CreatureListener;
import me.smartjacky.rustmc.listener.InventoryListener;
import me.smartjacky.rustmc.listener.MobSpawnListener;
import me.smartjacky.rustmc.listener.PlayerListener;
import me.smartjacky.rustmc.listener.WorldListener;
import me.smartjacky.rustmc.misc.ExplosionHandler;
import me.smartjacky.rustmc.misc.MiscItemHandler;
import me.smartjacky.rustmc.projectile.ProjectileHandler;
import me.smartjacky.rustmc.radiation.RadiationHandler;
import me.smartjacky.rustmc.structure.StructureHandler;
import me.smartjacky.rustmc.task.TaskHandler;
import me.smartjacky.rustmc.tool.ToolHandler;
import me.smartjacky.rustmc.util.ChatUtil;
import me.smartjacky.rustmc.util.FileUtil;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class RustMC extends JavaPlugin {
    public static final Logger log = Bukkit.getLogger();
    public static Permission permission = null;
    private static RustMC instance;
    private final PluginDescriptionFile pdfile = this.getDescription();

    public RustMC() {
        RustMC.instance = this;
    }

    public static RustMC instance() {
        return RustMC.instance;
    }

    @Override
    public void onEnable() {
        this.setupPermissions();
        ConfigurationHandler.load();
        if (!FileUtil.getUserDir().exists()) {
            FileUtil.getUserDir().mkdir();
        }
        Bukkit.getPluginManager().registerEvents(new CreatureListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new WorldListener(), this);
        Bukkit.getPluginManager().registerEvents(new MobSpawnListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
        this.getCommand(this.pdfile.getName().toLowerCase()).setExecutor(
                new CommandHandler());
        this.init();
        this.setWorldBorder();
        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
        } catch (IOException e) {
        }
        RustMC.log.info("[" + pdfile.getName() + "] " + pdfile.getFullName()
                + " is now enabled");
    }

    @Override
    public void onDisable() {
        if (ConfigurationHandler.stopMessageEnabled == true) {
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                player.kickPlayer(ChatUtil
                        .titleFormat(ConfigurationHandler.stopMessage));
            }
        }
        Bukkit.getScheduler().cancelTasks(this);
        RustMC.log.info("[" + pdfile.getName() + "] " + pdfile.getFullName()
                + " is now disabled");
        RustMCEntity.unregisterEntities();
    }

    private boolean setupPermissions() {
        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Permission> permissionProvider = getServer()
                    .getServicesManager().getRegistration(
                            net.milkbowl.vault.permission.Permission.class);
            if (permissionProvider != null) {
                permission = permissionProvider.getProvider();
                return (permission != null);
            }
        } else {
            RustMC.log.warning("[" + pdfile.getName() + "] "
                    + "Vault is not installed, RustMC will be disabled");
            Bukkit.getScheduler().scheduleSyncDelayedTask(RustMC.instance,
                    new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.getPluginManager().disablePlugin(
                                    RustMC.instance);
                        }
                    }, 1L);
        }
        return false;
    }

    private void setWorldBorder() {
        WorldBorder wb = ConfigurationHandler.getWorld().getWorldBorder();
        wb.setCenter(new Location(ConfigurationHandler.getWorld(), 0, 64, 0));
        wb.setSize(ConfigurationHandler.getWorldBorderSize());
        wb.setDamageAmount(0);
    }

    private void init() {
        new Registry();
        new RadiationHandler();
        CommandBuildMode.init();
        InventoryHandler.init();
        ConsumableHandler.init();
        StructureHandler.init();
        AmmoHandler.init();
        GunHandler.init();
        ToolHandler.init();
        MobDropHandler.init();
        CraftingHandler.init();
        MiscItemHandler.init();
        ArmorHandler.init();
        ProjectileHandler.init();
        ExplosionHandler.init();
        RustMCEntity.registerEntities();
        new TaskHandler();
    }
}
